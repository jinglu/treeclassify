#include "RandomForest.h"
#include <Common/StatisticAPI.h>
#include <omp.h>
#include <iostream>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>


using namespace std;

// define the static members
TreeTrainParameters RandomTree::s_trainParameter;
ForestTrainParameters RandomForest::s_trainParameter;

int RandomForest::s_numSplitSegments;
int TreeNodeSplitor::s_featDim;
vector<int> RandomForest::s_activeAttributesIdxs;

//////////////******************* TreeTrainParameter **********************///////////////////
TreeTrainParameters::TreeTrainParameters()
    : _maxDepth(32),
      _minLeafSize(1),
      _ratioPosTry(0.33),
      _numSplitTry(10),
      _numPosTry(10),
      _splitorType(TreeTrainParameters::SPLITOR_Stump)
{
}

/////////// ********************* ForestTrainParameter ******************/////////////////////

ForestTrainParameters::ForestTrainParameters()
    : _numTrees(48),
      _treeSampleRatio(1),
      _isParalleTrain(false),
      _isParalleTest(false)
{
}

///////////////***************** TreeSplitor ***********************////////////////////
TreeNodeSplitor::TreeNodeSplitor()
    : _splitPos(-1),
      _splitVal(-1)
{

}

TreeNodeSplitor::~TreeNodeSplitor()
{

}

inline int TreeNodeSplitor::classifySample(const DataSample *sample) const
{
    return (sample->getDataAt(_splitPos) <= _splitVal) ? -1 : 1;
}

bool TreeNodeSplitor::loadTreeNodeSplitor(istream &str)
{
    int filelength = 1024*10;
    char temp[filelength];
    if(RandomTree::s_trainParameter._splitorType == TreeTrainParameters::SPLITOR_Stump)
    {
        str.getline(temp,filelength);
        string textLine(temp);

        vector<string> dataElement;
        //BasicAPI::TextlineSplit(textLine,dataElement,' ');
        boost::split(dataElement, textLine, boost::is_any_of(" "), boost::token_compress_on);

        std::istringstream stm2;
        stm2.str(dataElement.at(1).c_str());
        stm2 >> _splitPos;

        std::istringstream stm3;
        stm3.str(dataElement.at(2).c_str());
        stm3 >>_splitVal;
    }
    return true;
}

void TreeNodeSplitor::saveTreeNodeSplitor(ostream &str)
{

    if(RandomTree::s_trainParameter._splitorType == TreeTrainParameters::SPLITOR_Stump)
        str<<"Splitor "<<_splitPos<<" "<<_splitVal<<endl;
}

//////////////////************** TreeNode *************************/////////////////////
TreeNode::TreeNode()
    : _numSamples(0),
      _level(-1),
      _nodeIdx(-1),
      _nodeType(TreeNode::NODE_INTERNAL),
      _leafLabelIdx(-1),
      _leftChildIdx(-1),
      _rightChildIdx(-1),
      _purity(0),
      _objectivePurity(0.95),
      _splitor(NULL),
      _leftChild(NULL),
      _rightChild(NULL)
{

}

TreeNode::TreeNode(const int nodeIdx)
    : _numSamples(0),
      _level(-1),
      _nodeType(TreeNode::NODE_INTERNAL),
      _leafLabelIdx(-1),
      _leftChildIdx(-1),
      _rightChildIdx(-1),
      _purity(0),
      _objectivePurity(0.95),
      _splitor(NULL),
      _leftChild(NULL),
      _rightChild(NULL),
      _nodeIdx(nodeIdx)
{

}

TreeNode::~TreeNode()
{
    if (_splitor != NULL)
        delete _splitor;
    _splitor = NULL;
}

int TreeNode::getNodeLevel() const
{
    return _level;
}

void TreeNode::setNodeLevel(const int level)
{
    _level = level;
}

int TreeNode::getNodeIdx() const
{
    return _nodeIdx;
}

void TreeNode::setNodeIdx(const int idx)
{
    _nodeIdx = idx;
}

TreeNode::TreeNodeType TreeNode::getNodeType() const
{
    return _nodeType;
}

void TreeNode::setNodeType(const TreeNodeType type)
{
    _nodeType = type;
}

int TreeNode::getLeafLabelIdx() const
{
    if (_nodeType == TreeNode::NODE_LEAF)
        return _leafLabelIdx;
    else
    {
        cout << "Error: the node is not a leaf. " << endl;
        return -1;
    }
}

void TreeNode::setLeafLabelIdx(const int labelIdx)
{
    if (_nodeType == TreeNode::NODE_LEAF)
        _leafLabelIdx = labelIdx;
    else
        cout << "Error : the node is not a leaf " << endl;
}

bool TreeNode::isLeaf() const
{
    return ((_nodeType == TreeNode::NODE_LEAF) ? true : false);
}

void TreeNode::constructSplitor(DataSampleSet *dataSamples,
                                DataSampleSet *&leftTreeSamples,
                                DataSampleSet *&rightTreeSamples)
{
    //distribution of samples
    if (dataSamples->_categoryDists == NULL)
        dataSamples->estimatePurity();
    EntropyType maxInfoGain = 0;

    // initialize left & right subtree
    assert(leftTreeSamples == NULL);
    assert(rightTreeSamples == NULL);

    leftTreeSamples = new DataSampleSet(dataSamples);
    rightTreeSamples = new DataSampleSet(dataSamples);

    DataSampleSet* tempLeftTreeSamples  = new DataSampleSet(dataSamples);
    DataSampleSet* tempRightTreeSamples = new DataSampleSet(dataSamples);

    assert(RandomTree::s_trainParameter._splitorType == TreeTrainParameters::SPLITOR_Stump);

    if (RandomTree::s_trainParameter._splitorType == TreeTrainParameters::SPLITOR_Stump)
    {
        vector<int> selectAttributesIdxs;
        vector<int> selectIdxs;

        vector<pair<DataType, DataType> > featValueRanges; //value range of each attribute
        //generate random sequence between a range
        StatisticAPI::generateRandomSequence(0,
                                             RandomForest::s_activeAttributesIdxs.size()-1,
                                             RandomTree::s_trainParameter._numPosTry,
                                             selectIdxs);
        cout << "random select size " << selectIdxs.size() << endl;
        for (size_t i = 0; i < selectIdxs.size(); ++i)
            selectAttributesIdxs.push_back(RandomForest::s_activeAttributesIdxs.at(selectIdxs.at(i)));

        dataSamples->buildAttributesRange(selectAttributesIdxs, featValueRanges);

        if (_splitor != NULL)
            delete _splitor;
        _splitor = new TreeNodeSplitor();

        TreeNodeSplitor* tempSplitor = new TreeNodeSplitor();

        for (size_t i = 0; i < selectAttributesIdxs.size(); ++i)
        {
            DataType range = featValueRanges[i].second - featValueRanges[i].first;
            cout << "range " << range << endl;
            if (range > 1e-6) //valid range
            {
                tempSplitor->_splitPos = selectAttributesIdxs[i];
                DataType stepSize = range / RandomForest::s_numSplitSegments;
                cout << "num split segment " << RandomForest::s_numSplitSegments << endl;
                for (int j = 0; j < RandomForest::s_numSplitSegments; ++j)
                {
                    tempSplitor->_splitVal = featValueRanges[i].first + j * stepSize;

                    // evaluate with classification accuracy
                    EntropyType infoGain = evaluateSplitor(tempSplitor, dataSamples, tempLeftTreeSamples, tempRightTreeSamples);
                    cout<<"info gain: "<<infoGain<<"("<<tempLeftTreeSamples->_ptSamples.size()<<","<<tempRightTreeSamples->_ptSamples.size()<<")"<<endl;
                    // the update of the splitor subject to two conditions:
                    // 1 the increase of information gain;
                    // 2 the size of the two  splited sub sets must be more than a threshold

                    // check the info gain, update the maximum info gain
                    if (maxInfoGain < infoGain)
                    {
                        maxInfoGain = infoGain;
                        _splitor->_splitPos = tempSplitor->_splitPos;
                        _splitor->_splitVal = tempSplitor->_splitVal;
                    }
                }
            }
        }
        if (tempSplitor != NULL)
            delete tempSplitor;
    }
    if (maxInfoGain > 0)
        evaluateSplitor(_splitor, dataSamples, leftTreeSamples, rightTreeSamples);
    else
    {
        delete leftTreeSamples;
        delete rightTreeSamples;
        leftTreeSamples = rightTreeSamples = NULL;
    }

    if(tempLeftTreeSamples != NULL) delete tempLeftTreeSamples;
    if(tempRightTreeSamples != NULL) delete tempRightTreeSamples;
}

// compute infomation gain
EntropyType TreeNode::evaluateSplitor(const TreeNodeSplitor *testSplitor,
                          DataSampleSet *dataSamples,
                          DataSampleSet *&leftTreeSamples,
                          DataSampleSet *&rightTreeSamples)
{
    leftTreeSamples->_ptSamples.clear();
    rightTreeSamples->_ptSamples.clear();

    for (int i = 0; i < dataSamples->getNumSamples(); ++i)
    {
        EntropyType prediction = testSplitor->classifySample(dataSamples->_ptSamples[i]);

        if (prediction >= 0)
            rightTreeSamples->insertSample(dataSamples->_ptSamples[i]);
        else
            leftTreeSamples->insertSample(dataSamples->_ptSamples[i]);
    }

    EntropyType infoGain = 0.0;

    if (rightTreeSamples->getNumSamples() > RandomTree::s_trainParameter._minLeafSize &&
            leftTreeSamples->getNumSamples() > RandomTree::s_trainParameter._minLeafSize)
    {
        EntropyType entropy = dataSamples->estimateEntropy(false);
        EntropyType leftRatio = EntropyType(leftTreeSamples->getNumSamples()) / EntropyType(dataSamples->getNumSamples());
        EntropyType leftEntropy = 0.0, rightEntropy = 0.0;

        int nLabels = dataSamples->getNumLabels();
        int nSamples = dataSamples->getNumSamples();
        int nLeftSamples = leftTreeSamples->getNumSamples();
        int nRightSamples = rightTreeSamples->getNumSamples();

        if (leftRatio < 0.5)
        {
            leftEntropy = leftTreeSamples->estimateEntropy(true);

            // compute right tree category distribution through left tree
            if (rightTreeSamples->_categoryDists == NULL)
                rightTreeSamples->_categoryDists = new EntropyType[nLabels];

            for (int i = 0; i < nLabels; ++i)
            {
                rightTreeSamples->_categoryDists[i] = dataSamples->_categoryDists[i] * nSamples
                        - leftTreeSamples->_categoryDists[i] * nLeftSamples;
                rightTreeSamples->_categoryDists[i] /= nRightSamples;


                rightEntropy += prob2entropy(rightTreeSamples->_categoryDists[i]); //-x*log2(x), if x == 0, return 0
            }
        }
        else
        {
            rightEntropy = rightTreeSamples->estimateEntropy(true);

            if (leftTreeSamples->_categoryDists == NULL)
                leftTreeSamples->_categoryDists = new EntropyType[nLabels];

            for (int i = 0; i < nLabels; ++i)
            {
                leftTreeSamples->_categoryDists[i] = dataSamples->_categoryDists[i] * nSamples
                        - rightTreeSamples->_categoryDists[i] * nRightSamples;
                leftTreeSamples->_categoryDists[i] /= nLeftSamples;

                leftEntropy += prob2entropy(leftTreeSamples->_categoryDists[i]);
            }
        }
        infoGain = entropy - leftRatio * leftEntropy - (1.0 - leftRatio) * rightEntropy;
    }
    return infoGain;
}

void TreeNode::generateLabelPartition(DataSampleSet *dataSamples, vector<int> &binarySequence, const int k)
{
    //ToDo
}

void TreeNode::saveNode(ostream &str)
{
    str<<"Node "<<_nodeIdx<<" "<< _level<<" "<<_nodeType<<" "<<_leafLabelIdx<<" "<<" "<<_purity<<" "<<_numSamples<<endl;
    if(this->isLeaf() == false)
    {
        str<<"Link "<<_nodeIdx<<" "<<_leftChild->getNodeIdx()<<" "<<_rightChild->getNodeIdx()<<endl;
        /*save split hyperplane*/
        _splitor->saveTreeNodeSplitor(str);
    }
}

bool TreeNode::loadNode(istream &str)
{
    int filelength = 1024*10;
    char temp[filelength];

    str.getline(temp,filelength);
    string textLine(temp);

    vector<string> textElement;
    boost::split(textElement, textLine, boost::is_any_of(" "), boost::token_compress_on);
    //BasicAPI::TextlineSplit(textLine,textElement,' ');

    _nodeIdx  = atoi(textElement.at(1).c_str());
    _level              = atoi(textElement.at(2).c_str());
    _nodeType    = TreeNodeType(atoi(textElement.at(3).c_str()));
    _leafLabelIdx     = atoi(textElement.at(4).c_str());

    std::istringstream stm;
    stm.str(textElement.at(5).c_str());
    stm >> _purity;

    if(_nodeType != TreeNode::NODE_LEAF)
    {
        /*load link*/
        str.getline(temp,filelength);
        textLine = string(temp);

        //BasicAPI::TextlineSplit(textLine,textElement,' ');
        boost::split(textElement, textLine, boost::is_any_of(" "), boost::token_compress_on);

        _leftChildIdx    = atoi(textElement.at(2).c_str());
        _rightChildIdx = atoi(textElement.at(3).c_str());

        if(_splitor != NULL) delete _splitor;
        _splitor = new TreeNodeSplitor();
        _splitor->loadTreeNodeSplitor(str);
    }

    return true;
}

//////////******************** RandomTree *********************************///////////////
RandomTree::RandomTree()
{

}

RandomTree::RandomTree(const int treeIdx)
    : _treeIdx(treeIdx)
{

}

RandomTree::~RandomTree()
{
    for (size_t i = 0; i < _nodes.size(); ++i)
    {
        if (_nodes[i] != NULL)
            delete _nodes[i];
    }
}

void RandomTree::constructTree(DataSampleSet *ptSampleSet, const int level)
{
    if (ptSampleSet->_categoryDists == NULL)
        ptSampleSet->estimatePurity();
    if (level == 0)
        _numSamples = ptSampleSet->getNumSamples();

    // compute the best purity
    EntropyType bestPurity = 0;
    int bestLabelIdx = 0;
    for (int i = 0; i < ptSampleSet->getNumLabels(); ++i)
    {
        if (bestPurity < ptSampleSet->_categoryDists[i])
        {
            bestPurity = ptSampleSet->_categoryDists[i];
            bestLabelIdx = i;
        }
    }

    // node
    TreeNode *ptNode = new TreeNode(_nodes.size());
    ptNode->setNodeLevel(level);
    ptNode->_numSamples = ptSampleSet->getNumSamples();
    ptNode->_purity = bestPurity;
    _nodes.push_back(ptNode);
    cout << "best pruity " << bestPurity << ", obj purity " << ptNode->_objectivePurity << endl;

    if (bestPurity < ptNode->_objectivePurity)  // if not achieve the objective purity
    {
        DataSampleSet* leftTreeSamples = NULL;
        DataSampleSet* rightTreeSamples = NULL;

        ptNode->constructSplitor(ptSampleSet, leftTreeSamples, rightTreeSamples);

        if (leftTreeSamples != NULL && rightTreeSamples != NULL) //spliting successful
        {
            // left sub tree
            ptNode->_leftChildIdx = _nodes.size();
            constructTree(leftTreeSamples, level+1);
            cout << ptNode->_nodeIdx << " left child " << ptNode->_leftChildIdx << endl;
            ptNode->_leftChild = _nodes[ptNode->_leftChildIdx];

            assert(ptNode->_leftChildIdx == ptNode->_leftChild->_nodeIdx);
            delete leftTreeSamples;
            leftTreeSamples = NULL;

            // right sub tree
            ptNode->_rightChildIdx = _nodes.size();
            constructTree(rightTreeSamples, level+1);
            cout << ptNode->_nodeIdx << " right child " << ptNode->_rightChildIdx << endl;
            ptNode->_rightChild = _nodes[ptNode->_rightChildIdx];

            assert(ptNode->_rightChildIdx == ptNode->_rightChild->_nodeIdx);
            delete rightTreeSamples;
            rightTreeSamples = NULL;
        }
        else //build a leaf, when no further split is possible
        {
            ptNode->setNodeType(TreeNode::NODE_LEAF);
            ptNode->setLeafLabelIdx(bestLabelIdx);
        }
    }
    else
    {
        ptNode->setNodeType(TreeNode::NODE_LEAF);
        ptNode->setLeafLabelIdx(bestLabelIdx);
    }
}

int RandomTree::classifySample(const DataSample *sample)
{
    assert(_nodes.size() > 0);
    TreeNode* ptNode = _nodes[0];

    while(!ptNode->isLeaf())    //if not leaf
    {
        if (ptNode->_splitor->classifySample(sample) == -1)
            ptNode = ptNode->_leftChild;
        else
            ptNode = ptNode->_rightChild;
    }
    return ptNode->getLeafLabelIdx();
}

void RandomTree::saveTree(ostream &str)
{
    str<<"& treeIndex: "<<_treeIdx<<endl;
    str<<"& number-of-training-samples: "<<_numSamples<<endl;
    str<<"& number-of-nodes: "<<_nodes.size()<<endl;

    for(size_t i = 0;i<_nodes.size(); ++i)
    {
        assert(_nodes.at(i)->getNodeIdx() == i);
        _nodes.at(i)->saveNode(str);
    }
}

bool RandomTree::loadTree(istream &str)
{
    int filelength = 1024*10;
    char temp[filelength];

    str.getline(temp,filelength);
    string textLine(temp);
    vector<string> textElement;
    //BasicAPI::TextlineSplit(textLine,textElement,' ');
    boost::split(textElement, textLine, boost::is_any_of(" "), boost::token_compress_on);
    _treeIdx = atoi(textElement.at(2).c_str());

    str.getline(temp,filelength);
    textLine = string(temp);
    //BasicAPI::TextlineSplit(textLine,textElement,' ');
    boost::split(textElement, textLine, boost::is_any_of(" "), boost::token_compress_on);
    _numSamples = atoi(textElement.at(2).c_str());

    /*load number of nodes*/
    str.getline(temp,filelength);
    textLine = string(temp);
    //BasicAPI::TextlineSplit(textLine,textElement,' ');
    boost::split(textElement, textLine, boost::is_any_of(" "), boost::token_compress_on);

    int numNodes = atoi(textElement.at(2).c_str());

    _nodes.clear();
    for(int i=0;i<numNodes;i++)
    {
        TreeNode* newNode = new TreeNode();
        _nodes.push_back(newNode);
    }

    for(size_t i=0;i<_nodes.size(); ++i)
    {
        _nodes.at(i)->loadNode(str);
        assert(_nodes.at(i)->_nodeIdx == i);
    }

    /*setup links*/
    for(size_t i=0; i<_nodes.size(); ++i)
    {
        if(_nodes.at(i)->isLeaf() == false)
        {
            _nodes.at(i)->_leftChild = _nodes.at(_nodes.at(i)->_leftChildIdx);
            _nodes.at(i)->_rightChild = _nodes.at(_nodes.at(i)->_rightChildIdx);
        }
    }
}

////////////*********************** RandomForest ************************//////////////////
RandomForest::RandomForest()
    : _numThreads(1)
{
}

RandomForest::RandomForest(const int numTreads)
    : _numThreads(numTreads)
{

}

RandomForest::~RandomForest()
{
    for (size_t i = 0; i < _trees.size(); ++i)
    {
        if (_trees[i] != NULL)
            delete _trees[i];
    }
}

void RandomForest::constructForest(DataSampleSet *ptSampleSet)
{
    ptSampleSet->searchActiveAttributes(s_activeAttributesIdxs);

    _numSamples = ptSampleSet->getNumSamples();
    _labelSet = ptSampleSet->_labelSet;

    TreeNodeSplitor::s_featDim = ptSampleSet->getSampleDataLength();

    RandomTree::s_trainParameter._numPosTry = int(TreeNodeSplitor::s_featDim * RandomTree::s_trainParameter._ratioPosTry);

    RandomForest::s_numSplitSegments = RandomTree::s_trainParameter._numSplitTry + 1;

    srand(time(NULL));

    if (RandomForest::s_trainParameter._isParalleTrain == false)
    {
        for (int i = 0; i < RandomForest::s_trainParameter._numTrees; ++i)
        {
            cout << "construct the " << (i+1) << "th tree" << endl;
            DataSampleSet *ptSelectSet = NULL;
            ptSampleSet->randomSample(RandomForest::s_trainParameter._treeSampleRatio, ptSelectSet);

            RandomTree* ptTree = new RandomTree(i);
            ptTree->constructTree(ptSelectSet);

            _trees.push_back(ptTree);

            if (ptSelectSet != NULL)
            {
                ptSelectSet->_ptSamples.clear(); //problem
                delete ptSelectSet;
            }
        }
    }
    else
    {
        omp_set_num_threads(_numThreads);
#pragma omp paralle for
        for (int i = 0; i < RandomForest::s_trainParameter._numTrees; ++i)
        {
            cout << "Construct the " << (i+1) << "th tree " << endl;
            DataSampleSet *ptSelectSet = NULL;
            ptSampleSet->randomSample(RandomForest::s_trainParameter._treeSampleRatio, ptSelectSet);

            RandomTree* ptTree = new RandomTree(i);
            ptTree->constructTree(ptSelectSet);

#pragma omp critical
                _trees.push_back(ptTree);
                if (ptSelectSet != NULL)
                {
                    ptSelectSet->_ptSamples.clear();    //problem
                    delete ptSelectSet;
                }
        }
    }
}

void RandomForest::predictClassProbs(const DataSample *sample, vector<float> &predictedProbs)
{

    assert(_labelSet.size() > 0);
    predictedProbs.clear();
    predictedProbs = vector<float>(_labelSet.size(), 0);

    if (RandomForest::s_trainParameter._isParalleTest == false)
    {
        for (size_t i = 0; i < _trees.size(); ++i)
        {
            int predictedLabelIdx = _trees[i]->classifySample(sample);
            cout << "tree " << i << ", predicte label " << predictedLabelIdx << endl;
            if (predictedLabelIdx >= 0)
                predictedProbs[predictedLabelIdx]++;
        }
    }
    else
    {
#pragma omp paralle for
        for (size_t i = 0; i < _trees.size(); ++i)
        {
            int predictedLabelIdx = _trees[i]->classifySample(sample);
            if (predictedLabelIdx >= 0)
                predictedProbs[predictedLabelIdx]++;
        }
    }

    // average all the trees
    for (size_t i = 0; i < _labelSet.size(); ++i)
        predictedProbs[i] /= (float)_trees.size();
}

int RandomForest::classifySample(const DataSample *sample)
{
    vector<float> predictedProbs;
    predictClassProbs(sample, predictedProbs);

    float maxProb = 0;
    int predictedLabelIdx = -1;

    for (size_t i = 0; i < _labelSet.size(); ++i)
    {
        cout << "label " << i << ", prob " << predictedProbs[i] << endl;
        if (maxProb < predictedProbs[i])
        {

            maxProb = predictedProbs[i];
            predictedLabelIdx = i;
        }
    }
    return predictedLabelIdx; //different
}

void RandomForest::classifySamples(DataSampleSet *samples)
{
    int numLabels = samples->getNumLabels();
    int numSamples = samples->getNumSamples();

    vector<vector<int> > confusionMatrix(numLabels, vector<int>(numLabels, 0));
    vector<int> categoryCount(numLabels, 0);

    for (int i = 0; i < numSamples; ++i)
    {
        int predictedLabelIdx = classifySample(samples->getSamplePtAt(i));
        int predictedLabel = _labelSet[predictedLabelIdx];

        int trueLabel = samples->getSamplePtAt(i)->getLabel();
        cout << "get label " <<trueLabel<< endl;
        int trueLabelIdx = samples->getLabelIdx(trueLabel);
        cout << "get label idx " << trueLabelIdx << endl;
        cout << "predicted label idx " << predictedLabelIdx<< endl;
        if (trueLabelIdx >= 0)
        {
            confusionMatrix[trueLabelIdx][predictedLabelIdx]++;
            categoryCount[trueLabelIdx]++;
        }
    }

    int correctSamples = 0;
    cout << "Test result " << endl;
    for (int i = 0; i < numLabels; ++i)
    {
        for (int j = 0; j < numLabels; ++j)
        {
            cout << confusionMatrix[i][j] << " ";
        }
        cout << endl;
        correctSamples += confusionMatrix[i][i];
    }

    if (numSamples > 0)
        cout << "Overall accuracy " << float(correctSamples) / float(numSamples) << endl;
}


//////// I O ////////////
void RandomForest::saveForest(string modelfilename)
{
    ofstream out;
    out.open(modelfilename.c_str());
    out<<"& number-of-training-samples: "<<this->_numSamples<<endl;
    out<<"& label-set: ";
    for(int i=0;i<this->_labelSet.size();i++) out<<_labelSet.at(i)<<" ";
    out<<endl;
    out<<"& feature-dim: "<<TreeNodeSplitor::s_featDim<<endl;

    /*save training parameters*/
    out<<"& number-of-tree: "<<RandomForest::s_trainParameter._numTrees<<endl;
    out<<"& tree-sample-ratio: "<<RandomForest::s_trainParameter._treeSampleRatio<<endl;
    out<<"& tree-max-depth: "<<RandomTree::s_trainParameter._maxDepth<<endl;
    out<<"& min-leaf-size: "<<RandomTree::s_trainParameter._minLeafSize<<endl;
    out<<"& splitor-type: "<<RandomTree::s_trainParameter._splitorType<<endl;

    for(int i = 0;i<this->_trees.size();i++)
    {
        _trees.at(i)->saveTree(out);
    }
    out.close();
}

bool RandomForest::loadForest(const string modelfilename)
{
    ifstream input;
    input.open(modelfilename.c_str());

    if(!input)
    {
        cout<<"load model from file failed: "<<modelfilename<<endl;
        return false;
    }

    int filelength = 1024*10;
    char temp[filelength];

    input.getline(temp,filelength);
    string textLine(temp);
    vector<string> textElement;
    //BasicAPI::TextlineSplit(textLine,textElement,' ');
    boost::split(textElement, textLine, boost::is_any_of(" "), boost::token_compress_on);
    _numSamples = atoi(textElement.at(2).c_str());

    _labelSet.clear();
    input.getline(temp,filelength);
    textLine = string(temp);
    //BasicAPI::TextlineSplit(textLine,textElement,' ');
    boost::split(textElement, textLine, boost::is_any_of(" "), boost::token_compress_on);
    for(int i = 2;i<textElement.size();i++)
    {
        _labelSet.push_back(atoi(textElement.at(i).c_str()));
    }

    input.getline(temp,filelength);
    textLine = string(temp);
    //BasicAPI::TextlineSplit(textLine,textElement,' ');
    boost::split(textElement, textLine, boost::is_any_of(" "), boost::token_compress_on);
    TreeNodeSplitor::s_featDim = atoi(textElement.at(2).c_str());

    input.getline(temp,filelength);
    textLine = string(temp);
    //BasicAPI::TextlineSplit(textLine,textElement,' ');
    boost::split(textElement, textLine, boost::is_any_of(" "), boost::token_compress_on);
    RandomForest::s_trainParameter._numTrees = atoi(textElement.at(2).c_str());

    input.getline(temp,filelength);
    textLine = string(temp);
    //BasicAPI::TextlineSplit(textLine,textElement,' ');
    boost::split(textElement, textLine, boost::is_any_of(" "), boost::token_compress_on);
    RandomForest::s_trainParameter._treeSampleRatio = atof(textElement.at(2).c_str());

    input.getline(temp,filelength);
    textLine = string(temp);
    //BasicAPI::TextlineSplit(textLine,textElement,' ');
    boost::split(textElement, textLine, boost::is_any_of(" "), boost::token_compress_on);
    RandomTree::s_trainParameter._maxDepth = atoi(textElement.at(2).c_str());

    input.getline(temp,filelength);
    textLine = string(temp);
    //BasicAPI::TextlineSplit(textLine,textElement,' ');
    boost::split(textElement, textLine, boost::is_any_of(" "), boost::token_compress_on);
    RandomTree::s_trainParameter._minLeafSize = atoi(textElement.at(1).c_str());

    input.getline(temp,filelength);
    textLine = string(temp);
    //BasicAPI::TextlineSplit(textLine,textElement,' ');
    boost::split(textElement, textLine, boost::is_any_of(" "), boost::token_compress_on);

    RandomTree::s_trainParameter._splitorType = TreeTrainParameters::SplitorType(atoi(textElement.at(2).c_str()));

    _trees.clear();
    for(int i = 0;i<RandomForest::s_trainParameter._numTrees;i++)
    {
        RandomTree* ptTree = new RandomTree();
        ptTree->loadTree(input);
        _trees.push_back(ptTree);
    }

    input.close();
    return true;
}
