#include "ColorCategory.h"
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <utility>
#include <boost/bind.hpp>

ColorCategory::ColorCategory()
    : _classLabel(-1), _r(0), _g(0), _b(0)
{
}


ColorCategoryTable::ColorCategoryTable()
{
}


int ColorCategoryTable::color2label(const int r, const int g, const int b) const
{
    int rgb =r*1e6+g*1e3+b;
    std::multimap<int,int>::const_iterator it = _colorCategoryMapping.find(rgb);

    if(it == _colorCategoryMapping.end())
        return -1;
    else return it->second;
}

int ColorCategoryTable::color2label(const cv::Vec3b bgr) const
{
    return this->color2label(int(bgr[2]), int(bgr[1]), int(bgr[0]));
}

int ColorCategoryTable::name2label(const std::string name) const
{
    std::multimap<std::string, ColorCategory>::const_iterator it = _nameColorMapping.find(name);
    if (it == _nameColorMapping.end())
        return -1;
    return it->second._classLabel;
}

std::string ColorCategoryTable::label2name(const int label) const
{
    std::multimap<int, ColorCategory>::const_iterator it = _labelColorMapping.find(label);
    if (it == _labelColorMapping.end())
        return "Unkown";
    else
        return it->second._className;
}


cv::Vec3b ColorCategoryTable::name2color(const std::string name) const
{
    std::multimap<std::string, ColorCategory>::const_iterator it = _nameColorMapping.find(name);
    if (it == _nameColorMapping.end())
        return cv::Vec3b(0,0,0);
    return cv::Vec3b((it->second)._b, (it->second)._g, (it->second)._r);
}

cv::Vec3b ColorCategoryTable::label2color(const int label) const
{
    std::multimap<int, ColorCategory>::const_iterator it = _labelColorMapping.find(label);
    if (it == _labelColorMapping.end())
        return cv::Vec3b(0,0,0);
    return cv::Vec3b((it->second)._b, (it->second)._g, (it->second)._r);
}

void ColorCategoryTable::clear()
{
    //_colorCategorySet.clear();
    _colorCategoryMapping.clear();
    _nameColorMapping.clear();
    _labelColorMapping.clear();
}

bool ColorCategoryTable::loadFile(std::string filename)
{
    std::cout<<"Loading color table..." << filename << std::endl;

    int filelength = 1024;
    std::ifstream inputFile;
    inputFile.open(filename.c_str());

    if(!inputFile)
    {
        std::cout<<"loading failed ... "<< std::endl;
        return false;
    }

    char temp[1024];
    while(!inputFile.eof())
    {
        inputFile.getline(temp,filelength);
        std::string textLine(temp);
        std::vector<std::string> textElement;

        boost::split(textElement, textLine, boost::is_any_of(" ,\t"), boost::token_compress_on);

        if (textLine.size() <= 0 || textLine[0] == '#') // for comment
            continue;

        // set category type
        if (textElement.size() >= 2)
        {
            std::string text0 = textElement[0];
            std::string text1 = textElement[1];
            boost::algorithm::to_lower(text0);
            boost::algorithm::to_lower(text1);
            if (text0 == "categorytype")
            {
                if (text1 == "species")
                    _categoryType = SPECIES;
                else if (text1 == "segmentation")
                    _categoryType = SEGMENTATION;
                continue;
            }
        }
        if(textElement.size()>4)
        {

            ColorCategory cc;
            cc._classLabel = atoi(textElement.at(0).c_str());
            cc._r = atoi(textElement.at(1).c_str());
            cc._g = atoi(textElement.at(2).c_str());
            cc._b = atoi(textElement.at(3).c_str());

            if(textElement.at(4).at(textElement.at(4).size()-1) == '\n')
            {
                cc._className = textElement.at(4).substr(0,textElement.at(4).size()-2);
            }
            else cc._className = textElement.at(4);

            if(cc._classLabel>=0)
            {
                std::cout<<"input class "<<cc._classLabel<<" RGB("<<cc._r<<","<<cc._g<<","<<cc._b<<")"<<" "<<cc._className<< std::endl;
                int rgb = cc._r*1e6+cc._g*1e3+cc._b;

                //cc._classLabel = _colorCategorySet.size();
                std::pair<int,int> projection(rgb,cc._classLabel);

                //_colorCategorySet.push_back(cc);
                _colorCategoryMapping.insert(projection);

                _nameColorMapping.insert(std::make_pair(cc._className, cc));
                _labelColorMapping.insert(std::make_pair(cc._classLabel, cc));
            }
        }
    }
    inputFile.close();

    //_numCategory = _colorCategorySet.size();

    std::vector<std::pair<int, ColorCategory> > labelColorPairs;
    for (std::multimap<int,ColorCategory>::iterator it = _labelColorMapping.begin(); it != _labelColorMapping.end(); ++it)
    {
        std::pair<int, ColorCategory> pa = std::make_pair(it->first, it->second);
        labelColorPairs.push_back(pa);
        //std::cout << "% label " << pa.first << ", " << pa.second._classLabel << ", " << pa.second._className << std::endl;
    }
    std::sort(labelColorPairs.begin(), labelColorPairs.end(),
              boost::bind(&std::pair<int,ColorCategory>::first, _1) <
              boost::bind(&std::pair<int,ColorCategory>::first, _2));
    _labelColorMapping.clear();
    for (int i = 0; i < labelColorPairs.size(); ++i)
    {
        _labelColorMapping.insert(labelColorPairs[i]);
        //std::cout << "label " << i << ": " << _labelColorMapping[i]._className << std::endl;
        //std::cout << "* label " << labelColorPairs[i].first << ": " <<labelColorPairs[i].second._classLabel<< ", " << labelColorPairs[i].second._className << std::endl;
    }
//    for (std::multimap<int,ColorCategory>::const_iterator it = _labelColorMapping.begin(); it != _labelColorMapping.end(); ++it)
//    {
//        std::cout << "$label " << it->first << ", " << it->second._className << std::endl;
//    }
    print();
}

std::vector<std::string> ColorCategoryTable::getSortedClassNames() const
{
    std::vector<std::string> names;
    for (std::multimap<int,ColorCategory>::const_iterator it = _labelColorMapping.begin(); it != _labelColorMapping.end(); ++it)
    {
        names.push_back(it->second._className);
    }
    return names;
}

int ColorCategoryTable::nCategory() const
{
    return _labelColorMapping.size();
}

void ColorCategoryTable::setCategoryType(const CategoryType type)
{
    _categoryType = type;
}

void ColorCategoryTable::print() const
{
    std::cout << "######### num of class " << this->nCategory() << "  ################### " << std::endl;
    for (std::multimap<int,ColorCategory>::const_iterator it = _labelColorMapping.begin(); it != _labelColorMapping.end(); ++it)
    {
        const ColorCategory& cc = it->second;
        std::cout<<"class "<<it->first<<" RGB("<<cc._r<<","<<cc._g<<","<<cc._b<<")"<<" "<<cc._className<< std::endl;
    }
}
