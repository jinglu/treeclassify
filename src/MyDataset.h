#ifndef MYDATASET_H
#define MYDATASET_H

#include "TreeCategory.h"
#include "PixelImage.h"
#include "ColorCategory.h"
#include <vector>
#include <string>


enum weightType{staticType,active,nonactive};

class MyDataset
{
public:
    enum IMAGETYPE {TRAINTYPE, TESTTYPE};
    enum MainOperation {TREESEGEMNTATION, TREESPECIESCLASSIFICATION};

    MyDataset();
    void setDirs(const std::string rootpath);
    void configure();
    void loadSettings(const std::string setname);

    void loadImageList(std::string listFile, const int imageType);
    void loadBarkList(std::string barkFile);

    static std::string getFeatName(const std::vector<FeatureParameter>& featParas);

    std::string getParsModelName() const;

    static weightType string2weightType(const std::string str);
    static MainOperation string2operation(const std::string str);

    ////////////////////
public:

    /* directories
     */
    std::string _rootDir;
    std::string _featureDir;
    std::string _featurePCADir;
    std::string _edgeDir;
    std::string _confMapDir;
    std::string _classifierDir;
    std::string _resultDir;
    std::string _bowDir;
    std::string _clusterDir;
    std::string _targetDir;

    /* inputs
     */
    std::vector<BasicImage> _trainImages;
    std::vector<BasicImage> _testImages;
    std::vector<BasicImage> _barkImages;

    //TreeCategory _treeCategory;
    std::string _modelName;
    std::string _objModelName;
    MainOperation _operation;

    ColorCategoryTable _parsColorTable;
    ColorCategoryTable _speciesColorTable;

    //  bag of words
    std::string _bowname;
    std::string _vocname;
    std::string _bowsample;
    std::string _svmfile;

    /* Feature Parameters
     */
    std::vector<FeatureParameter> _featParameters;
    std::vector<FeatureParameter> _objFeatParameters;
    std::vector<FeatureParameter> _barkFeatParameters;
    std::vector<FeatureParameter> _parsParameters;

    /* parameters configuration
     */
    int _numMaxTrainSamples;
    int _numThreads;

    bool _withPixelLevelfeatures;    // add pixel level feature
    bool _withSPLevelfeatures;       // add super-pixel level feature for each patch level feature or not

    /* flags
     */
    bool _isSaveConfidenceMap;
    bool _isLoadConfidenceMap;

    /* MRF parameters
     */
    weightType _layerUnary;
    weightType _layerPairwise;
    weightType _uplayerUnary;
    weightType _uplayerPairwise;
    weightType _superlayerUnary;
    weightType _superlayerPairwise;
    weightType _constraintlayer;
    weightType _up_layer_Pairwise;
    weightType _super_up_layerPairwise;
    weightType _super_layer_Pairwise;

    bool _withUnLabelRegion;
    bool _loadLearnedWeight;
    bool _randomInitialization;
    float _trainRatio;

    bool _optimizeAverageRecall; // use -averagerecall as optimization criterion

    /* random parameter
     */
    cv::RNG _cvrng;
    /* ouputs
     */
    cv::Mat _confusionMatrix;
};

#endif // MYDATASET_H
