#ifndef CLUSTERING_H
#define CLUSTERING_H

#include "Feature.h"
#include "PixelImage.h"

#include <Image/Color.h>

class Clustering
{
public:
    Clustering();

    void constructColorTable(const std::string tablename);


    void kmeanCluster(float *trainData,
                      const int featDim,
                      const int numSamples,
                      const int numCluster,
                      const int maxIteration,
                      std::vector<std::vector<float> >& centers);

    void clusterSamples(const std::vector<FeatVector>& sampleFeatureSet,
                        const int numClusters,
                        std::vector<std::vector<float> >& centers);

    void objLevelCluster(std::vector<BasicImage>& images, const int numCluster, const cv::Vec3b& activeColor);

    void objLevelClusterPCA(const std::vector<BasicImage>& images,
                              const std::vector<FeatVector>& sampleFeatureSet,
                              const std::vector<FeatVector>& centerFeatures,
                              const int numComponent);

public:
    std::vector<wjl::Color>_colorTable;

};

#endif // CLUSTERING_H
