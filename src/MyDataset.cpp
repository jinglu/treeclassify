#include "GloabalVal.h"
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>
namespace po = boost::program_options;
using namespace std;

MyDataset::MyDataset()
{
    _cvrng = cv::RNG(0xFFFFFFFF);
}

void MyDataset::configure()
{
    //string rootpath = "/mnt/Udata/Workspace_Data/TreeClassify_data/";
    loadSettings("setting/settings.ini");
    //setDirs(_rootDir);

    string trainlist = _rootDir + "/file/trainlist.txt";
    string testlist = _rootDir+"/file/testlist.txt";
    string barklist = _rootDir + "/file/barklist.txt";
    string parscolor_name = _rootDir+"/file/pars_color_table.txt";
    string speciescolor_name = _rootDir + "/file/species_color_table.txt";

    cout << "load train list " << trainlist << endl;
    loadImageList(trainlist, MyDataset::TRAINTYPE);

    cout << "load test list " << testlist << endl;
    loadImageList(testlist, MyDataset::TESTTYPE);

    cout << "load bark list " << barklist << endl;
    loadBarkList(barklist);

    _parsColorTable.loadFile(parscolor_name);
    _parsColorTable.setCategoryType(ColorCategoryTable::SEGMENTATION);
    _speciesColorTable.loadFile(speciescolor_name);
    _speciesColorTable.setCategoryType(ColorCategoryTable::SPECIES);

    // feature parameter
    FeatureParameter featParaShapeMask;
    featParaShapeMask._featName = FeatureParameter::SHAPEMASK;
    featParaShapeMask._featLevel = FeatureParameter::OBJECTLEVEL;
    _objFeatParameters.push_back(featParaShapeMask);

    FeatureParameter featParaMoment;
    featParaMoment._featName = FeatureParameter::SHAPEMOMENT;
    featParaMoment._featLevel = FeatureParameter::OBJECTLEVEL;
    //_objFeatParameters.push_back(featParaMoment);

    FeatureParameter featParaEH;
    featParaEH._featName = FeatureParameter::EDGEHIST;
    featParaEH._featLevel = FeatureParameter::OBJECTLEVEL;
    _objFeatParameters.push_back(featParaEH);

    /////////////////////
    FeatureParameter featParaTexton;
    featParaTexton._featName = FeatureParameter::TEXTON;
    featParaTexton._featLevel = FeatureParameter::PIXELLEVLE;
    featParaTexton._sampleStep = 1;
    featParaTexton._numScales = 2;
    featParaTexton._patchSize = 9;
    _featParameters.push_back(featParaTexton);


    FeatureParameter featParaHog;
    featParaHog._featName = FeatureParameter::HOG;
    featParaHog._featLevel = FeatureParameter::PIXELLEVLE;
    featParaHog._sampleStep = 1;
    featParaHog._numScales = 2;
    featParaHog._patchSize = 9;
    //_featParameters.push_back(featParaHog);

    FeatureParameter featParaSift;
    featParaSift._featName = FeatureParameter::SIFT;
    featParaSift._featLevel = FeatureParameter::PIXELLEVLE;
    featParaSift._sampleStep = 1;
    featParaSift._numScales = 1;
    featParaSift._patchSize = 9;
    _featParameters.push_back(featParaSift);

    FeatureParameter feat_paraColor;
    feat_paraColor._featName = FeatureParameter::COLOR;
    feat_paraColor._featLevel = FeatureParameter::PIXELLEVLE;
    feat_paraColor._sampleStep = 1;
    feat_paraColor._numScales = 1;
    feat_paraColor._patchSize = 9;
    _barkFeatParameters.push_back(feat_paraColor);

    _parsParameters.push_back(featParaSift);

        // model name
    std::string featNames = "_";
    for (size_t i = 0; i < _featParameters.size(); ++i)
        featNames = featNames + _featParameters[i].strName() + "_";
    _modelName = _classifierDir + "tree" + featNames + "Pixel_RF.model";

    featNames = "_";
    for (size_t i = 0; i < _objFeatParameters.size(); ++i)
        featNames = featNames + _objFeatParameters[i].strName() + "_";
    _objModelName = _classifierDir + "tree" + featNames + "Object_RF.model";

    // parameters
    //_numMaxTrainSamples = 100000;
    //_numThreads = 1;
}

void MyDataset::loadSettings(const string setname)
{
    std::ifstream fin(setname.c_str());
    assert(fin.is_open());

    po::options_description config_desc("Configuration");

    config_desc.add_options()
            ("rootpath", po::value<string>(&_rootDir), "root path")
            ("bowname", po::value<string>(), "bag of words, training descriptors name")
            ("vocname", po::value<string>(), "bag of words, vocabulary name")
            ("bowsample", po::value<string>(), "bag of words, sample name")
            ("svmfile", po::value<string>(), "bag of words, svm files")
            ("withPixelLevelfeatures", po::value<bool>(&_withPixelLevelfeatures), "if with pixel level features")
            ("withSPLevelfeatures", po::value<bool>(&_withSPLevelfeatures), "if with super-pixel level features")
            ("layerUnary", po::value<string>(), "CRF unary layer type")
            ("layerPairwise", po::value<string>(), "CRF layer pair wise type")
            ("uplayerUnary", po::value<string>(), "CRF uper layer unary type")
            ("uplayerPairwise", po::value<string>(), "CRF uper layer pair wise type")
            ("superlayerUnary", po::value<string>(), "CRF super layer unary type")
            ("superlayerPairwise", po::value<string>(), "CRF super layer pair wise type")
            ("constraintlayer", po::value<string>(), "CRF constraintlayer")
            ("up_layer_Pairwise", po::value<string>(), "CRF up layer pair wise type")
            ("super_up_layerPairwise", po::value<string>(), "CRF up layer pair wise type")
            ("super_layer_Pairwise", po::value<string>(), "CRF super_alyer pair wise")
            ("randomInitialization", po::value<bool>(&_randomInitialization), "is random initailization")
            ("withUnLabelRegion", po::value<bool>(&_withUnLabelRegion), "if with un labeld region")
            ("numMaxTrainSamples", po::value<int>(&_numMaxTrainSamples), "number of max train samples")
            ("numThreads", po::value<int>(&_numThreads), "number of threads")
            ("isSaveConfidenceMap", po::value<bool>(&_isSaveConfidenceMap), "if save confidenceMap")
            ("isLoadConfidenceMap", po::value<bool>(&_isLoadConfidenceMap), "if load confidenceMap")
            ("operation", po::value<string>(), "Main operation");


    po::variables_map config_vm;
    po::parsed_options config_opts = po::parse_config_file(fin, config_desc);
    po::store(config_opts, config_vm);

    fin.close();
    po::notify(config_vm);

    //config_desc.print(cout);

    if (config_vm.count("rootpath"))
    {
        setDirs(_rootDir);
    }
    if (config_vm.count("bowname"))
    {
        _bowname = _bowDir + config_vm["bowname"].as<string>();
        cout << "bow name " << _bowname << endl;
    }
    if (config_vm.count("vocname"))
    {
        _vocname = _bowDir + config_vm["vocname"].as<string>();
    }
    if (config_vm.count("bowsample"))
    {
        _bowsample = _bowDir + config_vm["bowsample"].as<string>();
    }
    if (config_vm.count("svmfile"))
        _svmfile = _bowDir + config_vm["svmfile"].as<string>();

    if (config_vm.count("layerUnary"))
        _layerUnary = MyDataset::string2weightType(config_vm["layerUnary"].as<string>());
    if (config_vm.count("layerPairwise"))
        _layerPairwise = MyDataset::string2weightType(config_vm["layerPairwise"].as<string>());
    if (config_vm.count("uplayerUnary"))
        _uplayerUnary = MyDataset::string2weightType(config_vm["uplayerUnary"].as<string>());
    if (config_vm.count("uplayerPairwise"))
        _uplayerPairwise = MyDataset::string2weightType(config_vm["uplayerPairwise"].as<string>());
    if (config_vm.count("superlayerUnary"))
        _superlayerUnary = MyDataset::string2weightType(config_vm["superlayerUnary"].as<string>());
    if (config_vm.count("superlayerPairwise"))
        _superlayerPairwise = MyDataset::string2weightType(config_vm["superlayerPairwise"].as<string>());
    if (config_vm.count("constraintlayer"))
        _constraintlayer = MyDataset::string2weightType(config_vm["constraintlayer"].as<string>());
    if (config_vm.count("up_layer_Pairwise"))
        _up_layer_Pairwise = MyDataset::string2weightType(config_vm["up_layer_Pairwise"].as<string>());
    if (config_vm.count("super_up_layerPairwise"))
        _super_up_layerPairwise = MyDataset::string2weightType(config_vm["super_up_layerPairwise"].as<string>());
    if (config_vm.count("super_layer_Pairwise"))
        _super_layer_Pairwise = MyDataset::string2weightType(config_vm["super_layer_Pairwise"].as<string>());

    if (config_vm.count("operation"))
        _operation = MyDataset::string2operation(config_vm["operation"].as<string>());


    typedef std::vector< po::basic_option<char> > vec_opt;
    cout << "Parsed coinfigure file options (from parsed_options):" << endl;

    for(vec_opt::iterator l_itrOpt = config_opts.options.begin();
         l_itrOpt != config_opts.options.end();
         ++l_itrOpt)
    {
        po::basic_option<char>& l_option = *l_itrOpt;
        cout << "\t" << l_option.string_key << ": ";
        typedef std::vector< std::basic_string<char> > vec_string;
        for(vec_string::iterator l_itrString = l_option.value.begin();
            l_itrString != l_option.value.end();
            ++l_itrString)
        {
            cout << *l_itrString;
        }
        cout << endl;
    }

}

weightType MyDataset::string2weightType(const string str)
{
    if (str == "s")
        return staticType;
    else if (str == "a")
        return active;
    else if (str == "n")
        return nonactive;
}

MyDataset::MainOperation MyDataset::string2operation(const string str)
{
    if (str == "TREESEGEMNTATION")
        return MyDataset::TREESEGEMNTATION;
    else if (str == "TREESPECIESCLASSIFICATION")
        return MyDataset::TREESPECIESCLASSIFICATION;
}

std::string MyDataset::getFeatName(const vector<FeatureParameter> &featParas)
{
    std::string featNames = "";
    for (size_t i = 0; i < featParas.size(); ++i)
        featNames = featNames + "_"+ featParas[i].strName();
    return featNames;
}

std::string MyDataset::getParsModelName() const
{
    std::string ret = this->_classifierDir + "pars" + MyDataset::getFeatName(this->_featParameters) + "_RF.model";
    return ret;
}
void MyDataset::setDirs(const string rootpath)
{
    _rootDir = rootpath;
    _featureDir = rootpath + "/feature/";
    _featurePCADir = rootpath + "/featurePCA/";
    _edgeDir = rootpath + "/data/edge/";
    _confMapDir = rootpath + "/confMap/";
    _classifierDir = rootpath + "/classifier/";
    _resultDir = rootpath + "/result/";
    _bowDir = rootpath + "/bow/";
    _clusterDir = rootpath + "/cluster/";
    _targetDir = rootpath + "/data/target/";
}

void MyDataset::loadImageList(string listFile, const int imageType)
{
    ifstream fin;
    fin.open(listFile.c_str());

    assert(fin.is_open());

    vector<BasicImage> inputImageList;
    BasicImage bimg;

    int lid = 0;
    int previous = 0;

    while(!fin.eof())
    {
        std::string str;
        getline(fin, str);
        std::vector<std::string> strlist;
        boost::split(strlist, str, boost::is_any_of(" "));

        if (strlist.size() >= 2)
        {
            //cout << lid << endl;
            if (strlist.at(0) == "image")
            {
                if (previous == 1)
                {
                    bimg.setMaskName("");
                    inputImageList.push_back(bimg);
                    //cout << "push " << bimg._filenameImage << "\n"<< bimg._filenameMask<<endl;
                }
                bimg._filenameImage = strlist.at(1);
                //cout << "before p = " << previous;
                previous = 1;
                //cout <<" image " <<strlist[1] <<", p = " << previous<< endl;
            }
            else if (strlist.at(0) == "mask")
            {
                if (previous == 1)
                {
                    bimg._filenameMask = strlist.at(1);
                    inputImageList.push_back(bimg);
                    //cout << "push " << bimg._filenameImage << "\n"<< bimg._filenameMask<<endl;
                }
                //cout << "before p = " << previous;
                previous = 2;
                //cout <<" mask " <<strlist[1] <<", p = " <<previous << endl;
            }
            lid++;
        }
    }

    // initial image type, train or test
    bool isTrain = (imageType == MyDataset::TRAINTYPE) ? true : false;


    for (size_t i = 0; i < inputImageList.size(); ++i)
    {
        inputImageList[i]._isTrain = isTrain;
    }

    if (isTrain)
    {
        _trainImages = inputImageList;
        //cout << "trainImage size " << _trainImages.size() <<", input size " << inputImageList.size()<< endl;
    }
    else
    {
        _testImages = inputImageList;
        //cout << "testImage size " << _trainImages.size() <<", input size " << inputImageList.size()<< endl;
    }

}

void MyDataset::loadBarkList(string barkFile)
{
    ifstream fin(barkFile.c_str());
    assert(fin.is_open());

    _barkImages.clear();

    while(!fin.eof())
    {
        string line;
        getline(fin, line);
        if (line.find(".png") != string::npos)
        {
            BasicImage bark_img;
            bark_img.setImageName(line);
            _barkImages.push_back(bark_img);
            //cout <<"bark: " << line << endl;
        }
    }
    fin.close();
}
