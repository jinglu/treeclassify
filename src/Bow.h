#ifndef BOW_H
#define BOW_H

#include "PixelImage.h"
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/ml/ml.hpp>

class Bow
{
public:
    Bow();

    void buildvoc(const std::vector<BasicImage>& trainImages,
                  const std::string bowname,
                  const std::string vocname,
                  const cv::Vec3b& activeColor);

    void trainbow(const std::vector<BasicImage> &trainImages,
                  const std::string vocname,
                  const std::string samplename,
                  const cv::Vec3b& activeColor);

    void extract_training_samples(const std::string samplename,
                                  const std::vector<BasicImage>& trainImages,
                                  cv::Ptr<cv::FeatureDetector>& detector,
                                  cv::BOWImgDescriptorExtractor& bowide,
                                  std::map<std::string, cv::Mat>& classes_training_data,
                                  const cv::Vec3b& activeColor);

    void trainSVM(const std::string svmfile,
                  std::map<std::string, cv::Mat>& classes_training_data,
                  const int response_cols,
                  const int response_type);

    void test_classifiers(const std::vector<BasicImage>& testImages, const cv::Vec3b& activeColor);

    void loadSVM(std::string svmfile);
    void loadVocabulary(std::string vocname);

    void evaluateOneImage(const PixelImage& pimg,
                          std::vector<std::string>& out_classes, cv::BOWImgDescriptorExtractor & bowide);

    void testInitialize();

    std::map<std::string,CvSVM> _classes_classifiers;
    std::map<std::string,cv::Scalar> _classes_colors;

    cv::Mat _vocabulary;
    cv::Ptr<cv::FeatureDetector > _detector;
    cv::Ptr<cv::DescriptorMatcher > _matcher;
    cv::Ptr<cv::DescriptorExtractor > _extractor;
    cv::Ptr<cv::BOWImgDescriptorExtractor > _bowide;
};

#endif // BOW_H
