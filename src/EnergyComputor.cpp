#include "EnergyComputor.h"
#include <Primitives/Signal.h>
#include "GloabalVal.h"

EnergyComputor::EnergyComputor()
{
}


float EnergyComputor::computeHomogeneity(const cv::Mat& probMat,
                                         const wjl::Rectangle& rect,
                                         const float threshold) const
{
    cv::Mat roi(probMat, rect._cvrect);

    float meanv = cv::mean(roi)[0];

    std::vector<float> sig;
    for (unsigned int i = 0; i < roi.rows; ++i)
        for (unsigned int j = 0; j < roi.cols; ++j)
            sig.push_back(roi.at<float>(i,j));

    //homogeneity rate
    int hr = 0;
    for (int i = 0; i < sig.size(); ++i)
        if (fabs(sig.at(i) - meanv) < threshold)
            hr++;

    float ret = (float)hr / (float)rect.area();
    if (true)
        std::cout << "homogeneity rate = " << ret << std::endl;
    return ret;
}


float EnergyComputor::computeConstrast_term1(const cv::Mat &probMat,
                                       const wjl::Rectangle &rect,
                                       const int margin)
{
    int win_x = rect.x();
    int win_y = rect.y();
    int win_w = rect.width();
    int win_h = rect.height();

    wjl::Rectangle bound_rect = rect.dilate(margin, margin);

    bound_rect &= wjl::Rectangle(0,0,probMat.cols, probMat.rows);

    cv::Mat roi(probMat, bound_rect._cvrect);

    wjl::Signal<float> sig;

    for (unsigned int i = 0; i < roi.rows; ++i)
        for (unsigned int j = 0; j < roi.cols; ++j)
            sig.push_back(roi.at<float>(i,j));

    wjl::Signal<float> sig_in, sig_out;

    int new_w = bound_rect.width();
    int new_h = bound_rect.height();

    //rect.print();
    bound_rect.print();
    for (int x = 0; x < new_w; ++x)
        for (int y = 0; y < new_h; ++y)
        {
            //std::cout << "idx " << y * new_w + x << std::endl;
            if (x >= margin && x < win_w + margin && y >= margin && y < margin+win_h)
                sig_in.push_back(sig.at(y * new_w + x));
            else
                sig_out.push_back(sig.at(y * new_w + x));

        }

    float m1, m2;

    float sigma1 = sig_in.stdev();
    float sigma2 = sig_out.stdev();

    float mean1 = sig_in.mean();
    float mean2 = sig_out.mean();

    //std::cout << "signal inside mean = " << mean1 << ", stdev = " << sigma1 << std::endl;
    //std::cout << "signal outside mean = " << mean2 << ", stdev = " << sigma2 << std::endl;

    m1 = (mean1 - mean2) * (mean1 - mean2);
    m1 = m1/(2*sqrt(sigma1*sigma1+sigma2*sigma2));
    m2 = sigma1*sigma2;
    m2 = m2/(sigma1*sigma1+sigma2*sigma2);
    m2 = log(m2+STD_SQUARE_NORM_TOLERANCE);

    float ret = m1-m2;

    if (true)
        std::cout << "energy_fenetre2 contrast rate = " << ret << std::endl;
    return ret;
}

float EnergyComputor::computeConstrast(const cv::Mat &probMat, const wjl::Rectangle &rect, const int margin, const float d)
{
    float m = this->computeConstrast_term1(probMat, rect, margin);

    float ret;
    if (m > d)
        ret = exp(-(m-d)/d)-1;
    else
        ret = 1.0 - m/d;

    //if (IF_DEBUG == 1)
        std::cout << "energy fenetre3 contrast = " << ret << std::endl;
    return ret;
}

float EnergyComputor::computeEnergy1(const cv::Mat &probMat, const wjl::Rectangle &rect, const float homoThresh, const int contrastMargin)
{
    cv::Mat roi(probMat, rect._cvrect);

    float ret = -(cv::sum(roi)[0]) + computeHomogeneity(probMat, rect, homoThresh) + computeConstrast(probMat, rect, contrastMargin, 3.0);

    return ret;

}
