#ifndef RANDOMFOREST_H
#define RANDOMFOREST_H

#include "DataSample.h"

////////////////////////////////////////////////////////////


class ForestTrainParameters {
public:
    ForestTrainParameters();

public:
    int _numTrees;
    float _treeSampleRatio; //random sample ratio for training a single tree
    bool _isParalleTrain;
    bool _isParalleTest;
};

/////////////////////////////////////////////////////////////
class TreeTrainParameters {
public:
    enum SplitorType {SPLITOR_Stump, SPLITOR_HyperPlane};
    TreeTrainParameters();

public:
    SplitorType _splitorType;
    int _maxDepth;      // maximum depth of a tree
    int _minLeafSize;   // minimum number of trining samples that support a leaf
    int _numSplitTry;   // number of trying different split values on one position
    int _numPosTry;     // number of trying different position
    float _ratioPosTry; // ratio of tried postion among all attributes
};

///////////////////////////////////////////////////////////
class TreeNodeSplitor {
public:
    TreeNodeSplitor();

    virtual ~TreeNodeSplitor();

    inline int classifySample(const DataSample* sample) const;

    /* IO
     */
    void saveTreeNodeSplitor(std::ostream& str);
    bool loadTreeNodeSplitor(std::istream& str);

public:
    int _splitPos;
    DataType _splitVal;

    static int s_featDim;
};

//////////////////////////////////////////////
class TreeNode {
public:
    enum TreeNodeType {NODE_INTERNAL, NODE_LEAF};
    TreeNode();
    TreeNode(const int nodeIdx);

    virtual ~TreeNode();

    int getNodeLevel() const;
    void setNodeLevel(const int level);

    int getNodeIdx() const;
    void setNodeIdx(const int idx);

    TreeNodeType getNodeType() const;
    void setNodeType(const TreeNodeType type);

    int getLeafLabelIdx() const;
    void setLeafLabelIdx(const int labelIdx);

    bool isLeaf() const;

    // construct tree node splitor
    void constructSplitor(DataSampleSet* dataSamples,
                          DataSampleSet*& leftTreeSamples,
                          DataSampleSet*& rightTreeSamples);

    EntropyType evaluateSplitor(const TreeNodeSplitor* testSplitor,
                                DataSampleSet* dataSamples,
                                DataSampleSet*& leftTreeSamples,
                                DataSampleSet*& rightTreeSamples);

    // binary partition of label space, for a binary SVM training
    void generateLabelPartition(DataSampleSet* dataSamples,
                                std::vector<int>& binarySequence,
                                const int k);

    /* I O
     */
    void saveNode(std::ostream& str);
    bool loadNode(std::istream& str);

public:
    int _numSamples;
    int _nodeIdx;
    int _level;
    int _leafLabelIdx;
    TreeNodeType _nodeType;

    int _leftChildIdx;
    int _rightChildIdx;

    int _purity;
    EntropyType _objectivePurity;
    TreeNodeSplitor* _splitor;
    TreeNode* _leftChild;
    TreeNode* _rightChild;

};
//////////////////////////////////////////////////
class RandomTree {
public:
    RandomTree();
    RandomTree(const int treeIdx);

    virtual ~RandomTree();


    void constructTree(DataSampleSet* samples, const int level = 0);

    int classifySample(const DataSample* sample);

    /* I O
     */
    void saveTree(std::ostream& str);
    bool loadTree(std::istream& str);

public:
    std::vector<TreeNode *> _nodes;
    int _treeIdx;
    int _numSamples;

    static TreeTrainParameters s_trainParameter;
};
/////////////////////////////////////////////////////////////////
class RandomForest
{
public:
    RandomForest();
    RandomForest(const int numTreads);

    virtual ~RandomForest();

    void constructForest(DataSampleSet *ptSampleSet);

    void predictClassProbs(const DataSample* sample, std::vector<float>& predictedProbs);
    int classifySample(const DataSample* sample);
    void classifySamples(DataSampleSet* samples);

    /* IO
     */
    void saveForest(const std::string modelfilename);
    bool loadForest(const std::string modelfilename);


public:
    int _numSamples;
    std::vector<RandomTree *> _trees;
    std::vector<int> _labelSet;

    int _numThreads;

    static int s_numSplitSegments;  //number of partitions for each attribute
    static ForestTrainParameters s_trainParameter;
    static std::vector<int> s_activeAttributesIdxs;
};

#endif // RANDOMFOREST_H
