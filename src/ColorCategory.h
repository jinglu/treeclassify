#ifndef COLORCATEGORY_H
#define COLORCATEGORY_H

#include <vector>
#include <map>
#include <string>
#include <opencv2/core/core.hpp>

class ColorCategory
{
public:
    ColorCategory();

public:
    int _r, _g, _b;
    int _classLabel;
    std::string _className;
};

class ColorCategoryTable
{
public:
    enum CategoryType{SPECIES,SEGMENTATION};

    ColorCategoryTable();

    /* switch members
     */
    int color2label(const int r, const int g, const int b) const;
    int color2label(const cv::Vec3b bgr) const;
    int name2label(const std::string name) const;
    std::string label2name(const int label) const;
    cv::Vec3b name2color(const std::string name) const;
    cv::Vec3b label2color(const int label) const;


    /* I O
     */
    bool loadFile(std::string filename);
    void print() const;

    void clear();

    /* Basic Info
     */
    std::vector<std::string> getSortedClassNames() const;
    int nCategory() const;

    void setCategoryType(const CategoryType type);



public:
    //int _numCategory;
    //std::vector<ColorCategory> _colorCategorySet;
    std::multimap<int,int> _colorCategoryMapping;
    std::multimap<std::string, ColorCategory> _nameColorMapping;
    std::map<int, ColorCategory> _labelColorMapping;

    CategoryType _categoryType;
};

#endif // COLORCATEGORY_H
