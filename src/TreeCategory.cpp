#include "TreeCategory.h"
using namespace std;
TreeCategory::TreeCategory()
{
    constructTable();
}

void TreeCategory::constructTable()
{

    _idNameTable.insert(std::pair<int,string>(0,"oak"));
    _idNameTable.insert(std::pair<int,string>(1,"pine"));
    _idNameTable.insert(std::pair<int,string>(2,"palm"));
    _idNameTable.insert(std::pair<int,string>(3,"maple"));
    _idNameTable.insert(std::pair<int,string>(4,"willow"));

    _idColorTable.insert(std::pair<int,cv::Vec3b>(0,cv::Vec3b(128,128,64)));
    _idColorTable.insert(std::pair<int,cv::Vec3b>(1,cv::Vec3b(0,128,0)));
    _idColorTable.insert(std::pair<int,cv::Vec3b>(2,cv::Vec3b(128,0,0)));
    _idColorTable.insert(std::pair<int,cv::Vec3b>(3,cv::Vec3b(0,0,128)));
    _idColorTable.insert(std::pair<int,cv::Vec3b>(4,cv::Vec3b(128,128,128)));

    // sort table by id

}

int TreeCategory::name2id(string name)
{
    for (map<int,string>::iterator it = _idNameTable.begin(); it != _idNameTable.end(); ++it)
    {
        if (it->second == name)
            return it->first;
    }
}

string TreeCategory::id2name(int id)
{
    if (id < 0)
        return "NONE";
    return _idNameTable[id];
}

cv::Vec3b TreeCategory::id2color(int id)
{
    if (id < 0)
        return cv::Vec3b(0,0,0);
    return _idColorTable[id];
}

int TreeCategory::nCategories() const
{
    return _idNameTable.size();
}

std::vector<std::string> TreeCategory::getTreeTypeNames() const
{
    int nClass = nCategories();
    std::vector<std::string> names(nClass);
    for (map<int,string>::const_iterator it = _idNameTable.begin(); it != _idNameTable.end(); ++it)
    {
        int id = it->first;
        string name = it->second;
        names[id] = name;
    }
}

void TreeCategory::getLabels(vector<int> &labels)
{
    labels.clear();
    for (map<int,string>::iterator it = _idNameTable.begin(); it != _idNameTable.end(); ++it)
    {
        int id = it->first;
        labels.push_back(id);
    }
}
