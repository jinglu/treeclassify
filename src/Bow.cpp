#include "Bow.h"

#include <iostream>
#include <opencv2/legacy/legacy.hpp>

#include "GloabalVal.h"
#include <Common/FileAPI.h>
#include <omp.h>

using namespace std;

Bow::Bow()
{
}


void Bow::buildvoc(const std::vector<BasicImage> &trainImages, const string bowname, const string vocname, const cv::Vec3b& activeColor)
{

    cv::SurfFeatureDetector detector(400);

    vector<cv::KeyPoint> keypoints;

    cv::Ptr<cv::DescriptorExtractor> extractor(new cv::OpponentColorDescriptorExtractor(
                                                   cv::Ptr<cv::DescriptorExtractor>(
                                                       new cv::SurfDescriptorExtractor())));
    cv::Mat descriptors;
    cv::Mat training_descriptors(1,
                                 extractor->descriptorSize(),
                                 extractor->descriptorType());

    for (size_t i = 0; i < trainImages.size(); ++i)
    {
        PixelImage pimg(trainImages[i]);
        pimg.setAtivePart(activeColor);

        cout << i+1 << "/" << trainImages.size() << " " << pimg._filenameImage << endl;
        cv::Mat target = pimg.getOriTarget();
        cv::Mat img_fg = target(pimg.getTargetBox());

        detector.detect(img_fg, keypoints);

        extractor->compute(target, keypoints, descriptors);

        training_descriptors.push_back(descriptors);
    }

    cout << "Total descriptors: " << training_descriptors.rows << endl;

    cout << "save training descriptors to " << bowname << endl;
    cv::FileStorage fs(bowname.c_str(), cv::FileStorage::WRITE);
    fs << "training_descriptors" << training_descriptors;
    fs.release();

    cv::BOWKMeansTrainer bowtrainer(1000);  //  number of clusters
    bowtrainer.add(training_descriptors);
    cout << "cluster BOW features ... " << endl;
    cv::Mat vocabulary = bowtrainer.cluster();
    cout << "save vocabulary to " << vocname << endl;
    cv::FileStorage fs1(vocname, cv::FileStorage::WRITE);
    fs1 << "vocabulary" << vocabulary;
    fs1.release();
}

void Bow::extract_training_samples(const string samplename,
                                   const std::vector<BasicImage>& trainImages,
                                   cv::Ptr<cv::FeatureDetector> &detector,
                                   cv::BOWImgDescriptorExtractor &bowide,
                                   map<string,cv::Mat> &classes_training_data,
                                   const cv::Vec3b& activeColor)
{
    int total_samples = 0;
    vector<string> classes_names;
    cout << "look in train data"<<endl;

    for (size_t i = 0; i < trainImages.size(); ++i)
    {
        vector<cv::KeyPoint> keypoints;
        cv::Mat response_hist;
        PixelImage pimg(trainImages[i]);
        pimg.setAtivePart(activeColor);
        cout << i+1 << "/" << trainImages.size() << " " << pimg._filenameImage << endl;cout.flush();

        cv::Mat img = pimg.getOriTarget();

        detector->detect(img, keypoints);
        bowide.compute(img, keypoints, response_hist);


        string class_;
        int class_id;
        pimg.getTreeTypeFromName(class_, class_id);

#pragma omp critical
        {
            if(classes_training_data.count(class_) == 0) { //not yet created...
                classes_training_data[class_].create(0,response_hist.cols,response_hist.type());
                classes_names.push_back(class_);
            }
            classes_training_data[class_].push_back(response_hist);
        }
        total_samples++;
    }

    cout << endl;

    cout << "save to file.."<<endl;
    {
        cv::FileStorage fs(samplename,cv::FileStorage::WRITE);
        for (map<string,cv::Mat>::iterator it = classes_training_data.begin(); it != classes_training_data.end(); ++it) {
            cout << "save " << (*it).first << endl;
            fs << (*it).first << (*it).second;
        }
    }
}

void Bow::trainSVM(const string svmfile, map<string,cv::Mat> &classes_training_data, const int response_cols, const int response_type)
{
    //train 1-vs-all SVMs
    ofstream fout(svmfile.c_str());

    vector<string> classes_names;
    for (map<string,cv::Mat>::iterator it = classes_training_data.begin(); it != classes_training_data.end(); ++it)
    {
        classes_names.push_back((*it).first);
    }
    //myDataset._treeCategory.getTreeTypeNames(classes_names);

#pragma omp parallel for schedule(dynamic) //private(use_postfix)
    for (int i=0;i<classes_names.size();i++) {
        string class_ = classes_names[i];
        cout << omp_get_thread_num() << " training class: " << class_ << ".." << endl;

        cv::Mat samples(0,response_cols,response_type);
        cv::Mat labels(0,1,CV_32FC1);

        //  copy class samples and label
        cout << "adding " << classes_training_data[class_].rows << " positive" << endl;
        samples.push_back(classes_training_data[class_]);
        cv::Mat class_label = cv::Mat::ones(classes_training_data[class_].rows, 1, CV_32FC1);
        labels.push_back(class_label);

        //  copy rest samples and label
        for (map<string,cv::Mat>::iterator it1 = classes_training_data.begin(); it1 != classes_training_data.end(); ++it1) {
            string not_class_ = (*it1).first;
            if(not_class_.compare(class_)==0) continue;
            samples.push_back(classes_training_data[not_class_]);
            class_label = cv::Mat::zeros(classes_training_data[not_class_].rows, 1, CV_32FC1);
            labels.push_back(class_label);
        }

        cout << "Train SVM.." << endl;
        cv::Mat samples_32f; samples.convertTo(samples_32f, CV_32F);
        if(samples.rows == 0) continue; //phantom class?!
        CvSVM classifier;
        classifier.train(samples_32f,labels);
        cout << "end train " << endl;
        {
            stringstream ss;
            //ss << "SVM_classifier_";
            //if(file_postfix.size() > 0) ss << file_postfix << "_";
            //ss << class_ << ".yml";

            string svmname = myDataset._bowDir + "SVM_classifier_" + class_ + ".yml";
            cout << "Save.." << svmname<< endl;
            fout << svmname << endl;
            classifier.save(svmname.c_str());
        }
    }
    fout.close();
}

void Bow::trainbow(const vector<BasicImage> &trainImages, const string vocname, const string samplename, const cv::Vec3b& activeColor)
{
    cout << "-------- train BOVW SVMs -----------" << endl;
    cout << "read vocabulary form file"<<endl;
    cv::Mat vocabulary;
    cv::FileStorage fs(vocname, cv::FileStorage::READ);
    fs["vocabulary"] >> vocabulary;
    fs.release();

    vector<cv::KeyPoint> keypoints;

    cv::Mat response_hist;

    map<string, cv::Mat> classes_trainng_data;

    cv::Ptr<cv::FeatureDetector > detector(new cv::SurfFeatureDetector());
    cv::Ptr<cv::DescriptorMatcher > matcher(new cv::BruteForceMatcher<cv::L2<float> >());

    cv::Ptr<cv::DescriptorExtractor > extractor(new cv::OpponentColorDescriptorExtractor(
                                                    cv::Ptr<cv::DescriptorExtractor>(
                                                        new cv::SurfDescriptorExtractor())));
    //cv::Ptr<cv::BOWImgDescriptorExtractor> bowide(new cv::BOWImgDescriptorExtractor(extractor,matcher));
    cv::BOWImgDescriptorExtractor bowide(extractor,matcher);
    bowide.setVocabulary(vocabulary);

    map<string,cv::Mat> classes_training_data; classes_training_data.clear();


    cout << "extract_training_samples.." << endl;
    extract_training_samples(samplename, trainImages, detector, bowide, classes_training_data, activeColor);

    cout << "got " << classes_training_data.size() << " classes." <<endl;

    for (map<string,cv::Mat>::iterator it = classes_training_data.begin(); it != classes_training_data.end(); ++it)
    {
        cout << " class " << (*it).first << " has " << (*it).second.rows << " samples"<<endl;
    }

    cout << "train SVMs" << endl;
    string svmfile = myDataset._svmfile;
    trainSVM(svmfile, classes_training_data, bowide.descriptorSize(), bowide.descriptorType());
}

void Bow::test_classifiers(const vector<BasicImage> &testImages, const cv::Vec3b& activeColor)
{
    testInitialize();

    map<string,map<string,int> > confusion_matrix;
        for (map<string,CvSVM>::iterator it = _classes_classifiers.begin(); it != _classes_classifiers.end(); ++it) {
            for (map<string,CvSVM>::iterator it1 = _classes_classifiers.begin(); it1 != _classes_classifiers.end(); ++it1) {
                //string class1 = ((*it).first.compare("cake")==0) ? "cookies" : ((*it).first.compare("fruit")==0) ? "fruit_veggie" : (*it).first;
                //string class2 = ((*it1).first.compare("cake")==0) ? "cookies" : ((*it1).first.compare("fruit")==0) ? "fruit_veggie" : (*it1).first;
                string class1 = it->first;
                string class2 = it1->first;
                confusion_matrix[class1][class2] = 0;
            }
        }


    vector<string> classes_;
    for (map<string,CvSVM>::iterator it = _classes_classifiers.begin(); it != _classes_classifiers.end(); ++it)
    {
        classes_.push_back(it->first);
        cout << "class " << it->first << endl;
    }

    cv::BOWImgDescriptorExtractor bowide(_extractor,_matcher);
    bowide.setVocabulary(_vocabulary);

    for (size_t i = 0; i < testImages.size(); ++i)
    {
        PixelImage pimg(testImages[i]);
        pimg.setAtivePart(activeColor);

        cout <<"*\n"<< i+1 << "/" << testImages.size() << " " << pimg._filenameImage << endl;cout.flush();

        vector<string> max_class;
        evaluateOneImage(pimg, max_class, bowide);

        //cout << "manual class: ";
        //for(int j_=0;j_<classes_.size();j_++) cout << classes_[j_] << ",";
        //cout << endl;

        int j_=0;
        for(;j_<classes_.size();j_++) {
            if(classes_[j_].compare(max_class[0])==0) //got a hit
            {
                confusion_matrix[max_class[0]][classes_[j_]]++;
                break;
            }
        }
        if(j_==classes_.size()) //no hit was found, just use any class
            confusion_matrix[max_class[0]][classes_[0]]++;

        cv::Mat out; pimg._image.copyTo(out);
        cv::putText(out, max_class[0] + "!", cv::Point(out.cols/2-100,out.rows/2-50), CV_FONT_HERSHEY_PLAIN, 6.0, cv::Scalar(0,0,255), 8);
        if(max_class.size()>1) {
            cv::putText(out, max_class[1] + "?", cv::Point(out.cols/2-100,out.rows/2+50), CV_FONT_HERSHEY_PLAIN, 6.0, cv::Scalar(255,0,0), 6);
        }
        //cv::imshow("out",out);
        //cv::waitKey(0);
        string outname = myDataset._bowDir + "/out/"+FileAPI::extractFileName(pimg._filenameImage,true);
        cout << "save out " << outname << endl;
        cv::imwrite(outname, out);

    }

    for(map<string,map<string,int> >::iterator it = confusion_matrix.begin(); it != confusion_matrix.end(); ++it)
    {
        cout << (*it).first << " -> ";
        for(map<string,int>::iterator it1 = (*it).second.begin(); it1 != (*it).second.end(); ++it1)
        {
            cout << (*it1).first << ":" << (*it1).second << endl;
        }
        cout << endl;
    }

        cout << endl;
}

void Bow::loadSVM(string svmfile)
{
    ifstream fin(svmfile.c_str());
    while(!fin.eof())
    {
        string line;
        getline(fin, line);
        int sid = line.find("SVM_classifier_");
        if (sid > 0)
        {
            string class_ = line.substr(line.rfind('_')+1,line.rfind('.')-line.rfind('_')-1);
            cout << "load " << line << ", class: " << class_ << endl;
            _classes_classifiers.insert(pair<string,CvSVM>(class_,CvSVM()));
            _classes_classifiers[class_].load(line.c_str());
        }
    }
}

void Bow::loadVocabulary(string vocname)
{
    cout << "read vocabulary form file"<<endl;
    cv::FileStorage fs(myDataset._vocname, cv::FileStorage::READ);
    fs["vocabulary"] >> _vocabulary;
    fs.release();
}

void Bow::testInitialize()
{
    loadSVM(myDataset._svmfile);

    loadVocabulary(myDataset._vocname);

     _detector = cv::Ptr<cv::FeatureDetector >(new cv::SurfFeatureDetector());
     _matcher = cv::Ptr<cv::DescriptorMatcher >(new cv::BruteForceMatcher<cv::L2<float> >());
     _extractor = cv::Ptr<cv::DescriptorExtractor > (new cv::OpponentColorDescriptorExtractor(cv::Ptr<cv::DescriptorExtractor>(new cv::SurfDescriptorExtractor())));

     _bowide = cv::Ptr<cv::BOWImgDescriptorExtractor>(new cv::BOWImgDescriptorExtractor(_extractor,_matcher));
     _bowide->setVocabulary(_vocabulary);

    int ccount = 0;
    for (map<string,CvSVM>::iterator it = _classes_classifiers.begin(); it != _classes_classifiers.end(); ++it) {
            _classes_colors[(*it).first] = cv::Scalar((float)(ccount++)/(float)(_classes_classifiers.size())*180.0f,255,255);
            cout << "class " << (*it).first << " color " << _classes_colors[(*it).first].val[0] << endl;
        }
}

void Bow::evaluateOneImage(const PixelImage& pimg, vector<std::string> &out_classes, cv::BOWImgDescriptorExtractor & bowide)
{
    //cv::Mat diff = (img - background), diff_8UC1;
    cv::Mat target = pimg.getOriTarget();
    cv::Mat copy; cvtColor(target, copy, CV_BGR2HSV);

    vector<cv::Point> check_points;
    int winsize = 200;
    map<string,pair<int,float> > found_classes;

    for (int x=0; x<target.cols; x+=winsize/4)
    {
        for (int y=0; y<target.rows; y+=winsize/4)
        {
            if (target.at<uchar>(y,x) == 0)
            {
                continue;
            }
            check_points.push_back(cv::Point(x,y));
        }
    }

    cout << "to check: " << check_points.size() << " points"<<endl;
    cv::Mat seg = cv::Mat::zeros(copy.size(),CV_8UC3);

#pragma omp parallel for
    for (int i = 0; i < check_points.size(); i++)
    {
        int x = check_points[i].x;
        int y = check_points[i].y;
        //cout << omp_get_thread_num() << " scan " << check_points[i] << endl;
        cv::Mat img,response_hist;
        target(cv::Rect(x-winsize/2,y-winsize/2,winsize,winsize)&cv::Rect(0,0,target.cols,target.rows)).copyTo(img);

        vector<cv::KeyPoint> keypoints;
        _detector->detect(img,keypoints);


        bowide.compute(img, keypoints, response_hist); //, &pointIdxsOfClusters);
        if (response_hist.cols == 0 || response_hist.rows == 0) {
            continue;
        }
//        vector<vector<int> > pointIdxsOfClusters;

//                drawKeypoints(img, keypoints, img, cv::Scalar(0,0,255));
//                for (int i = 0; i < pointIdxsOfClusters.size(); i++) {
//                    if(pointIdxsOfClusters[i].size()>0) {
//                        cv::Scalar clr(i/1000.0*255.0,0,0);
//                        for (int j = 0; j < pointIdxsOfClusters[i].size(); j++) {
//                            circle(img, keypoints[pointIdxsOfClusters[i][j]].pt, 1, clr, 2);
//                        }
//                    }
//                }
//                cv::imshow("pic",img);
//                cv::waitKey(0);

        //test vs. SVMs
        try {
            float minf = FLT_MAX; string minclass;
            for (map<string,CvSVM>::iterator it = _classes_classifiers.begin(); it != _classes_classifiers.end(); ++it) {
                float res = (*it).second.predict(response_hist,true);
                if ((*it).first == "misc" && res > 0.9) {
                    continue;
                }
                if(res > 1.0) continue;
                if (res < minf) {
                    minf = res;
                    minclass = (*it).first;
                }
            }
            //cout << "best class: " << minclass << " ("<<minf<<")"<<endl;
            //cout << "."; cout.flush();
            cv::circle(copy, cv::Point(x,y), 5, _classes_colors[minclass], CV_FILLED);
            float dim = std::max(std::min(minf - 0.8f,0.3f),0.0f) / 0.3f; //dimming the color: [0.8,1.1] -> [0.0,1.0]
            cv::Scalar color_(_classes_colors[minclass].val[0], _classes_colors[minclass].val[1], _classes_colors[minclass].val[2] * dim);

#pragma omp critical
            {
                cv::putText(copy, minclass.substr(0, 4), cv::Point(x-35,y+10), CV_FONT_HERSHEY_PLAIN, 2.0, cv::Scalar(0,0,255), 2);
                cv::circle(seg, check_points[i], winsize/5, color_, CV_FILLED);
                found_classes[minclass].first++;
                found_classes[minclass].second += minf;
                //cout << "find classed " << minclass << " num " << found_classes[minclass].first <<", " << found_classes[minclass].second<<endl;

                //cv::imwrite(myDataset._bowDir+"/out/"+FileAPI::extractFileName(pimg._filenameImage,false)+"_copy.png", copy);
            }
        }
        catch (cv::Exception) {
            continue;
        }
    }

    cout << endl << "found classes: ";
    float max_class_f = FLT_MIN, max_class_f1 = FLT_MIN; string max_class, max_class1;
    vector<float> scores;
    for (map<string,pair<int,float> >::iterator it=found_classes.begin(); it != found_classes.end(); ++it)
    {
        float score = sqrt(fabs(float((*it).second.first) * (*it).second.second));
        //cout << "score " << score << "," << (*it).second.first << ", " <<  (*it).second.second << endl;
        if (score > 1e+10) {
            continue;	//an impossible score
        }
        scores.push_back(score);
        cout << (*it).first << ": score(" << score << "),";
//             << (*it).second.first
//             << "," << (*it).second.second / (float)(*it).second.first << "), "
//                << (*it).second.second;
        if(score > max_class_f) { //1st place thrown off
            max_class_f1 = max_class_f;
            max_class1 = max_class;

            max_class_f = score;
            max_class = (*it).first;
        } else if (score >  max_class_f1) {	//2nd place thrown off
            max_class_f1 = score;
            max_class1 = (*it).first;
        }
    }
    cout << endl;

    //normalizeClassname(max_class);
    //normalizeClassname(max_class1);

    cv::Scalar mean_,stddev_;
//	meanStdDev(Mat(scores), mean_, stddev_);
    out_classes.clear();
    cout << "max class " << max_class << endl;
    out_classes.push_back(max_class);
    if(max_class_f - max_class_f1 < 10) {
        //Forget about it: variance is low (~10), so result is undecicive, we should take both max-classes.
        out_classes.push_back(max_class1);
    }

    cout << "chosen class: " << max_class << ", (" << max_class1 << "?)" << endl;
}
