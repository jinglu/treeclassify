#include "Clustering.h"
#include <Common/compare.h>
#include <vl/kmeans.h>
#include "FeatureExtractor.h"
#include "GloabalVal.h"
#include <iostream>
#include <opencv2/ml/ml.hpp>
#include <Image/Image.h>
#include <Common/FileAPI.h>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;

Clustering::Clustering()
{
    string tablename = myDataset._clusterDir+"colortable.txt";
    constructColorTable(tablename);
}

void Clustering::constructColorTable(const string tablename)
{
    ifstream fin(tablename.c_str());

    while(!fin.eof())
    {
        string line;
        getline(fin, line);

        vector<string> lineitems;
        boost::split(lineitems, line, boost::is_any_of(" "), boost::token_compress_on);
        if (lineitems.size() >= 3)
        {
            cv::Vec3b vec;
            for (size_t i = 0; i < 3; ++i)
            {
                vec[i] = (uchar)boost::lexical_cast<int>(lineitems[i]);
            }
            _colorTable.push_back(wjl::Color(vec,false));
        }
    }
    cout << "color table " << _colorTable.size() << endl;
//    _colorTable.push_back(wjl::Color::red());
//    _colorTable.push_back(wjl::Color::green());
//    _colorTable.push_back(wjl::Color::cyan());
//    _colorTable.push_back(wjl::Color::magenta());
//    _colorTable.push_back(wjl::Color::gray());
}


void Clustering::kmeanCluster(float *trainData,
                              const int featDim,
                              const int numSamples,
                              const int numClusters,
                              const int maxIteration,
                              vector<vector<float> >& centers)
{
    VlKMeansAlgorithm kmAlgorithm = VlKMeansElkan;
    VlVectorComparisonType distance = VlDistanceL2;
    VlKMeans * kmeans = vl_kmeans_new (VL_TYPE_FLOAT, distance) ;
    vl_kmeans_set_algorithm(kmeans, kmAlgorithm);
    vl_kmeans_seed_centers_plus_plus(kmeans, trainData, featDim, numSamples, numClusters);
    vl_kmeans_set_max_num_iterations(kmeans, maxIteration);
    vl_kmeans_refine_centers(kmeans, trainData, numSamples);

    float* ptCenters = (float*) vl_kmeans_get_centers(kmeans);

    centers = vector<vector<float> >(numSamples, vector<float>(featDim,0));

    for (int i = 0; i < numClusters; ++i)
        for(int j = 0; j < featDim; ++j)
        {
            centers[i][j] = ptCenters[i*featDim + j];
            //cout << i<<","<<j<<": "<<ptCenters[i*featDim + j] << "$";
        }

    vl_kmeans_delete(kmeans);
}

void Clustering::clusterSamples(const std::vector<FeatVector> &sampleFeatureSet,
                                const int numClusters,
                                vector<vector<float> >& centers)
{
    centers.clear();
    centers.reserve(numClusters);
    assert(!sampleFeatureSet.empty());
    int numSamples = sampleFeatureSet.size();
    int featDim =  sampleFeatureSet.at(0).getDim();
    int maxIteration = 100;
    float* trainData = new float[featDim * numSamples];

    for (int i = 0; i < numSamples; ++i)
        for (int j = 0; j < featDim; ++j)
            trainData[i*featDim + j] = sampleFeatureSet[i]._data[j];

    kmeanCluster(trainData,
                 featDim,
                 numSamples,
                 numClusters,
                 maxIteration,
                 centers);


    delete trainData;
}

void Clustering::objLevelCluster(std::vector<BasicImage> &images, const int numCluster, const cv::Vec3b& activeColor)
{
    string outname = myDataset._clusterDir + "temp"+ MyDataset::getFeatName(myDataset._objFeatParameters)+ ".cluster";
    ofstream fout(outname.c_str());

    string labelname = myDataset._clusterDir + "temp.label";
    //ofstream out_label(labelname.c_str());


    // extract features
    FeatureExtractor extractor;
    vector<FeatVector> sampleFeatureSet;
    extractor.extractImageLevelTrainFeature(myDataset._objFeatParameters, activeColor, images, sampleFeatureSet);
    int numSamples = sampleFeatureSet.size();
    assert(sampleFeatureSet.size() > 0);
    cout << "num images " << images.size() << ", num samples " << numSamples << endl;
    // shape mask feature pca

    int shapemaskId = -1;
    for (size_t i = 0; i < myDataset._objFeatParameters.size(); ++i)
        if (myDataset._objFeatParameters[i]._featName == FeatureParameter::SHAPEMASK)
        {
            shapemaskId = i;
            break;
        }
    if (shapemaskId == 0 && myDataset._objFeatParameters.size() == 1)
    {
        int shapemask_dim = 32 * 32;
        int shapemask_dst_dim = 128;
        cv::Mat shape_data(numSamples, shapemask_dim, CV_32FC1);
        cv::Mat shape_proj_data(numSamples, shapemask_dst_dim, CV_32FC1);

        for (int i = 0; i < numSamples; ++i)
        {
            for (int j = 0; j < shapemask_dim; ++j)
                shape_data.at<float>(i,j) = sampleFeatureSet[i]._data[j];
        }

        cv::PCA shape_pca(shape_data, cv::Mat(), CV_PCA_DATA_AS_ROW, shapemask_dst_dim);
        shape_pca.project(shape_data, shape_proj_data);


        for (int i = 0; i < numSamples; ++i)
        {
            FeatVector oldfeat = sampleFeatureSet[i];
            FeatVector tempfeat;
            sampleFeatureSet[i]._data.clear();

            for (int j = 0; j < shapemask_dst_dim; ++j)
            {
                //sampleFeatureSet[i]._data.push_back(shape_proj_data.at<float>(i,j));
                tempfeat._data.push_back(shape_proj_data.at<float>(i,j));
            }
            //tempfeat.normalize();
            sampleFeatureSet[i]._data = tempfeat._data;
            for (int j = shapemask_dim; j < oldfeat.getDim(); ++j)
            {
                sampleFeatureSet[i]._data.push_back(oldfeat._data[j]);
            }
        }
    }

    // kmeans clustering
    int featDim = sampleFeatureSet[0].getDim();
    vector<vector<float> > centers;
    clusterSamples(sampleFeatureSet,
                   numCluster,
                   centers);

//    cv::Mat data(numSamples, featDim, 3, CV_32F);
//    for (int i = 0; i < numSamples; ++i)
//        for (int j = 0; j < featDim; ++j)
//            data.at<float>(i,j) = sampleFeatureSet[i]._data[j];

//    cv::kmeans(data, numClusters, bestLabels,
//               cv::TermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 10000, 0.0001),
//               attempts,
//               KMEANS_PP_CENTERS,
//               centers)

//    for (int i = 0; i < numCluster; ++i)
//    {
//        cout << "center " << i << endl;
//        for (int j = 0; j < centers[i].size(); ++j)
//            cout << centers[i][j] << ", ";
//    }
    vector<int> sampleLabels(numSamples, -1);
    vector<pair<int,float> > sample_label_dist;
    sample_label_dist.reserve(numSamples);

    for (int i = 0; i < numSamples; ++i)
    {
        float minDist;
        int label = -1;
        for (int j = 0; j < numCluster; ++j)
        {
            float dist = wjl::vec_dist2<float>(sampleFeatureSet[i]._data, centers[j]);
            if (j == 0)
            {
                minDist = dist;
                label = j;
            }
            else if (minDist > dist)
            {
                minDist = dist;
                label = j;
            }
            //cout << "sample " << i << ", label " << j <<", dist " << dist<< endl;
        }
        //cout << "sample " << i << ", label " << label <<", mindist " << minDist<< endl;
        sampleLabels.at(i) = label;
        sample_label_dist.push_back(make_pair(label, minDist));
        //out_label << i <<' ' << images[i]._filenameImage << ' ' << label << ' ' << minDist << endl;
    }
    //out_label.close();

    vector<vector<int> > clusterIds(numCluster, vector<int>());
    vector<vector<pair<float, int> > > cluster_dist_ids(numCluster, vector<pair<float,int> >());
    for (size_t i = 0; i < sampleLabels.size(); ++i)
    {
        int label = sampleLabels[i];
        clusterIds[label].push_back(i);
        cluster_dist_ids[label].push_back(make_pair(sample_label_dist[i].second, i));
    }

    for (int i = 0; i < numCluster; ++i)
    {
        cout << "cluster " << i << " include: " << endl;
        fout << "cluster " << i << " include: " << endl;
        sort(cluster_dist_ids[i].begin(), cluster_dist_ids[i].end());

        for (size_t j = 0; j < cluster_dist_ids[i].size(); ++j)
        {
            int sample_id = cluster_dist_ids[i][j].second;
            float dist = cluster_dist_ids[i][j].first;

            sampleFeatureSet[sample_id].setLabel(i);
            fout << images.at(sample_id)._filenameImage << " "
                 //<< label << " "
                 << dist << endl;
        }
    }
    fout.close();

    // *************** PCA analize ****************************////////////////
    vector<FeatVector> featCenters;
    featCenters.reserve(numCluster);
    for (int i = 0; i < numCluster; ++i)
    {
        FeatVector feat;
        feat._data = centers[i];
        feat.setLabel(i);
        featCenters.push_back(feat);
    }
    this->objLevelClusterPCA(images, sampleFeatureSet, featCenters, 2);

    // ************** get average image *****************/////////////////
    int dsttargetw = 512, dsttargeth = 512;
    for (int i = 0; i < numCluster; ++i)
    {
        cv::Mat sum_targeti = cv::Mat::zeros(dsttargeth, dsttargetw, CV_32FC3);
        float factor = 1.0 / float(cluster_dist_ids[i].size());

        for (size_t j = 0; j < cluster_dist_ids[i].size(); ++j)
        {
            cout << "cluster " << i << ", num " << j << endl;
            int sample_id = cluster_dist_ids[i][j].second;
            cv::Mat target;
            PixelImage pimg(images[sample_id]);

            target = pimg.getTarget(dsttargetw, dsttargeth);
            //cv::imwrite("target.png", target);
            cv::Mat target_int(dsttargeth, dsttargetw, CV_32FC3);
            target.convertTo(target_int, CV_32FC3);
            sum_targeti += target_int;
            //cv::add(target_int, sum_targeti, sum_targeti);

        }

        cv::Mat average_targeti(dsttargeth, dsttargetw, CV_32FC3);
        average_targeti = sum_targeti * factor;
        string average_name = myDataset._clusterDir+ "avg_img/cluster_"  + boost::lexical_cast<string>(i) + MyDataset::getFeatName(myDataset._objFeatParameters) + ".png";
        cout << average_name << endl;

        cv::imwrite(average_name, average_targeti);
    }
}

void Clustering::objLevelClusterPCA(const vector<BasicImage> &images,
                                      const vector<FeatVector>& sampleFeatureSet,
                                      const vector<FeatVector>& centerFeatures,
                                      const int numComponent)
{

    int max_it = 100;

    string showname = myDataset._clusterDir + "pca" + MyDataset::getFeatName(myDataset._objFeatParameters)+".png";

    FeatureExtractor extractor;

    assert(!sampleFeatureSet.empty());
    int num_sample = sampleFeatureSet.size();
    int feat_dim = sampleFeatureSet[0].getDim();


    cv::Mat input_data(num_sample, feat_dim, CV_32FC1);
    for (int i = 0; i < num_sample; ++i)
        for (int j = 0; j < feat_dim; ++j)
            input_data.at<float>(i,j) = sampleFeatureSet[i]._data[j];

    cv::PCA pca(input_data, cv::Mat(), CV_PCA_DATA_AS_ROW, numComponent);

    cv::Mat mean = pca.mean.clone();
    cv::Mat eigen_vals = pca.eigenvalues.clone();
    cv::Mat eigen_vecs = pca.eigenvectors.clone();

    cout << "mean " << mean.rows << ", " << mean.cols << endl;
    cout << "eval " << eigen_vals.rows << ", " << eigen_vals.cols << endl;
    cout << "evec " << eigen_vecs.rows << ", " << eigen_vecs.cols << endl;

    cv::Mat project_data(num_sample, numComponent, CV_32FC1);

    pca.project(input_data, project_data);
    //cv::Mat test_project(1,numComponent,CV_32FC1);
    //pca.project(input_data.row(0), test_project);

    float left = 1e6, right = -1e6, top = 1e6, bottom = -1e6;
    for (int i = 0; i < num_sample; ++i)
    {
        left = min(left, project_data.at<float>(i,0));
        right = max(right, project_data.at<float>(i,0));

        top = min(top, project_data.at<float>(i,1));
        bottom = max(bottom, project_data.at<float>(i,1));
    }
    cout << "[" << left << "," << right<<"] * [" << top << ","<<bottom<<"]" << endl;

//    vector<float> train_data;
//    train_data.reserve(num_sample * numComponent);
//    for (int i = 0; i < num_sample; ++i)
//        for (int j = 0; j < numComponent; ++j)
//            train_data.push_back(project_data.at<float>(i,j));


//    vector<vector<float> > centers;
//    this->kmeanCluster(&(train_data[0]), numComponent, num_sample, numCluster, max_it, centers);

    int dst_width = 1024;
    int dst_height = float(dst_width) / (right-left) * (bottom-top);
    float scale_w = float(dst_width) / (right - left);
    float scale_h = float(dst_height) / (bottom - top);
    float dst_left = left * scale_w;
    float dst_top = top * scale_h;

    int offset = 200;
    dst_left -= offset/2;
    dst_top -= offset/2;
    wjl::Image showimage(dst_width+offset, dst_height+offset, wjl::Color::white());

    for (int i = 0; i < num_sample; ++i)
    {
        int x = project_data.at<float>(i,0) * scale_w - dst_left;
        int y = project_data.at<float>(i,1) * scale_h - dst_top;
        int label = sampleFeatureSet[i].getLabel();

        showimage.draw_circle(x,y,8,_colorTable[label], CV_FILLED);

        string shortname = FileAPI::extractFileName(images[i]._filenameImage, false);
        showimage.draw_text(x,y,shortname, CV_FONT_HERSHEY_SIMPLEX, 0.4, wjl::Color::blue(), 1);
        //cout << shortname << ", l = " << label << endl;
    }

    int num_cluster = centerFeatures.size();
    cv::Mat center_data(num_cluster, feat_dim, CV_32FC1);
    for (int i = 0; i < num_cluster; ++i)
        for (int j = 0; j < feat_dim; ++j)
        {
            center_data.at<float>(i,j) = centerFeatures[i]._data[j];
            //cout << i << ", " << j << ", " << center_data.at<float>(i,j) << endl;
        }
    cv::Mat project_center(num_cluster, numComponent, CV_32FC1);
    pca.project(center_data, project_center);

    for (int i = 0; i < centerFeatures.size(); ++i)
    {
        int x = project_center.at<float>(i,0)  * scale_w - dst_left;
        int y = project_center.at<float>(i,1) * scale_h - dst_top;
        int label = centerFeatures[i].getLabel();

        showimage.draw_circle(x,y,10,_colorTable[label], 4);
    }

    showimage.write(showname);
}

