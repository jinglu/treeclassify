#ifndef BARKESTIMATOR_H
#define BARKESTIMATOR_H

#include "PixelImage.h"
#include <opencv2/legacy/legacy.hpp>
class BarkEstimator
{
public:
    BarkEstimator();

    std::string getSVMName() const;

    void extractSamples(const std::vector<FeatureParameter>& featParas,
                        const std::vector<BasicImage>& barkImages,
                        const std::vector<BasicImage>& nonBarkImages,
                        cv::Mat& trainData,
                        cv::Mat& labelMat);

    void train(const std::vector<BasicImage>& barkImages,
               const std::vector<BasicImage>& nonBarkImages);
    void trainSVM(const std::string svmName,
                  const cv::Mat& trainData,
                  const cv::Mat& labeMat);
    void pcaAnalize(const cv::Mat& trainData,
                    const cv::Mat& labelMat);

//    void loadSVM(const std::string svmName, CvSVM& classifier);

    void test(const std::vector<BasicImage>& testImages);

    void evaluateOneImage(const std::vector<FeatureParameter> &featParas,
                          const PixelImage& pimg,
                          cv::Mat& confMat);

    //
    void getCandidateWindow(const PixelImage& pimg, const cv::Mat& branchProbMat, const int winsize);

private:
    CvSVM _classifer;
};

#endif // BARKESTIMATOR_H
