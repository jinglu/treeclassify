#include "PixelImage.h"

#include <iostream>
#include <Common/FileAPI.h>
#include <Image/ImageProcessor.h>
#include "GloabalVal.h"
#include "FeatureExtractor.h"
#include "Clustering.h"
#include "GraphModel.h"
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include "BarkEstimator.h"

using namespace std;

BasicImage::BasicImage()
{

}

BasicImage::BasicImage(const string filenameImg,
                       const string filenameMask,
                       bool isTrain)
    : _filenameImage(filenameImg),
      _filenameMask(filenameMask),
      _isTrain(isTrain)
{
}

void BasicImage::setImageName(const string filename)
{
    _filenameImage = filename;
}
void BasicImage::setMaskName(const string filename)
{
    _filenameMask = filename;
}
void BasicImage::setIsTrain(const bool isTrain)
{
    _isTrain = isTrain;
}

//////////////////////////////////////////////////////
PixelImage::PixelImage()
{
    this->initialize();
}

PixelImage::PixelImage(const BasicImage &bimg)
    : BasicImage(bimg)
{
    //cout << "construct " << _filenameImage << ", " << _filenameMask << endl;
    this->initialize();
}

PixelImage::PixelImage(const string filenameImg,
                       string filenameMask,
                       bool isTrain)
    : BasicImage(filenameImg, filenameMask, isTrain)
{
    //cout << "construct " << _filenameImage << ", " << _filenameMask << endl;
    this->initialize();
}

bool PixelImage::loadImage()
{
    _image = cv::imread(_filenameImage);
    return (!_image.empty());
}

bool PixelImage::loadLabelMap()
{
    _labelMap = cv::imread(_filenameMask);

    return (!_labelMap.empty());
}

void PixelImage::setParsLabelMat()
{
    assert(!_labelMap.empty());
    _parsLabelMat = cv::Mat(_image.rows, _image.cols, CV_32S);

    for (int y = 0; y < _image.rows; ++y)
        for (int x = 0; x < _image.cols; ++x)
        {
            cv::Vec3b& color = _labelMap.at<cv::Vec3b>(y,x);
            int label = myDataset._parsColorTable.color2label(color);
            _parsLabelMat.at<int>(y,x) = label;
        }
}

void PixelImage::initLabelMap()
{
    assert(!_image.empty());
    _labelMap = cv::Mat::ones(_image.rows, _image.cols, CV_8UC3) * 255;
}

void PixelImage::initialize()
{
    if (loadImage())
    {
        cout << "load image " << _filenameImage<< " successfully!" << endl;
        if (loadLabelMap())
        {
            cout << "load mask " << _filenameMask<<" successfully!" << endl;
            assert(_image.rows == _labelMap.rows);
            assert(_image.cols == _labelMap.cols);
        }
        else
        {
            initLabelMap();
            cout << "initialize mask as all 255 except the black ones!" << endl;
        }
       setParsLabelMat();


        if (_isTrain)
        {
            string filename = FileAPI::extractFileName(_filenameImage, false);
            string treename = filename.substr(0,filename.find_first_of("_"));
            _treeId = myDataset._speciesColorTable.name2label(treename);
            cout << "true tree category " << treename << endl;
        }
        else
            _treeId = -1;
        _activeMask = cv::Mat::ones(_image.rows, _image.cols, CV_8UC1)*255;
    }
}

void PixelImage::setAtivePart(const cv::Vec3b& activeColor)
{

    _activeMask = cv::Mat::ones(_image.rows, _image.cols, CV_8UC1) * 255;

    for (int y = 0; y < _image.rows; ++y)
        for (int x = 0; x < _image.cols; ++x)
            if (activeColor != _labelMap.at<cv::Vec3b>(y,x))
                _activeMask.at<uchar>(y,x) = 0;
}

void PixelImage::constructShapeMaskFeature(FeatVector &shapeFeature) const
{
    assert(!_labelMap.empty());

    shapeFeature.clear();
    vector<cv::Point> crownPts;
    //find boundingbox
    int minX = _image.cols;
    int maxX = 0;
    int minY = _image.rows;
    int maxY = 0;
    for (int y = 0; y < _image.rows; ++y)
        for (int x = 0; x < _image.cols; ++x)
            if (_activeMask.at<uchar>(y,x) == 255)
            {
                crownPts.push_back(cv::Point(x,y));

                if (x < minX)
                    minX = x;
                if (x > maxX)
                    maxX = x;
                if (y < minY)
                    minY = y;
                if (y > maxY)
                    maxY = y;
            }
    cout << "BoundingBox [" << minX <<","<<maxX<<"], [" <<minY<<","<<maxY<<"]"<<endl;

    int boxWidth = maxX - minX + 1;
    int boxHeight = maxY - minY + 1;
    cv::Mat boxMask(boxHeight, boxWidth, CV_8UC1);
    for (int y = 0; y < boxHeight; ++y)
        for (int x = 0; x < boxWidth; ++x)
        {
            boxMask.at<uchar>(y,x) = _activeMask.at<uchar>(y+minY, x+minX);
        }
    cout << "boxMask " <<boxMask.cols << ","<<boxMask.rows << endl;

    //resize
    int rw = 32;
    int rh = 32;
    cv::Mat tinyMask = cv::Mat::zeros(rh, rw, CV_8UC1);
    if (boxMask.rows > boxMask.cols)
    {
        float factor = (float)rh / boxMask.rows;
        rw = boxMask.cols * factor;

    }
    else
    {
        float factor = (float)rw / boxMask.cols;
        rh = boxMask.rows * factor;
    }
    cv::Mat resizeBoxMask(rh,rw,CV_8UC1);
    cv::resize(boxMask, resizeBoxMask, resizeBoxMask.size());
    resizeBoxMask.copyTo(tinyMask(cv::Rect(tinyMask.cols/2 - resizeBoxMask.cols/2,
                                           tinyMask.rows/2 - resizeBoxMask.rows/2,
                                           resizeBoxMask.cols,
                                           resizeBoxMask.rows)));



    string shapeName = myDataset._featureDir + FileAPI::extractFileName(_filenameImage, false) + "_shape.png";
    //cv::imwrite(shapeName, tinyMask);

    float pixelNum = float(32 * 32);
    for (int y = 0; y < tinyMask.rows; ++y)
        for (int x = 0; x < tinyMask.cols; ++x)
        {
            float value = (float)tinyMask.at<uchar>(y,x) / 255.0 / pixelNum;
            shapeFeature._data.push_back(value);
            //cout << "% " << value << " ";
        }
    shapeFeature.setLabel(_treeId);
}

void PixelImage::constructContourMoment(FeatVector &momentFeature) const
{
    assert(!_activeMask.empty());
    momentFeature.clear();

//    cv::Mat canny_output;
//    vector<vector<cv::Point> > contours;
//    vector<cv::Vec4i> hierarchy;
//    int thresh = 100;
//    cv::Canny(_mask, canny_output, thresh, thresh * 2, 3);

//    cv::findContours(canny_output, contours, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

//    vector<cv::Moments> mu(contours.size());
//    for (size_t i = 0; i < contours.size(); ++i)
//        mu[i] = cv::moments(contours[i], false);

    vector<cv::Point> contour;
    for (int y = 0; y < _image.rows; ++y)
        for (int x = 0; x < _image.cols; ++x)
        {
            vector<cv::Point> neighbors;
            pixelNeighbors(cv::Point(x,y), neighbors);
            uchar nIntensity;
            bool isIsolate = false;
            for (size_t i = 0; i < neighbors.size(); ++i)
            {

                int nx = neighbors[i].x;
                int ny = neighbors[i].y;
                if (i == 0)
                {
                    nIntensity = _activeMask.at<uchar>(ny,nx);
                }
                else
                {
                    if (nIntensity != _activeMask.at<uchar>(ny,nx))
                    {
                        isIsolate = true;
                        break;
                    }
                }
            }
            if (isIsolate)
                contour.push_back(cv::Point(x,y));
        }
    cv::Moments mo = cv::moments(contour);
    momentFeature._data.push_back(mo.nu02);
    momentFeature._data.push_back(mo.nu03);
    momentFeature._data.push_back(mo.nu11);
    momentFeature._data.push_back(mo.nu12);
    momentFeature._data.push_back(mo.nu20);
    momentFeature._data.push_back(mo.nu21);
    momentFeature._data.push_back(mo.nu30);

    momentFeature.setLabel(_treeId);
    cout << "momemnt " << endl;
    for (size_t i = 0; i < momentFeature._data.size(); ++i)
        cout << momentFeature._data[i] << "\t";
}

void PixelImage::constructEdgeHist(FeatVector& edgeHist) const
{
    bool isdebug = false;
    wjl::Image image(_image);
    wjl::Image mask(_activeMask);
    image = image.copy_to(mask);
    //wjl::Image canny_img = wjl::ImageProcessor::edge_map_canny(image, 80,3,3);
    //canny_img.write(myDataset._edgeDir+FileAPI::extractFileName(_filenameImage,false)+"_canny.png");
    vector<wjl::LineSeg2i> ori_lines = wjl::ImageProcessor::find_lines_lsd(image,
                                                                           0.8,
                                                                           0.6,
                                                                           2.0,
                                                                           30.0);
//    vector<wjl::LineSeg2i> ori_lines;
//    wjl::ImageProcessor::find_canny_lines(_image, 50, 3, 3, ori_lines);
    vector<wjl::LineSeg2i> lines;

//    for (size_t i = 0; i < ori_lines.size(); ++i)
//    {
//        int bound_count = 0;
//        //std::cout << ori_lines[i];
//        //ori_lines[i].print(cout);
//        cv::LineIterator lineit(_image, ori_lines[i]._start, ori_lines[i]._end);
//        for (int j = 0; j < lineit.count; ++j, ++lineit)
//        {
//            if (isAtMaskBound(lineit.pos()))
//                bound_count++;
//        }
//        if (bound_count <= 3)
//            lines.push_back(ori_lines[i]);
//    }
    lines = ori_lines;

    if (isdebug)
    {
    wjl::Image edgemap(_image.cols, _image.rows, wjl::Color::black());
    edgemap.draw_lines(lines, wjl::Color::white(), 1);
    string edgename = myDataset._edgeDir + FileAPI::extractFileName(_filenameImage, false) + "_edge_full.png";
    edgemap.write(edgename);
    }

    //screen
    // 4 direction * 4 length
    int num_dir = 4;
    int num_len = 5;
    int side = _image.rows < _image.cols ? _image.rows : _image.cols; // min

    // length parameters
    float max_factor = 0.1;
    float gap = max_factor / float(num_len);

    vector<float> lengthW;

    for (int i = 0; i < num_len; ++i)
        lengthW.push_back(gap * float(i) * side);
    //lengthW.push_back(0.02 * side);
    //lengthW.push_back(0.04 * side);
    //lengthW.push_back(0.08 * side);

    edgeHist._data = vector<float>(num_dir*(1+lengthW.size()), 0);

    int dirID = -1;
    int lenID = -1;
    for (size_t i = 0; i < lines.size(); ++i)
    {
        float degree = fabs(lines[i].angleDegree());

        dirID = int(degree / 45.0 + 0.5);

        float length = lines[i].length();

//        if (length < lengthW[0])
//            lenID = 0;
//        else if (length < lengthW[1])
//            lenID = 1;
//        else if (length < lengthW[2])
//            lenID = 2;
//        else
//            lenID = 3;
        for (int j = 0; j < num_len; ++j)
        {
            if (length >= lengthW[j] && length < lengthW[j+1])
                lenID = j;
        }
        if (lenID < 0)
            lenID = num_len;

        int idx = dirID * num_len + lenID;
        edgeHist._data[idx]++;
    }

    for (size_t i = 0; i < edgeHist._data.size(); ++i)
        edgeHist._data[i] /= (float)lines.size();

    // analize length hist
    if (isdebug)
    {
    string lenname = myDataset._edgeDir + FileAPI::extractFileName(_filenameImage, false) + "_edgehist.png";
    int bench_height = 8;
    wjl::Image lenimage(_image.cols, lines.size() + lengthW.size()*bench_height, wjl::Color::white());
    for (size_t i = 0; i < lines.size(); ++i)
    {
        float length = lines[i].length();
        lenimage.draw_line(0, i, length, i, wjl::Color::red(), 1);
    }

    for (size_t i = 0; i < lengthW.size(); ++i)
    {
        int draw_y = i * bench_height + lines.size();
        lenimage.draw_line(0, draw_y , lengthW[i], draw_y, wjl::Color::random(myDataset._cvrng), bench_height);
    }
    lenimage.write(lenname);
    }
}

void PixelImage::pixelNeighbors(const cv::Point & pixel, vector<cv::Point> &neighbors) const
{
    const int& x = pixel.x;
    const int& y = pixel.y;
    neighbors.clear();
    if (x < 0 || y < 0 || x >= _image.cols || y >= _image.rows)
        return;

    if (x-1 >= 0)
        neighbors.push_back(cv::Point(x-1,y));
    if (y-1 >= 0)
        neighbors.push_back(cv::Point(x,y-1));
    if (x+1 < _image.cols)
        neighbors.push_back(cv::Point(x+1,y));
    if (y+1 < _image.rows)
        neighbors.push_back(cv::Point(x,y+1));
}

bool PixelImage::isAtMaskBound(const cv::Point& point) const
{
    vector<cv::Point> neighbors;
    pixelNeighbors(point, neighbors);
    neighbors.push_back(point);
    int black_count = 0;
    int white_count = 0;
    //cout << "current " << point.x << ", " << point.y << endl;
    for (size_t i = 0; i < neighbors.size(); ++i)
    {
        int nx = neighbors[i].x;
        int ny = neighbors[i].y;
        //cout << nx << ", " << ny << endl;
        if (_activeMask.at<uchar>(ny, nx) == 0)
            black_count++;
        else
            white_count++;
    }

    if (black_count == 0 || white_count == 0)
        return false;
    else
        return true;
}

void PixelImage::resizeTo(const int width, const int height, PixelImage &dstPimg) const
{
    cv::resize(_image, dstPimg._image, cv::Size(width, height));
    cv::resize(_activeMask, dstPimg._activeMask, cv::Size(width, height));
    cv::resize(_labelMap, dstPimg._labelMap, cv::Size(width, height));
}

void PixelImage::setTreeTypeProbs(const vector<float> &probVec)
{
    _treeTypeProbs = probVec;
}

void PixelImage::saveTreeTypeProbFile()
{
    int maxId = -1;
    float maxProb = 0;
    for (int i = 0; i < _treeTypeProbs.size(); ++i)
    {
        string treeName = myDataset._speciesColorTable.label2name(i);
        cout << treeName << ": " << _treeTypeProbs[i] << endl;
        if (maxProb < _treeTypeProbs[i])
        {
            maxProb = _treeTypeProbs[i];
            maxId = i;
        }
    }
    _treeId = maxId;
    cout << "classified tree type " << _treeId <<", "<< myDataset._speciesColorTable.label2name(_treeId) << endl;
}

void PixelImage::getTreeTypeFromName(string& treeName, int& treeId) const
{
    string filename = FileAPI::extractFileName(_filenameImage, false);
    treeName = filename.substr(0,filename.find_first_of("_"));
    treeId = myDataset._speciesColorTable.name2label(treeName);
}
void PixelImage::speciesValidation(cv::Mat *confusionMatrix)
{
    if (_isTrain)
        return;
//    string filename = FileAPI::extractFileName(_filenameImage, false);
//    string treename = filename.substr(0,filename.find_first_of("_"));
//    int trueTreeId = myDataset._treeCategory.name2id(treename);
    int numClass = myDataset._speciesColorTable.nCategory();
    int trueTreeId;
    string trueTreeName;
    getTreeTypeFromName(trueTreeName, trueTreeId);

    cout << "true tree category " << trueTreeName<< endl;

    assert(trueTreeId < numClass);
    assert(trueTreeId >= 0);
    assert(_treeId >= 0);
    assert(trueTreeId < numClass);
    confusionMatrix->at<int>(trueTreeId, _treeId) += 1;
}



//////////// ************** pixel level *********************///////////
void PixelImage::setPixelProbMat(const vector<cv::Point> &pts, const vector<vector<float> > &probs, const bool isSegmentation)
{
    assert(pts.size() == probs.size());
    assert(!probs.empty());
    int numClass = probs[0].size();

    assert(pts.size() == probs.size() );

    vector<vector<vector<float> > > probMats = vector< vector< vector<float> > >(_image.rows, vector< vector< float > >(_image.cols, vector<float>(numClass, 0)));
    //vector<cv::Mat> probMats(numClass, cv::Mat(_image.rows, _image.cols, CV_32F));

    for (size_t i = 0; i < pts.size(); ++i)
    {
        int x = pts[i].x;
        int y = pts[i].y;

        for (int j = 0; j < numClass; ++j)
        {
            //_classProbMats[j].at<float>(y,x) = probs[i][j];
            probMats[y][x][j] = probs[i][j];
            //_treeTypeProbMat[j].at<float>(y,x) = probs[i][j];
            //probMats[j].at<float>(y,x) = probs[i][j];
            //cout << "pos " << x << ", " << y << ", tid " << j << ", prob " << probs[i][j] << ", "<< probMats[j].at<float>(y,x)<< endl;

        }
    }

    if (isSegmentation)
        _parsProbMats = probMats;
    else
        _speciesProbMats = probMats;

//    for (int y = 0; y < _image.rows; ++y)
//        for (int x = 0; x < _image.cols; ++x)
//            for (int j = 0; j < numClass; ++j)
//                cout << "pos " << x << ", " << y << ", tid " << j << ", prob " << _parsProbMats[y][x][j]<< endl;

}

void PixelImage::viewPixelProbMap(const bool isSegmentation)
{
    int nClass;
    vector<vector<vector<float> > > *probMats;
    vector<string> sortedNames;
    ColorCategoryTable *colorTabel;
    string conf_dir = myDataset._confMapDir;
    if (isSegmentation)
    {
        nClass = myDataset._parsColorTable.nCategory();
        probMats = &_parsProbMats;
        sortedNames = myDataset._parsColorTable.getSortedClassNames();
        colorTabel = &(myDataset._parsColorTable);
        conf_dir += "/pars/";
    }
    else // species
    {
        nClass = myDataset._speciesColorTable.nCategory();
        probMats = &(_speciesProbMats);
        sortedNames = myDataset._speciesColorTable.getSortedClassNames();
        colorTabel = &(myDataset._speciesColorTable);
        conf_dir += "/species/";
    }

    for (int i = 0; i < nClass; ++i)
    {
        std::string mapname = conf_dir + FileAPI::extractFileName(_filenameImage, false) + "_" +sortedNames[i] +".png";
        cv::Mat probMap(_image.rows, _image.cols, CV_8UC3);
        cv::Vec3b color = colorTabel->label2color(i);

        for (int y = 0; y < _image.rows; ++y)
            for (int x = 0; x < _image.cols; ++x)
            {
                float prob = probMats->at(y)[x][i];
                //float prob = probMats->at(i).at<float>(y,x);
                for (int k = 0; k < 3; ++k)
                    probMap.at<cv::Vec3b>(y,x)[k] = int(color[k] * prob);
            }
        cv::imwrite(mapname, probMap);
        //cout << "save conf map " << mapname << endl;
    }
}

void PixelImage::setSegResult(const vector<vector<vector<float> > > &probMats,
                               cv::Mat* segMat)
{
    assert(!probMats.empty());
    int nClass = probMats[0][0].size();
    int height = _image.rows;
    int width = _image.cols;

    vector<int> data_num(nClass,0);
    for (int y = 0; y < height; ++y)
        for (int x = 0; x < width; ++x)
        {
            if (_activeMask.at<uchar>(y,x) > 0)
            {
                float maxProb = 0;
                int maxId = -1;
                for (int i = 0; i < nClass; ++i)
                {
                    //float currentProb = probMats[i].at<float>(y,x);
                    float currentProb = probMats[y][x][i];
                    //cout <<x<<"," <<y<<", id " << i << ", " << currentProb << endl;

                    if (maxProb < currentProb)
                    {
                        maxProb = currentProb;
                        maxId = i;
                    }
                }
                segMat->at<int>(y,x) = maxId;
                //cout <<x<<"," <<y<<", id " << maxId << endl;
                if (maxId >= 0 && maxId < nClass)
                    data_num[maxId]++;
            }
            else
                segMat->at<int>(y,x) = -1;
        }
    for (int i = 0; i < nClass; ++i)
        cout << "label " << i << ", sample num " << data_num[i] << endl;
}

void PixelImage::saveSpeciesSegResult(vector<float> *speciesProbs)
{
    _speciesSegMat = cv::Mat(_image.rows, _image.cols, CV_32SC1);
    int nClass = _speciesProbMats[0][0].size();
    setSegResult(_speciesProbMats, &_speciesSegMat);

    cv::Mat segResultImg = cv::Mat::zeros(_image.rows, _image.cols, CV_8UC3);

    int nValid = 0;
    for (int y = 0; y < _image.rows; ++y)
        for (int x = 0; x < _image.cols; ++x)
        {
            if (_activeMask.at<uchar>(y,x) > 0)
            {
                int label = _speciesSegMat.at<int>(y,x);
                if (label >= 0 && label < nClass)
                    speciesProbs->at(label) += 1;
                cv::Vec3b color = myDataset._speciesColorTable.label2color(label);
                segResultImg.at<cv::Vec3b>(y,x) = color;
                nValid++;
            }

        }
    for (int i = 0; i < nClass; ++i)
        speciesProbs->at(i) /= (float)nValid;

    string name = myDataset._resultDir +"/species_seg/" + FileAPI::extractFileName(_filenameImage, false) + "_speciesseg.png";
    cv::imwrite(name, segResultImg);
}

void PixelImage::saveParsSegResult()
{
    _parsSegMat = cv::Mat(_image.rows, _image.cols, CV_32SC1);
    cv::Mat segResultImg = cv::Mat::zeros(_image.rows, _image.cols, CV_8UC3);
    int nClass = myDataset._parsColorTable.nCategory();

    setSegResult(_parsProbMats, &_parsSegMat);
    for (int y = 0; y < _image.rows; ++y)
        for (int x = 0; x < _image.cols; ++x)
        {
            if (_activeMask.at<uchar>(y,x) > 0)
            {
                int label = _parsSegMat.at<int>(y,x);

                cv::Vec3b color = myDataset._parsColorTable.label2color(label);
                segResultImg.at<cv::Vec3b>(y,x) = color;
            }

        }

    string name = myDataset._resultDir +"/pars_seg/" + FileAPI::extractFileName(_filenameImage, false) + "_parsseg.png";
    cv::imwrite(name, segResultImg);

}

void PixelImage::saveSegResult(const bool isSegmentation)
{

}

//void PixelImage::saveSegResult(vector<float>& treeTypeProbs)
//{
//    _segResult = cv::Mat(_image.rows, _image.cols, CV_32SC1);
//    cv::Mat segResultImg = cv::Mat::zeros(_image.rows, _image.cols, CV_8UC3);
//    int nClass = myDataset._speciesColorTable.nCategory();

//    treeTypeProbs = vector<float>(nClass, 0);
//    int nValid = 0;

//    for (int y = 0; y < _image.rows; ++y)
//        for (int x = 0; x < _image.cols; ++x)
//        {
//            if (_activeMask.at<uchar>(y,x) > 0)
//            {
//                float maxProb = 0;
//                int maxId = -1;
//                for (int i = 0; i < nClass; ++i)
//                {
//                    //float currentProb = _classProbMat[y][x][i];
//                    float currentProb = _treeTypeProbMat[i].at<float>(y,x);
//                    //cout <<x<<"," <<y<<", id " << i << ", " << currentProb << endl;

//                    if (maxProb < currentProb)
//                    {
//                        maxProb = currentProb;
//                        maxId = i;
//                    }
//                }
//                cv::Vec3b color;
//                if (myDataset._operation == MyDataset::TREESEGEMNTATION)
//                    color = myDataset._parsColorTable.label2color(maxId);
//                else if (myDataset._operation == MyDataset::TREETYPECLASSIFICATION)
//                    color = myDataset._speciesColorTable.label2color(maxId);
//                segResultImg.at<cv::Vec3b>(y,x) = color;
//                _segResult.at<int>(y,x) = maxId;

//                if (maxId >= 0 && maxId < nClass)
//                    treeTypeProbs[maxId]++;
//                nValid++;

//            }
//            else
//                _segResult.at<int>(y,x) = -1;
//        }

//    for (int i = 0; i < nClass; ++i)
//        treeTypeProbs[i] /= (float)nValid;

//    std::string name;
//    if (myDataset._operation == MyDataset::TREESEGEMNTATION)
//        name = myDataset._resultDir +"/pars_seg/" + FileAPI::extractFileName(_filenameImage, false) + "_parsseg.png";
//    else if (myDataset._operation == MyDataset::TREETYPECLASSIFICATION)
//        name = myDataset._resultDir +"/type_seg/" + FileAPI::extractFileName(_filenameImage, false) + "_typeseg.png";

//    cv::imwrite(name, segResultImg);
//}

void PixelImage::speciesPixelValidation(cv::Mat *confusionMatrix)
{
    if (_isTrain)
        return;
    assert(!_speciesSegMat.empty());

    string filename = FileAPI::extractFileName(_filenameImage, false);
    string treename = filename.substr(0,filename.find_first_of("_"));
    int trueTreeId = myDataset._speciesColorTable.name2label(treename);
    cout << "true tree category " << treename << endl;
    int numClass = myDataset._speciesColorTable.nCategory();
    assert(trueTreeId < numClass);
    assert(trueTreeId >= 0);

    for (int y = 0; y < _image.rows; ++y)
        for (int x = 0; x < _image.cols; ++x)
        {
            if (_activeMask.at<uchar>(y,x) > 0)
            {
                int resultId = _speciesSegMat.at<int>(y,x);
                if (resultId >= 0 && resultId < numClass)
                {
                    confusionMatrix->at<int>(trueTreeId, resultId)++;
                }
            }
        }
}

void PixelImage::parsPixelValidation(cv::Mat *confusionMatrix)
{
    assert(!_parsSegMat.empty());
    int numClass = _parsProbMats.size();

    for (int y = 0; y < _image.rows; ++y)
        for (int x = 0; x < _image.cols; ++x)
        {
            if (_activeMask.at<uchar>(y,x) > 0)
            {
                int resultId = _parsSegMat.at<int>(y,x);
                int trueLabel = myDataset._parsColorTable.color2label(_labelMap.at<cv::Vec3b>(y,x));

                if (resultId >= 0 && resultId < numClass && trueLabel >= 0 && trueLabel < numClass)
                {
                    confusionMatrix->at<int>(trueLabel, resultId)++;
                }
            }
        }
}

int PixelImage::getWidth() const
{
    return _image.cols;
}

int PixelImage::getHeight() const
{
    return _image.rows;
}

int PixelImage::getChannels() const
{
    return _image.channels();
}

cv::Mat PixelImage::getTarget(int dstw, int dsth) const
{


    assert(!_image.empty());
    assert(!_activeMask.empty());

    int srcw = _image.rows;
    int srch = _image.cols;

    cv::Mat full_target = cv::Mat::zeros(srch, srcw, CV_8UC3);
    _image.copyTo(full_target, _activeMask);
    //  get bounding box
    int minX = _image.cols;
    int maxX = 0;
    int minY = _image.rows;
    int maxY = 0;
    for (int y = 0; y < _image.rows; ++y)
        for (int x = 0; x < _image.cols; ++x)
            if (_activeMask.at<uchar>(y,x) == 255)
            {
                if (x < minX)
                    minX = x;
                if (x > maxX)
                    maxX = x;
                if (y < minY)
                    minY = y;
                if (y > maxY)
                    maxY = y;
            }

    int boxw = maxX - minX + 1;
    int boxh = maxY - minY + 1;
    cout << "BoundingBox [" << minX <<","<<maxX<<"], [" <<minY<<","<<maxY<<"]"<<endl;

    //  get original target

    if (dstw < 0 || dsth < 0)
    {
        dstw = boxw;
        dsth = boxh;
    }
    cv::Mat ori_target(boxh, boxw, CV_8UC3);
    cv::Mat target = cv::Mat::zeros(dsth, dstw, CV_8UC3);

//    for (int y = 0; y <= boxh; ++y)
//    {
//        int oriy = y + minY;
//        for (int x = 0; x <= boxw; ++x)
//        {
//            int orix = x + minX;
//            if (_mask.at<uchar>(oriy, orix) == 255)
//                ori_target.at<cv::Vec3b>(y,x) = _image.at<cv::Vec3b>(oriy, orix);
//        }
//    }
    //full_target.copyTo(full_target(cv::Rect(minX, minY, boxw, boxh)));
    ori_target = full_target(cv::Rect(minX, minY, boxw, boxh));

    // resize original target

    int dstw_scale, dsth_scale;

    if (boxw > boxh)
    {
        dstw_scale = dstw;
        dsth_scale = float(dstw_scale) / float(boxw) * float(boxh);
    }
    else
    {
        dsth_scale = dsth;
        dstw_scale = float(dsth_scale) / float(boxh) * float(boxw);
    }

    cv::Mat scale_target(dsth_scale, dstw_scale, CV_8UC3);
    cv::resize(ori_target, scale_target, scale_target.size());
    cout << "resize " << scale_target.rows << ", " << scale_target.cols << endl;
    cout << "target " << target.rows << ", " << target.cols << endl;
    // resized target copy to dst target
    scale_target.copyTo(target(cv::Rect(dstw/2 - dstw_scale/2,
                                        dsth/2 - dsth_scale/2,
                                        dstw_scale,
                                        dsth_scale)));
//    imwrite("oritarget.png", ori_target);
//    imwrite("resizetarget.png", scale_target);
//    imwrite("dsttarget.png", target);
//    cout << "end get target " << endl;

    return target;
}

cv::Rect PixelImage::getTargetBox() const
{
    assert(!_image.empty());
    assert(!_activeMask.empty());

    int minX = _image.cols;
    int maxX = 0;
    int minY = _image.rows;
    int maxY = 0;
    for (int y = 0; y < _image.rows; ++y)
        for (int x = 0; x < _image.cols; ++x)
            if (_activeMask.at<uchar>(y,x) == 255)
            {
                if (x < minX)
                    minX = x;
                if (x > maxX)
                    maxX = x;
                if (y < minY)
                    minY = y;
                if (y > maxY)
                    maxY = y;
            }

    int boxw = maxX - minX + 1;
    int boxh = maxY - minY + 1;
    cout << "BoundingBox [" << minX <<","<<maxX<<"], [" <<minY<<","<<maxY<<"]"<<endl;

    cv::Rect ret(minX, minY, boxw, boxh);
    return ret;
}

cv::Mat PixelImage::getOriTarget() const
{
    assert(!_image.empty());
    assert(!_activeMask.empty());

    cv::Mat full_target = cv::Mat::zeros(_image.rows, _image.cols, CV_8UC3);
    _image.copyTo(full_target, _activeMask);

    return full_target;
}

void PixelImage::refineCrown()
{
    //cv::Vec3b crowncolor = myDataset._parsColorTable.name2color("crown");
    //setAtivePart(crowncolor);
    int crownid = myDataset._parsColorTable.name2label("crown");
    assert(!_parsSegMat.empty());

    // set the classified crown part active
    _activeMask = cv::Mat::zeros(_image.rows, _image.cols, CV_8UC1);
    for (int i = 0; i < _image.rows; ++i)
        for (int j = 0; j < _image.cols; ++j)
            if (_parsSegMat.at<int>(i,j) == crownid)
                _activeMask.at<uchar>(i,j) = 255;

    FeatureExtractor extractor;
    vector<FeatVector> features;
    extractor.extractPixelFeatureScreened(myDataset._featParameters, (*this), features);

    //Clustering cluster;
    int numCluster = 2;
    int numSamples = features.size();
    int featDim = features[0].getDim();

    cv::Mat data(numSamples, numCluster, CV_32F);
    cv::Mat bestLabels;
    cv::Mat centers;
    int attempts = 5;

    for (int i = 0; i < numSamples; ++i)
        for (int j = 0; j < numCluster; ++j)
            data.at<float>(i,j) = features[i]._data[j];

    cv::kmeans(data, numCluster,
               bestLabels,
               cv::TermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 10000, 0.0001),
               attempts,
               cv::KMEANS_PP_CENTERS,
               centers);

    cv::Mat re_crown_map = cv::Mat::zeros(_image.rows, _image.cols, CV_8UC3);

    for (int i = 0; i < numSamples; ++i)
    {
        int cluster_id = bestLabels.at<int>(i);
        int x = features[i]._x;
        int y = features[i]._y;
        if (cluster_id == 0)
            re_crown_map.at<cv::Vec3b>(y,x) = cv::Vec3b(255,0,0);
        else
            re_crown_map.at<cv::Vec3b>(y,x) = cv::Vec3b(0,255,0);
    }
    string re_crown_name = myDataset._resultDir + "refine_crown/" + FileAPI::extractFileName(_filenameImage, false) + "_recrown.png";
    cv::imwrite(re_crown_name, re_crown_map);
}


//////////// MRF ///////////////////////
void PixelImage::pixelMRFInference(const bool isSegmentation)
{
    int numClass = myDataset._parsColorTable.nCategory();
    ColorCategoryTable& colorTable = myDataset._parsColorTable;
    cv::Mat& segResult = this->_parsSegMat;
    vector<vector<vector<float> > > & probMats = _parsProbMats;

    if (!isSegmentation)
    {
        numClass = myDataset._speciesColorTable.nCategory();
        colorTable = myDataset._speciesColorTable;
        segResult = this->_speciesSegMat;
        probMats = _speciesProbMats;
    }
    segResult = cv::Mat(_image.rows, _image.cols, CV_32S);

    GraphModel pixelCRF(numClass);

    if(GraphModel::s_initialized == false)
        GraphModel::SetConfiguration(NULL);

    int numPts = _image.rows * _image.cols;
    cv::Mat probability = cv::Mat::zeros(numPts, numClass, CV_32F);
    cv::Mat probability_index = cv::Mat::zeros(numPts, 2, CV_32S);

    for (int y = 0; y < _image.rows; ++y)
        for (int x = 0; x < _image.cols; ++x)
        {
            int idx = y * _image.cols + x;

            probability_index.at<int>(idx, 0) = x;
            probability_index.at<int>(idx, 1) = y;

            for (int j = 0; j < numClass; ++j)
            {
                probability.at<float>(idx, j) = probMats[y][x][j];
                //cout << y << "," << x << "," << j << ": " << probMats[y][x][j] << endl;
            }
        }

    pixelCRF.setupGraph(this,&probability,&probability_index);
    pixelCRF.mapInference(false);

    for(unsigned int i=0;i<pixelCRF._layer.size();i++)
    {
        int x = pixelCRF._layer.at(i)->_x;
        int y = pixelCRF._layer.at(i)->_y;
        //int label = pixelCRF._layer.at(i)->_predictLabel-1;
        int label = pixelCRF._layer.at(i)->_predictLabel;

        if(label>=0)
        {
//        	int r = myDataset.colorTable.colorCategorySet.at(label).r;
//        	int g = myDataset.colorTable.colorCategorySet.at(label).g;
//        	int b = myDataset.colorTable.colorCategorySet.at(label).b;
//        	cvSet2D(segResult,y,x,cvScalar(b,g,r));
            //cv::Vec3b color = colorTable.label2color(label);
            //segResult.at<cv::Vec3b>(y,x) = color;
            //cout << y << ", " << x << ": " << label <<endl;
            segResult.at<int>(y,x) = label;
        }
    }
}


bool PixelImage::saveConfidenceMap(const bool isSegmeantion)
{
    string name = myDataset._resultDir + "confidence/" + FileAPI::extractFileName(_filenameImage, false);
    ColorCategoryTable& colorTable = myDataset._parsColorTable;
    vector<vector<vector<float> > > & probMat = this->_parsProbMats;
    if (isSegmeantion)
    {
        name += "_pars_confidence_pixel.txt";
    }
    else
    {
        name += "_species_confidence_pixel.txt";
        probMat = this->_speciesSegMat;
        colorTable = myDataset._speciesColorTable;
    }

    cout << "save " << name << endl;

    vector<string> cate_names = colorTable.getSortedClassNames();
    int nClass = colorTable.nCategory();

    ofstream out;
    out.open(name.c_str());

    out <<"*image-size(w,h): " << _image.cols << " " << _image.rows << endl;
    out << "*category-order: ";
    for (size_t i = 0; i < colorTable.nCategory(); ++i)
        out << cate_names[i] << " ";
    out << endl;
    out << "# format: <x,y> <prob i>" << endl;

    for (int y = 0; y < _image.rows; ++y)
        for (int x = 0; x < _image.cols; ++x)
        {
            out << x << " " << y;

            for (int k = 0; k < nClass; ++k)
                out << " " << probMat[y][x][k];
            out << endl;
        }
    out.close();
    return true;
}

bool PixelImage::loadConfidenceMap(const bool isSegmentation)
{
    string name = myDataset._resultDir + "confidence/" + FileAPI::extractFileName(_filenameImage, false);
    ColorCategoryTable& colorTable = myDataset._parsColorTable;
    vector<vector<vector<float> > > & probMat = this->_parsProbMats;
    if (isSegmentation)
    {
        name += "_pars_confidence_pixel.txt";
    }
    else
    {
        name += "_species_confidence_pixel.txt";
        probMat = this->_speciesSegMat;
        colorTable = myDataset._speciesColorTable;
    }

    cout << "load " << name << endl;

    ifstream in(name.c_str());
    if (!in.is_open())
    {
        cout << "faild" << endl;
        return false;
    }
    int nClass = colorTable.nCategory();

    probMat = vector<vector<vector<float> > >(_image.rows, vector<vector<float> >(_image.cols, vector<float>(nClass, 0)));
    while(!in.eof())
    {
        string line;
        getline(in, line);

        if (line.size() > 1)
        {
            if (line[0] == '*' || line[0] == '#')
                continue;


            vector<string> elements;
            boost::split(elements, line, boost::is_any_of(" "), boost::token_compress_on);

            if (elements.size() >= 3)
            {
                int x = boost::lexical_cast<int>(elements[0]);
                int y = boost::lexical_cast<int>(elements[1]);

                vector<float> probs;
                for (int k = 2; k < elements.size(); ++k)
                {
                    float prob = boost::lexical_cast<float>(elements[k]);
                    probs.push_back(prob);
                }

                probMat[y][x] = probs;
            }
        }
    }
}

/////////////////////// Estimate trunk //////////////////
void PixelImage::estimateBranch()
{
    assert(!this->_parsProbMats.empty());
    int branchId = myDataset._parsColorTable.name2label("branch");

    cv::Mat br_prob_map(_image.rows, _image.cols, CV_32F);


    for (int y = 0; y < _image.rows; ++y)
        for(int x = 0; x < _image.cols; ++x)
            br_prob_map.at<float>(y,x) = _parsProbMats[y][x][branchId];

    BarkEstimator barkestimator;
    barkestimator.getCandidateWindow(*this, br_prob_map, 9);
}
