#ifndef TREECATEGORY_H
#define TREECATEGORY_H

#include <string>
#include <map>
#include <vector>
#include <opencv2/core/core.hpp>



class TreeCategory
{
public:
    TreeCategory();
    void constructTable();

    int name2id(std::string name);  //id is label
    std::string id2name(int id);
    int nCategories() const;
    std::vector<std::string> getTreeTypeNames() const;
    void getLabels(std::vector<int>& labels);

    cv::Vec3b id2color(int id);

    ////////////////////////
public:
    std::map<int,std::string> _idNameTable;
    std::map<int,cv::Vec3b> _idColorTable;
};

#endif // TREECATEGORY_H
