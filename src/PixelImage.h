#ifndef PIXELIMAGE_H
#define PIXELIMAGE_H

//#include <Image/Image.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "Feature.h"
#include "ColorCategory.h"

class BasicImage
{
public:
    BasicImage();
    BasicImage(const std::string filenameImg,
               const std::string filenameMask = "",
               bool _isTrain = false);

    virtual ~BasicImage(){}

    void setImageName(const std::string filename);
    void setMaskName(const std::string filename);
    void setIsTrain(const bool isTrain);

public:
    std::string _filenameImage;
    std::string _filenameMask;
    std::string _filenameSuperPixel;
    bool _isTrain;
};

class PixelImage : public BasicImage
{
public:
    PixelImage();
    PixelImage(const BasicImage& bimg);
    PixelImage(const std::string filenameImg,
               std::string filenameMask = "",
               bool isTrain = false);

    virtual ~PixelImage(){}
    bool loadImage();
    bool loadLabelMap();
    void initLabelMap();

    void setParsLabelMat();

    void initialize();

    void setAtivePart(const cv::Vec3b& activeColor);

    /* image level */
    void constructShapeMaskFeature(FeatVector& shapeFeature) const;
    void constructContourMoment(FeatVector& momentFeature) const;
    void constructEdgeHist(FeatVector& edgeHist) const;

    /* tree type */
    void getTreeTypeFromName(std::string& treeName, int& treeId) const;
    void setTreeTypeProbs(const std::vector<float>& probVec);
    void saveTreeTypeProbFile();

    void speciesValidation(cv::Mat* confusionMatrix);
    void speciesPixelValidation(cv::Mat* confusionMatrix);

    void parsPixelValidation(cv::Mat* confusionMatrix);

    /* pixel level */
    void setPixelProbMat(const std::vector<cv::Point>& pts,
                         const std::vector<std::vector<float> > &probs,
                         const bool isSegmentation);
    void viewPixelProbMap(const bool isSegmentation);

    void setSegResult(const std::vector<std::vector<std::vector< float > > >& probMats,
                       cv::Mat* segMat);

    void saveSpeciesSegResult(std::vector<float>* speciesProbs);

    void saveParsSegResult();

    void saveSegResult(const bool isSegmentation);

    //void saveSegResult(std::vector<float>& treeTypeProbs);

    //help function
    void pixelNeighbors(const cv::Point& pixel, std::vector<cv::Point>& neighbors) const;
    bool isAtMaskBound(const cv::Point& point) const;

    void resizeTo(const int width, const int height, PixelImage& dstPimg) const;

    /* basic information */
    int getWidth() const;
    int getHeight() const;
    int getChannels() const;

    /* refine crown
     */
    void refineCrown();

    /* estimate main trunk
     */
    void estimateBranch();

    /* get target */
    cv::Mat getTarget(int dstw = -1, int dsth = -1) const;
    cv::Mat getOriTarget() const;
    cv::Rect getTargetBox() const;


    /* MRF
     */
    void pixelMRFInference(const bool isSegmentation);

    /* Confidence I O
     */
    bool saveConfidenceMap(const bool isSegmentation);

    bool loadConfidenceMap(const bool isSegmentation);

public:

    /* input
     */
    cv::Mat _image;
    cv::Mat _labelMap;   // for segmentation label (map is for storing color, while mat is for storing figure)
    cv::Mat _parsLabelMat;  // for segmentation label mat, store the true labels
    cv::Mat _activeMask;  // only the ative pixels extract feature

    /* output
     */
    // for tree species confidence
    //std::vector<cv::Mat> _speciesProbMats;
    std::vector<std::vector<std::vector<float> > > _speciesProbMats; // dimension [y][x][class]
    cv::Mat _speciesSegMat;
    int _treeId;    //image level
    std::vector<float> _treeTypeProbs;

    //  for tree segmentation
    //std::vector<cv::Mat> _parsProbMats;  //  for tree segmentation confidence
    std::vector<std::vector<std::vector<float> > > _parsProbMats;  // dimension [y][x][class]
    cv::Mat _parsSegMat;
};

#endif // PIXELIMAGE_H
