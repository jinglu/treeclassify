#include "SuperPixelImage.h"
#include <Primitives/Primitives.h>
#include <Image/Image.h>
#include "SLIC.h"
#include "GloabalVal.h"
#include <Common/FileAPI.h>

SuperPixel::SuperPixel()
    : _id(-1)
{
}

SuperPixel::SuperPixel(const int id)
    : _id(id)
{

}

void SuperPixel::setCenter()
{
    double cx = 0, cy = 0;
    int num = this->_points.size();
    for (int i = 0; i < num; ++i)
    {
        cx += this->_points.at(i).x;
        cy += this->_points.at(i).y;
    }
    cx /= (double)num;
    cy /= (double)num;

    this->_center.x = (int)cx;
    this->_center.y = (int)cy;
}

void SuperPixel::setBoundBox()
{
    int left, right, top, bottom;
    wjl::Rectangle rect;
    wjl::Rectangle::getBoundBox(this->_points, rect);
    this->_boundbox = rect._cvrect;
}

////////////////////////////////////////////////////////////////////////////
SuperPixelImage::SuperPixelImage()
{
}

SuperPixelImage::SuperPixelImage(const BasicImage &bimg, const SPType sptype)
    : PixelImage(bimg)
{
    constructSP(sptype);
}

SuperPixelImage::SuperPixelImage(const PixelImage &pimg, const SPType sptype)
    : PixelImage(pimg)
{
    constructSP(sptype);
}

void SuperPixelImage::constructSP(const SPType sptype)
{
    if (sptype == SuperPixelImage::SuperSLIC)
        constructSLICSP();
}

bool SuperPixelImage::constructSLICSP()
{
    SLIC oversegmentor;
    int width = this->getWidth();
    int height = this->getHeight();

    int* klabels = new int[width*height];
    int  numlabels;
    int  superpixelSize = 160;  // default parameter
    int  K = width*height/superpixelSize + 1;
    double m = 40;

    IplImage *ptImage = new IplImage(this->_image);

    oversegmentor.DoSuperpixelSegmentation_ForGivenK(ptImage,width,height,klabels,numlabels,K,m);

    cv::Mat superpixelMat(height, width, CV_32S);
    int maxSPIndex = 0;
    for (int i = 0; i < height; ++i)
        for (int j = 0; j < width; ++j)
        {
            superpixelMat.at<int>(i,j) = klabels[i*width+j]+1;
            if (maxSPIndex < superpixelMat.at<int>(i,j))
                maxSPIndex = superpixelMat.at<int>(i,j);
        }

    delete[] klabels;
    if(maxSPIndex == 0)
    {
        cout<<"wrong super-pixel Matrix"<<endl;
        return false;
    }

    //  init super pixels
    for (int i = 0; i < maxSPIndex; ++i)
    {
        SuperPixel sp(i+1);
        _superPixels.push_back(sp);
    }

    //  insert points in the superpxels
    for (int i = 0; i < height; ++i)
        for (int j = 0; j < width; ++j)
        {
            int spid = superpixelMat.at<int>(i,j) - 1;
            _superPixels[spid]._points.push_back(cv::Point(j,i));
        }

    for (size_t i = 0; i < _superPixels.size(); ++i)
    {
        _superPixels[i].setCenter();

        list<int> adjSPIndexSet;
        for (size_t j = 0; j < _superPixels[i]._points.size(); ++j)
        {
            int x = _superPixels[i]._points[j].x;
            int y = _superPixels[i]._points[j].y;

            int spIndex = superpixelMat.at<int>(y,x);

            for (int m = -1; m <= 1; ++m)
                for (int n = -1; n <= 1; ++n)
                {
                    if (x+n >= 0 && x+n < width && y+m >= 0 && y+m < height)
                    {
                        int adjSpIndex = superpixelMat.at<int>(y+m, x+n);
                        if (adjSpIndex != spIndex)
                            adjSPIndexSet.push_back(adjSpIndex);
                    }
                }
        }

        adjSPIndexSet.sort();
        adjSPIndexSet.unique();

        for (list<int>::iterator it = adjSPIndexSet.begin(); it != adjSPIndexSet.end(); ++it)
            _superPixels[i]._adjSpIds.push_back(*it);
    }

    cout << "in SP: numLabel " << numlabels << ", maxSPID " << maxSPIndex <<", numSP " <<_superPixels.size() << endl;
    _superPixelMap = superpixelMat;
    return true;
}

void SuperPixelImage::viewSuperPixel(bool isDrawBoundary)
{
    int numSuperPixel = _superPixels.size();
    int width = _image.cols;
    int height = _image.rows;
    wjl::Image spimg(width, height);

    vector<wjl::Color> color_table;
    int bit_per_channle = ceil(log2(numSuperPixel*2+1)/3.0);
    int bits_total      = 3*bit_per_channle;
    int color_num       = 1 << bits_total;

    for(int c = 0; c < color_num; c++)
    {
        int r = 0;
        int g = 0;
        int b = 0;
        for(int k = 0; k < bits_total;)
        {
            b = (b << 1) + ((c >> k++) & 1);
            g = (g << 1) + ((c >> k++) & 1);
            r = (r << 1) + ((c >> k++) & 1);
        }
        r = r << (8 - bit_per_channle);
        g = g << (8 - bit_per_channle);
        b = b << (8 - bit_per_channle);

        wjl::Color color(r,g,b);
        color_table.push_back(color);
    }


    for (int y = 0; y < height; ++y)
        for (int x = 0; x < width; ++x)
        {
            int spIndex = _superPixelMap.at<int>(y,x);
            spimg.set_pixel(x,y, color_table[spIndex-1]);
        }
    //draw boundary
    if (isDrawBoundary)
    {
    for (int x = 1; x < width-1; ++x)
        for (int y = 1; y < height-1; ++y)
        {
            int spId = _superPixelMap.at<int>(y,x);
            int leftId = _superPixelMap.at<int>(y,x-1);
            int rightId = _superPixelMap.at<int>(y,x+1);
            int aboveId = _superPixelMap.at<int>(y-1,x);
            int belowId = _superPixelMap.at<int>(y+1,x);
            if (spId != leftId || spId != rightId || spId != aboveId || spId != belowId)
                spimg.set_pixel(x,y,wjl::Color::red());

        }
    }

    wjl::Image oriimg(this->_image);

    float alpha = 0.75;
    wjl::Image::alpha_blend(oriimg, alpha, spimg, 1.0-alpha, 0.0, spimg);

    string spname = myDataset._rootDir + "/superpixel/" + FileAPI::extractFileName(this->_filenameImage,false) + "_sp.png";
    spimg.write(spname);
}


/*remove the small holes in the segmentation result*/
void SuperPixelImage::segmentationPostProcessing()
{
    vector< vector<cv::Point> > regions;
    int height = _image.rows;
    int width = _image.cols;
    int nchannel = _image.channels();

    cv::Mat flagMask = cv::Mat::zeros(_image.rows, _image.cols, CV_32SC1);

    cv::Mat * segResult = &_parsSegMat;
    if (myDataset._operation == MyDataset::TREESPECIESCLASSIFICATION)
        segResult = &_speciesSegMat;

    /*connected components*/
    vector<cv::Point> stack;
    for(unsigned int i=0;i<height;i++)
    {
        for(int j=0;j<width;j++)
        {
            if(flagMask.at<int>(i,j) == 0)
            {
                stack.clear();
                vector<cv::Point> region;
                stack.push_back(cv::Point(j,i));
                flagMask.at<int>(i,j) = 1;

                while(stack.size()>0)
                {
                    cv::Point p = stack.at(stack.size()-1);
                    stack.pop_back();
                    region.push_back(p);

                    for(int m = -1;m<=1;m++)
                    {
                        for(int n = -1;n<=1;n++)
                        {
                            if((p.y+m)>=0 && (p.y+m)<height &&
                               (p.x+n)>=0 && (p.x+n)<width)
                            {
                                if(flagMask.at<int>(p.y+m,p.x+n) == 0)
                                {
                                    bool insertFlag = true;
                                    if (segResult->at<int>(p.y, p.x) != segResult->at<int>(p.y+m, p.x+n))
                                    {
                                        insertFlag = false;
                                        break;
                                    }
//									for(int k = 0;k<nchannel;k++)
//									{
//										if(segResult->imageData[p.y*segResult->widthStep+p.x*segResult->nChannels+k] !=
//										   segResult->imageData[(p.y+m)*segResult->widthStep+(p.x+n)*segResult->nChannels+k])
//										{
//											InsertFlag = false;
//											break;
//										}
//									}

                                    if(insertFlag == true)
                                    {
                                        stack.push_back(cv::Point(p.x+n,p.y+m));
                                        flagMask.at<int>(p.y+m,p.x+n) = 1;
                                    }
                                }
                            }
                        }
                    }
                }
                regions.push_back(region);
            }
        }
    }


    vector< list<int> > adjRegions;
    cv::Mat regionMap = cv::Mat::zeros(height,width,CV_32SC1);

    for(unsigned int i=0;i<regions.size();i++)
    {
        for(int j=0;j<regions.at(i).size();j++)
        {
            int x = regions.at(i).at(j).x;
            int y = regions.at(i).at(j).y;
            //regionMap.at<int>(y,x) = i + 1;
            regionMap.at<int>(y,x) = i;
        }
        list<int> adjRegion;
        adjRegions.push_back(adjRegion);
    }

    for(unsigned int i=1;i<height-1;i++)
    {
        for(int j=1;j<width-1;j++)
        {
            int clusterIdx = regionMap.at<int>(i,j);
            for(int m = -1;m<=1;m++)
            {
                for(int n = -1;n<=1;n++)
                {
                    int adjClusterIdx = regionMap.at<int>(i+m,j+n);
                    if(clusterIdx != adjClusterIdx)
                    {
                        adjRegions.at(clusterIdx-1).push_back(adjClusterIdx);
                    }
                }
            }
        }
    }

    for(unsigned int i=0;i<adjRegions.size();i++)
    {
        adjRegions.at(i).sort();
        adjRegions.at(i).unique();
    }

    for(unsigned int i=0;i<regions.size();i++)
    {
        if(regions.at(i).size()<200)
        {
            int maxCategorySize = 0;
            int selectedClusterIdx = 0;

            for(list<int>::iterator iter=adjRegions.at(i).begin();
                                    iter!=adjRegions.at(i).end();iter++)
            {
                if(regions.at(*iter - 1).size() > maxCategorySize && regions.at(*iter - 1).size() > regions.at(i).size())
                {
                    selectedClusterIdx = *iter- 1;
                    maxCategorySize = regions.at(selectedClusterIdx).size();
                }
            }

            if(maxCategorySize>0)
            {
                //cout<<"update the label of small region"<<endl;
//				CvScalar s = cvGet2D(segResult,Regions.at(selectedClusterIdx).at(0).y,
//						                       Regions.at(selectedClusterIdx).at(0).x);
                int label = segResult->at<int>(regions.at(selectedClusterIdx).at(0).y, regions.at(selectedClusterIdx).at(0).x);

                for(int j=0;j<regions.at(i).size();j++)
                {
                    //cvSet2D(segResult,Regions.at(i).at(j).y,Regions.at(i).at(j).x,s);
                    segResult->at<int>(regions.at(i).at(j).y,regions.at(i).at(j).x) = label;
                }
            }
        }
    }
}

void SuperPixelImage::segResultP2SP()
{
    int nClass;
    cv::Mat * segResult;
    ColorCategoryTable * colorTable;
    string res_dir = myDataset._resultDir;

    if (myDataset._operation == MyDataset::TREESEGEMNTATION)
    {
        nClass = myDataset._parsColorTable.nCategory();
        segResult = &(this->_parsSegMat);
        colorTable = &(myDataset._parsColorTable);
        res_dir += "pars_seg/";
    }
    else // species
    {
        nClass = myDataset._speciesColorTable.nCategory();
        segResult = &(this->_speciesSegMat);
        colorTable = &(myDataset._speciesColorTable);
        res_dir += "species_seg/";
    }
    for (size_t i = 0; i < this->_superPixels.size(); ++i)
    {
        vector<int> spstatistics(nClass, 0);
        for (size_t j = 0; j < this->_superPixels[i]._points.size(); ++j)
        {
            int y = _superPixels[i]._points[j].y;
            int x = _superPixels[i]._points[j].x;

            int label = segResult->at<int>(y,x);

            if (label > 0 && label < nClass)
                spstatistics[label]++;
        }
        int maxId = -1;
        int maxNum = 0;
        for (size_t j = 0; j < nClass; ++j)
        {
            if (spstatistics[j] > maxNum)
            {
                maxNum = spstatistics[j];
                maxId = j;
            }
        }
        if (maxId > 0 && maxId < nClass)
        {
            for (size_t j = 0; j < this->_superPixels[i]._points.size(); ++j)
            {
                int y = _superPixels[i]._points[j].y;
                int x = _superPixels[i]._points[j].x;

                segResult->at<int>(y,x) = maxId;
            }
            _superPixels[i]._label = maxId;
        }
    }

    string name = res_dir + FileAPI::extractFileName(this->_filenameImage, false) + "_sp.png";
    cv::Mat segResultImg = cv::Mat::zeros(_image.rows, _image.cols, CV_8UC3);

    for (int y = 0; y < _image.rows; ++y)
        for (int x = 0; x < _image.cols; ++x)
        {
            if (_activeMask.at<uchar>(y,x) > 0)
            {
                int label = _speciesSegMat.at<int>(y,x);
                if (label >= 0 && label < nClass)
                {

                cv::Vec3b color = myDataset._speciesColorTable.label2color(label);
                segResultImg.at<cv::Vec3b>(y,x) = color;
                }

            }

        }
    cv::imwrite(name, segResultImg);

}
