#ifndef FEATUREEXTRACTOR_H
#define FEATUREEXTRACTOR_H

#include "Feature.h"
#include "PixelImage.h"
#include "ColorCategory.h"

#include <flann/algorithms/KMeansTree.h>

class FeatureExtractor
{
public:
    FeatureExtractor();

    void extractImageLevelTrainFeature(const std::vector<FeatureParameter>& featParas,
                                       const cv::Vec3b& activeColor,
                                       const std::vector<BasicImage>& trainImages,
                                       std::vector<FeatVector> &sampleFeatureSet);

    void adjustSampleStep(const std::vector<FeatureParameter>& featParas);

    void extractPixelLevelTrainFeature(const std::vector<FeatureParameter>& featParas,
                                       const std::vector<BasicImage>& trainImages,
                                       std::vector<FeatVector> & selectSampleFeatureSet,
                                       const cv::Vec3b& activeColor = cv::Vec3b(0,0,0));

    /* sample feature */
    void randomSampleFeatures(const std::vector<FeatVector>& featureSet,
                              const int numSelectFeatures,
                              std::vector<FeatVector> &selectFeatureSet);

    /* Image level feature
     */
    void extractShapeMaskFeature(const PixelImage& pimg, FeatVector& feat);
    void extractShapeMomentFeature(const PixelImage& pimg, FeatVector& feat);
    void extractImageLevelFeature(const std::vector<FeatureParameter>& featParas, const PixelImage& pimg, FeatVector& feat);

    void extractEdgeHistogram(const PixelImage& pimg, FeatVector& feat);

    /* pixel level feature
     */
    void initializePixelFeature(const PixelImage &pimg, std::vector<FeatVector>& features);

    void assignPixelFeatureLabel(const PixelImage &pimg,
                                 const ColorCategoryTable& colorTabel,
                                 std::vector<FeatVector>& features);

    void extractPixelFeature(const std::vector<FeatureParameter> &featParas,
                             const PixelImage &pimg,
                             std::vector<FeatVector> &features);

    void extractPixelFeatureScreened(const std::vector<FeatureParameter> &featParas, const PixelImage &pimg, std::vector<FeatVector> &features);

    void extractTexton(const FeatureParameter &featPara, const PixelImage &pimg, std::vector<FeatVector>& features, int &dim);
    void extractTextonSample(const int sampleStep, const PixelImage& pimg, std::vector<FeatVector>& features);

    void extractHOG(const FeatureParameter& featPara, const PixelImage& pimg, std::vector<FeatVector>& features, int &dim);
    void extractHOGSample(const int smapleStep, const int patchSize, const PixelImage& pimg, std::vector<FeatVector>& features);

    void extractSIFT(const FeatureParameter& featPara, const PixelImage& pimg, std::vector<FeatVector>& features, int &dim);
    void extractSIFTSample(const int sampleStep, const PixelImage& pimg, std::vector<FeatVector> &features);

    void extractSparseSIFT(const FeatureParameter& featPara, const PixelImage& pimg, std::vector<FeatVector>& features, int &dim);

    void extractCOLOR(const FeatureParameter& featPara, const PixelImage& pimg, std::vector<FeatVector>& features, int &dim);

    void analysisPCA(const std::vector<FeatVector>& features, const PixelImage& pimg);

    /* bag of words
     */

    void buildBowTree(const std::vector<FeatureParameter> &featParas,
                      std::vector<BasicImage> &trainImages,
                      const string treename,
                          const string scorename,
                          const string weightname);
    void buildBowIndex(std::vector<DataType>& data,
                       const int num_features,
                       const int feat_dim,
                       const std::string treename);
    void calculateBowWeight(
                            const std::string filename);

    void loadBow(const std::string filename, KMeansTree& kmeans_tree);
    void searchBow(std::vector<FeatureParameter>& featParas,
                   PixelImage& pimg,
                   KMeansTree& kmeans_tree);

};

namespace TextonFeature {

int kernelSize(const float sigma);
void textonfilter(const cv::Mat& img, std::vector<cv::Mat>& responses);
void preprocessImage(const cv::Mat& image, const float scale, std::vector<cv::Mat> &responses);
void resizeInPlace();

}

#endif // FEATUREEXTRACTOR_H
