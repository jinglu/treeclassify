#include "Classifier.h"
#include <assert.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <Common/StatisticAPI.h>
#include <Common/FileAPI.h>
#include "SuperPixelImage.h"

using namespace std;

Classifier::Classifier()
    : _rTrees(NULL),
      _rTreesObj(NULL),
      _extractor(FeatureExtractor())
{
}

Classifier::~Classifier()
{
    if (_rTrees != NULL)
        delete _rTrees;
    if (_rTreesObj != NULL)
        delete _rTreesObj;
}

// main training process
void Classifier::speciesTrain(const vector<BasicImage> &trainImages, const cv::Vec3b& activeColor)
{
    std::string rTFileName = myDataset._modelName;

    // extract training samples
    vector<FeatVector> sampleFeatureSet;
    _extractor.extractPixelLevelTrainFeature(myDataset._featParameters, trainImages, sampleFeatureSet, activeColor);

    std::vector<string> treeTypeNames = myDataset._speciesColorTable.getSortedClassNames();
    int numClass = treeTypeNames.size();
    analizeTrainSampleStas(sampleFeatureSet, treeTypeNames);
    //train classifier
    trainClassifierRF(rTFileName, numClass, sampleFeatureSet);

    // object level
    rTFileName = myDataset._objModelName;
    vector<FeatVector> objSampleFeatureSet;
    _extractor.extractImageLevelTrainFeature(myDataset._objFeatParameters, activeColor, trainImages, objSampleFeatureSet);
    trainClassifierRF(rTFileName, numClass, objSampleFeatureSet);

}

void Classifier::analizeTrainSampleStas(vector<FeatVector> &sampleFeatureSet,
                                        const vector<string>& sortedClassNames)
{
    assert(!sampleFeatureSet.empty());

    int numClass = sortedClassNames.size();

    vector<int> train_sample_num(numClass, 0);
    int featDim = sampleFeatureSet[0]._data.size();

    for (int i = 0; i < sampleFeatureSet.size(); ++i)
    {
        FeatVector &feat = sampleFeatureSet[i];

        assert(feat._label < numClass && feat._label >= 0);
        //cout << "feat data size " << feat._data.size() << ", feat 0 dim " << featDim << endl;
        assert(!feat._data.empty() && feat._data.size() == featDim);

        train_sample_num[feat._label]++;
    }

    cout << "Extract training samples staticstics : dim = " << featDim << std::endl;
    for (int i = 0; i < numClass; ++i)
    {
        cout << i << " " << sortedClassNames[i] << ": " << train_sample_num[i]
             <<", " << (float)train_sample_num[i] / (float)sampleFeatureSet.size() * 100.0 << "%" <<endl;
    }
}

// transfer sampleFeatureSet --> alglib::real_2d_array
void Classifier::extractTrainSamples(vector<FeatVector>& sampleFeatureSet,
                                     const int numClass,
                                     alglib::real_2d_array*& trainData)
{
    assert(trainData == NULL);
    assert(!sampleFeatureSet.empty());

    int featDim = sampleFeatureSet[0]._data.size();

    trainData = new alglib::real_2d_array();
    trainData->setlength(sampleFeatureSet.size(), featDim+1);

    //vector<int> train_sample_num(numClass, 0);

    for (int i = 0; i < sampleFeatureSet.size(); ++i)
    {
        FeatVector &feat = sampleFeatureSet[i];

        assert(feat._label < numClass && feat._label >= 0);
        //cout << "feat data size " << feat._data.size() << ", feat 0 dim " << featDim << endl;
        assert(!feat._data.empty() && feat._data.size() == featDim);

        for (int j = 0; j < featDim; ++j)
        {
            (*trainData)[i][j] = feat._data.at(j);
        }
        (*trainData)[i][featDim] = feat._label;
        //train_sample_num[feat._label]++;
    }

//    cout << "Extract training samples staticstics : dim = " << featDim << std::endl;
//    for (int i = 0; i < numClass; ++i)
//        cout << sortedClassNames[i] << ": " << train_sample_num[i]
//             <<", " << (float)train_sample_num[i] / (float)sampleFeatureSet.size() * 100.0 << "%" <<endl;

    sampleFeatureSet.clear();
    //cout << "trainData rows " << trainData->rows() << endl;
}


void Classifier::trainClassifierRF(const std::string modelName, const int numClass, vector<FeatVector> &sampleFeatureSet)
{
    alglib::real_2d_array* trainData = NULL;
    extractTrainSamples(sampleFeatureSet, numClass, trainData);

    cout<<"\ntraining sample num " << trainData->rows() << ", dim " << trainData->cols()
       << "\n Random forest training ... "<<flush;
//    for (int i = 0; i < trainData->rows(); ++i)
//    {
//        for (int j = 0; j < trainData->cols(); ++j)
//            cout << (*trainData)[i][j] << " ";
//        cout << endl;
//    }

    alglib::decisionforest randomforest;

    alglib::ae_int_t npoints  = trainData->rows();
    alglib::ae_int_t nvars    = trainData->cols() - 1;
    alglib::ae_int_t nclasses = numClass;
    alglib::ae_int_t ntrees   = 100;
    alglib::ae_int_t info;
    alglib::dfreport rep;
    double r = 0.33;

    alglib::dfbuildrandomdecisionforest(*trainData,npoints,
                                        nvars,nclasses,ntrees,r,info,
                                        randomforest,rep);

    cout<<"average error : "<<rep.avgerror<<endl;
    string strRF;
    alglib::dfserialize(randomforest,strRF);
    ofstream out;
    std::cout << "Save RF classifier " << modelName  << " nClass " << randomforest.c_ptr()->nclasses<< std::endl;
    out.open(modelName.c_str());
    out<<strRF<<endl;
    out.close();

    if(trainData != NULL) delete trainData;

}

///////////***************** Test **********************////////////////

// main test process
void Classifier::speciesTest(const vector<BasicImage> &testImages, const cv::Vec3b& activeColor)
{
    testInitialize();
    cout << "end test initialization " << endl;
    int numClass = myDataset._speciesColorTable.nCategory();
    cv::Mat pixelConfusionMatrix = cv::Mat::zeros(numClass, numClass, CV_32SC1);
    cv::Mat objConfusionMatrix = cv::Mat::zeros(numClass, numClass, CV_32SC1);
    vector<float> totalProbs(numClass, 0);
    vector<string> treeNames = myDataset._speciesColorTable.getSortedClassNames();

    vector<float> weights(2,0);
    weights[0] = 0.5;
    weights[1] = 0.5;
    int printw = 16;

    // test images
    for (size_t i = 0; i < testImages.size(); ++i)
    {
        cout << "\n("<<(i+1)<< "/"<<testImages.size()<< ") " << testImages.at(i)._filenameImage
             << "\n" << testImages.at(i)._filenameMask << endl;

        SuperPixelImage testImage(testImages[i]);
        testImage.setAtivePart(activeColor);

        vector<float> objLevelProbs;
        vector<float> pixelLevelProbs(numClass,0);

        testBasicClassifers2prob(testImage, objLevelProbs, objConfusionMatrix);
        //testBasicClassifers(testImage, pixelLevelProbs, pixelConfusionMatrix);
        testPixelBasicClassifers(testImage,
                                 myDataset._featParameters,
                                 myDataset._speciesColorTable,
                                 &pixelLevelProbs,
                                 &pixelConfusionMatrix);


        //print
        cout<<setw(printw)<<left<<" ";
        cout<<setw(printw)<<left << "Obj-level";
        cout << setw(printw)<<left<< "Pixel-level" << endl;
        for (int j = 0; j < numClass; ++j)
        {
            cout << setw(printw) <<left << treeNames[j]
                 << setw(printw) <<left << objLevelProbs[j]
                 << setw(printw) <<left << pixelLevelProbs[j] << endl;
        }
        for (int j = 0; j < numClass; ++j)
            totalProbs[j] = objLevelProbs[j] * weights[0] + pixelLevelProbs[j] * weights[1];

        testImage.setTreeTypeProbs(totalProbs);
        testImage.saveTreeTypeProbFile();
        testImage.speciesValidation(&myDataset._confusionMatrix);

    }

    StatisticAPI::analysisConfusionMat(treeNames, myDataset._confusionMatrix);
}

void Classifier::loadRandomForest(const string modelName, alglib::decisionforest*& forest)
{
    ifstream input;
    input.open(modelName.c_str());
    assert(input.is_open());
    cout << "load random forest " << modelName;
    char temp[1024];
    string strRF = "";
    while(!input.eof())
    {
        input.getline(temp,1024);
        if(temp[0] != '\n' && temp[0] != '\0')
        {
            string textline(temp);
            strRF.append(textline);
        }
        else break;
    }
    input.close();
    if (forest != NULL)
        delete forest;
    forest = new alglib::decisionforest();
    alglib::dfunserialize(strRF, *forest);
    cout << " nClass " << forest->c_ptr()->nclasses << endl;
}


void Classifier::testInitialize()
{
    //_rTFileName = myDataset._modelName;
    loadRandomForest(myDataset._modelName, _rTrees);
    loadRandomForest(myDataset._objModelName, _rTreesObj);

    int nClass = myDataset._speciesColorTable.nCategory();
    myDataset._confusionMatrix = cv::Mat::zeros(nClass, nClass, CV_32SC1);
}




// featvector --> real_1d_array
void Classifier::extractTestSample(const FeatVector &feat, alglib::real_1d_array &sample)
{
    for (size_t i = 0; i < feat._data.size(); ++i)
    {
        sample[i] = feat._data.at(i);
    }
}

void Classifier::estimateSampleProb(alglib::decisionforest *rTrees,
                                    alglib::real_1d_array &sample,
                                    alglib::real_1d_array &prob)
{
    //cout << "rf " << rTrees->c_ptr()->nclasses << endl;
    alglib::dfprocess(*rTrees,sample,prob);
}

/* pixel level */
void Classifier::testPixelBasicClassifers(SuperPixelImage& testImage,
                                          const vector<FeatureParameter>& featParas,
                                          const ColorCategoryTable& colorCategory,
                                          vector<float>* treeTypeProbs,
                                          cv::Mat *confusionMatrix)
{
    int numClass = colorCategory.nCategory();
    colorCategory.print();
    bool isSegmentation = false;
    if (colorCategory._categoryType == ColorCategoryTable::SEGMENTATION)
        isSegmentation = true;

    if (myDataset._isLoadConfidenceMap)
    {
        testImage.loadConfidenceMap(isSegmentation);
    }
    else
    {
        alglib::real_1d_array prob;
        prob.setlength(numClass);

        vector<FeatVector> features;
        _extractor.extractPixelFeature(featParas, testImage, features);
        assert(features.size() == testImage._image.rows * testImage._image.cols);


        vector<cv::Point> pts;
        vector<vector<float> > probs;

        for (size_t i = 0; i < features.size(); ++i)
        {
            FeatVector &feat = features.at(i);
            int featDim = feat.getDim();

            if (featDim <= 0)
                continue;
            int y = i / testImage._image.cols;
            int x = i % testImage._image.cols;

            alglib::real_1d_array testSample;
            testSample.setlength(featDim);
            extractTestSample(feat, testSample);

            alglib::real_1d_array prob;
            prob.setlength(numClass);

            estimateSampleProb(_rTrees, testSample, prob);

            vector<float> probVec(numClass);
            for (int j = 0; j < prob.length(); ++j)
            {
                probVec[j] = prob[j];
            }
            probs.push_back(probVec);
            pts.push_back(cv::Point(x,y));

        }
        //_extractor.analysisPCA(features, testImage);

        testImage.setPixelProbMat(pts, probs, isSegmentation);
        if (myDataset._isSaveConfidenceMap)
            testImage.saveConfidenceMap(isSegmentation);
    }


    testImage.viewPixelProbMap(isSegmentation);
    //testImage.constructEdgeMap();

    // MRF
//    if(myDataset._withPixelLevelfeatures == true)
//        testImage.pixelMRFInference(isSegmentation);
//    else if(myDataset._withSPLevelfeatures == true)
//        testImage.segmentMRFinference();

    if (isSegmentation)
    {
        testImage.saveParsSegResult();
        testImage.parsPixelValidation(confusionMatrix);

        testImage.estimateBranch();
    }
    else // tree species classification
    {
        testImage.saveSpeciesSegResult(treeTypeProbs);
        testImage.segResultP2SP();
        testImage.speciesPixelValidation(confusionMatrix);
    }
}

// image level
void Classifier::testBasicClassifers2prob(PixelImage& testImage, vector<float>& treeTypeProbs, cv::Mat& confusionMatrix)
{

    int numClass = myDataset._speciesColorTable.nCategory();
    treeTypeProbs = vector<float>(numClass,0);

    alglib::real_1d_array prob;
    prob.setlength(numClass);

    FeatVector feat;
    _extractor.extractImageLevelFeature(myDataset._objFeatParameters, testImage, feat);
    int featDim = feat._data.size();

    alglib::real_1d_array testSample;
    testSample.setlength(featDim);
    extractTestSample(feat, testSample);
    estimateSampleProb(_rTreesObj, testSample, prob);

    //vector<float> probVec(numClass);
    for (int i = 0; i < numClass; ++i)
        treeTypeProbs[i] = prob[i];
    //testImage.setTreeTypeProbs(probVec);
    //testImage.saveTreeTypeProbFile();
    //testImage.treeTypeValidation(confusionMatrix);

//    for (int i = 0; i < numClass; ++i)
//    {
//        for (int j = 0; j < numClass; ++j)
//            cout << confusionMatrix.at<int>(i,j) << "\t";
//        cout << endl;
//    }
}

void Classifier::trainBow(vector<BasicImage> &trainImages)
{
//    _extractor.extractBOW(myDataset._featParameters,
//                          myDataset._trainImages,
//                          myDataset._testImages
//                          );
    string treename = myDataset._bowDir + "/temp.kmtree";
    string scorename = myDataset._bowDir + "/temp.score";
    string weightname = myDataset._bowDir + "/temp.weight";
    _extractor.buildBowTree(myDataset._featParameters,
                        myDataset._trainImages,
                        treename,
                        scorename,
                        weightname);
}

void Classifier::testBow(vector<BasicImage> &testImages)
{
    string outname = myDataset._bowDir + "/temp.out";
    ofstream out(outname.c_str());
    string selectoutname = myDataset._bowDir + "temp3.out";
    ofstream selectout(selectoutname.c_str());
    // read tree
    string treename = myDataset._bowDir + "/temp.kmtree";
    string scorename = myDataset._bowDir + "/temp.score";
    string weightname = myDataset._bowDir + "/temp.weight";

    ifstream in_tree(treename.c_str());
    ifstream in_score(scorename.c_str());
    ifstream in_weight(weightname.c_str());

    KMeansTree kmeans_tree(in_tree);
    cout << "Load kmeans tree from " << treename << endl;
    cout << "Node num " << kmeans_tree.GetNodeNum() << endl;
    cout << "Depth " << kmeans_tree.GetDepth() << endl;
    cout << "Leave cluster num " << kmeans_tree.GetLeavesClusterNum() << endl;
    //_extractor.loadBow(bow_filename, kmeans_tree);
    in_tree.close();

    //  read weight
    vector<float> weight(kmeans_tree.GetNodeNum());
    for (size_t i = 0; i < weight.size(); ++i)
    {
        in_weight >> weight[i];
    }
    in_weight.close();

    //  read score
    vector<pair<int, float> > scores;
    vector<pair<string, int> > score_start;
    vector<float> scoresum;

    int num;
    int scoren = 0;
    string name;

    while(!in_score.eof())
    {
        string line;
        getline(in_score, line);
        if (!in_score.good())
            break;
        istringstream iss(line);
        iss >> name >> num;
        scoren += num;

    }
    cout << "score num " << scoren << endl;
    in_score.clear();
    in_score.seekg(0);
    scores.reserve(scoren);
    int nodei, noden;

    while(!in_score.eof())
    {
        string line;
        getline(in_score, line);
        if (!in_score.good())
            break;
        istringstream iss(line);
        iss >> name >> num;
        //cout << name << " " << num << endl;
        //string storename = FileAPI::extractFileName(name, true);
        score_start.push_back(make_pair(name, scores.size()));

        float sum = 0.0;
        while(true)
        {
            iss >> nodei >> noden;
            //cout << "nodei " << nodei << ", noden " << noden << endl;
            if (!iss.good()) break;
            if (weight[nodei] < 1e-6)
                continue;
            float score = weight[nodei] * float(noden);
            scores.push_back(make_pair(nodei, score));
            sum += fabs(score);
            //cout << "node " << nodei << ", score " << score << ", w " << weight[nodei] << ", num " << noden << ", sum " << sum<< endl;
        }
        cout << "name " << name << ", sum " << sum << endl;
        for (int i =  score_start.back().second; i < scores.size(); ++i)
            scores[i].second /= sum;

    }
    score_start.push_back(make_pair(string("END"), scores.size()));
    in_score.close();

    //  query

    for (size_t i = 0; i < testImages.size(); ++i)
    {
        PixelImage testImage(testImages[i]);
        name = testImage._filenameImage;
        cout << i+1 << "/" << testImages.size() << " " << name << endl;
        //_extractor.searchBow(myDataset._featParameters, testImage, kmeans_tree);
        vector<FeatVector> testfeatures;
        _extractor.extractPixelFeatureScreened(myDataset._featParameters, testImage, testfeatures);
        //  find visited nodes
        map<int, int> visited;
        for (size_t j = 0; j < testfeatures.size(); ++j)
        {
            vector<int> visited_nodes;
            vector<DataType>& testdata = testfeatures.at(j)._data;
            int featdim = testdata.size();
            int cluster = kmeans_tree.FindCluster(&(testdata[0]), featdim,32, &(visited_nodes));

            if (cluster >= 0)
            {
                for (size_t k = 0; k < visited_nodes.size(); ++k)
                {
                    if (visited_nodes[k] >= 0)
                    {
                        map<int, int>::iterator mit = visited.find(visited_nodes[k]);
                        if (mit == visited.end())
                            visited[visited_nodes[k]] = 1;
                        else
                            ++mit->second;
                    }
                }
            }
        }
        cout << "visited size " << visited.size() << endl;
        // calculate scores
        float sum = 0.0;
        for (map<int,int>::iterator mit = visited.begin(); mit != visited.end(); ++mit)
        {
            //cout << mit->first << endl;

            sum += fabs(weight[mit->first] * float(mit->second));
            //cout << weight[mit->first] << ", " << mit->second <<endl;
        }

        vector<pair<int, float> > q;
        for (map<int,int>::iterator mit = visited.begin(); mit != visited.end(); ++mit)
        {
            if (weight[mit->first] < 1e-6)
                continue;
            q.push_back(make_pair(mit->first, weight[mit->first] * mit->second / sum));
        }
        cout << name << " sum " << sum << endl;

        //  find score difference
        vector<pair<float, string> > matches;
        matches.reserve(score_start.size());

        for (size_t j = 0; j < score_start.size() - 1; ++j)
        {
            int start = score_start[j].second;
            int end = score_start[j+1].second;
            float s = 0;
            vector<pair<int, float> >::iterator p0 = q.begin();
            int p1 = start;

            while(p1 < end || p0 != q.end())
            {
                if (p1 == end)
                {
                    s += p0->second;
                    ++p0;
                }
                else if (p0 == q.end())
                {
                    s += scores[p1].second;
                    ++p1;
                }
                else if (scores[p1].first > p0->first)
                {
                    s += p0->second;
                    ++p0;
                }
                else if (scores[p1].first < p0->first)
                {
                    s += scores[p1].second;
                    ++p1;
                }
                else
                {
                    s += abs(p0->second - scores[p1].second);
                    ++p0; ++p1;
                }
            }
            matches.push_back(make_pair(s, score_start[j].first));
        }
        sort(matches.begin(), matches.end());

        out << name << endl;
        for (size_t j = 0; j < matches.size(); ++j)
        {
            out << matches[j].second << " " << matches[j].first << " ";
            //cout <<"in match " << matches[j].second << " " << matches[j].first << endl;
        }
        out << endl;

        selectout << "input " << name << endl;
        for (size_t k = 1; k < 4; ++k)
        {
            selectout << "matches " << matches[k].second << " " << matches[k].first << endl;
            cout << "matches " << matches[k].second << " " << matches[k].first << endl;
        }
        selectout << endl;
    }
    out.close();
    selectout.close();
}

void Classifier::parsingTrain(const vector<BasicImage> &trainImages)
{
    vector<FeatVector> sampleFeatureSet;
    _extractor.extractPixelLevelTrainFeature(myDataset._featParameters, trainImages, sampleFeatureSet);

    vector<string> segTypeNames = myDataset._parsColorTable.getSortedClassNames();
    int numClass = segTypeNames.size();

    analizeTrainSampleStas(sampleFeatureSet, segTypeNames);

    string rTFileName = myDataset.getParsModelName();

    trainClassifierRF(rTFileName, numClass, sampleFeatureSet);

}

void Classifier::parsingTest(const vector<BasicImage> &testImages)
{
    if (!myDataset._isLoadConfidenceMap)
        loadRandomForest(myDataset.getParsModelName(), _rTrees);


    int nClass = myDataset._parsColorTable.nCategory();
    vector<string> parsNames = myDataset._parsColorTable.getSortedClassNames();
    myDataset._confusionMatrix = cv::Mat::zeros(nClass, nClass, CV_32SC1);
    cv::Mat pixelConfusionMatrix = cv::Mat::zeros(nClass, nClass, CV_32SC1);

    for (size_t i = 0; i < testImages.size(); ++i)
    {
        cout << "\n("<<(i+1)<< "/"<<testImages.size()<< ") " << testImages.at(i)._filenameImage
             << "\n" << testImages.at(i)._filenameMask << endl;
        SuperPixelImage testImage(testImages[i]);
        vector<float> pixelLevelProbs(nClass);

        testPixelBasicClassifers(testImage,
                                 myDataset._featParameters,
                                 myDataset._parsColorTable,
                                 &pixelLevelProbs,
                                 &pixelConfusionMatrix);
        StatisticAPI::printConfusionMat(parsNames, pixelConfusionMatrix);


        //testImage.refineCrown();
    }
    StatisticAPI::analysisConfusionMat(parsNames,
                                       pixelConfusionMatrix);

}
