#include "GraphModel.h"
#include "GloabalVal.h"
#include <boost/algorithm/string.hpp>

using namespace std;

// Declaration of static variables
int             GraphModel::s_numClass;
int             GraphModel::s_constraintLinkStep;
cv::Mat*        GraphModel::s_constraintTable;
bool            GraphModel::s_withUnLabelRegion;
bool            GraphModel::s_withConstraintLayer;

/*link configuration*/
bool GraphModel::s_withLayerUnary;
bool GraphModel::s_withUpLayerUnary;
bool GraphModel::s_withSuperLayerUnary;

bool GraphModel::s_withLayerPairwise;
bool GraphModel::s_withUpLayerPairwise;
bool GraphModel::s_withSuperLayerPairwise;

bool GraphModel::s_withUpLayer_LayerPairwise;
bool GraphModel::s_withSuperLayer_upLayerPairwise;
bool GraphModel::s_withSuperLayer_LayerPairwise;

bool            GraphModel::s_initialized;
GraphParameters GraphModel::s_weights;

const float LargePenalty = 1e2;

PotentialParameters::PotentialParameters()
    : _paraSizeRow(0),
      _paraSizeCol(0)
{
}

PotentialParameters::PotentialParameters(const int setParaSizeRow, const int setParaSizeCol, const double intialValue, const weightType status)
    : _paraSizeRow(setParaSizeRow),
      _paraSizeCol(setParaSizeCol)

{
    for(unsigned int i=0;i<_paraSizeRow;++i)
    {
        vector<double> parasRow;
        vector<weightType> parasStatusRow;
        for(int j=0;j<_paraSizeCol;j++)
        {
            parasRow.push_back(intialValue);
            parasStatusRow.push_back(status);
        }
        _paras.push_back(parasRow);
        _parasStatus.push_back(parasStatusRow);
    }
}

bool PotentialParameters::operator==(const PotentialParameters& T)
{
    double eps = 1e-5;
    for(unsigned int i=0;i<_paraSizeRow;i++)
    {
        for(int j=0;j<_paraSizeCol;j++)
        {
            if(abs(_paras.at(i).at(j) - T._paras.at(i).at(j))>eps)
            {
                return false;
            }
        }
    }

    return true;
}

void PotentialParameters::setParameter(const int setParaSizeRow,
                                       const int setParaSizeCol,
                                       const double intialValue,
                                       const weightType status)
{
    _paraSizeRow = setParaSizeRow;
    _paraSizeCol = setParaSizeCol;
    _paras.clear();
    _parasStatus.clear();
    for(unsigned int i=0;i< _paraSizeRow; ++i)
    {
        vector<double> parasRow;
        vector<weightType> parasStatusRow;
        for(int j=0;j<_paraSizeCol;++j)
        {
            parasRow.push_back(intialValue);
            parasStatusRow.push_back(status);
        }
        _paras.push_back(parasRow);
        _parasStatus.push_back(parasStatusRow);
    }
}

void PotentialParameters::setStatus(const weightType status)
{
    for(unsigned int i=0;i<_paraSizeRow;i++)
    {
        for(int j=0;j<_paraSizeCol;j++)
        {
            _parasStatus.at(i).at(j) = status;
        }
    }
}

void PotentialParameters::writeWeight(ofstream &out, const string potentialType)
{
    out<<"& "<<potentialType<<endl;
    for(unsigned int i=0;i<_paraSizeRow;i++)
    {
        for(int j=0;j<_paraSizeCol;j++)
        {
            out<<_paras.at(i).at(j)<<" ";
        }
        out<<endl;
    }
}

void PotentialParameters::loadWeight(ifstream &in)
{
    char temp[1024*10];
    if(_paraSizeRow*_paraSizeCol>0)
    {
        for(unsigned int i=0;i<_paraSizeRow;++i)
        {
            in.getline(temp,1024*10);
            string dataLine(temp);

            vector<string> textElement;
            //BasicAPI::TextlineSplit(dataLine,textElement,' ');
            boost::split(textElement, dataLine, boost::is_any_of(" ,\t"), boost::token_compress_on);
            for(int j=0;j< _paraSizeCol; ++j)
            {
                std::istringstream stm1;
                stm1.str(textElement.at(j).c_str());
                stm1 >> _paras.at(i).at(j);
            }
        }
    }
}

void PotentialParameters::printStatus(const string potentialType)
{
    cout<<potentialType<<endl;
    for(unsigned int i=0;i<_paraSizeRow;i++)
    {
        for(int j=0;j<_paraSizeCol;j++)
        {
            if(_parasStatus.at(i).at(j) == staticType)
                cout<<"s ";
            else if(_parasStatus.at(i).at(j) == active)
                cout<<"a ";
            else
                cout<<"n ";
        }
        cout<<endl;
    }
}

void PotentialParameters::printWeight()
{
    for(unsigned int i=0;i<_paraSizeRow;i++)
    {
        for(int j=0;j<_paraSizeCol;j++)
        {
            cout<<_paras.at(i).at(j)<<" ";
        }
        cout<<endl;
    }
}

void PotentialParameters::print(const string potentialType)
{
    printStatus(potentialType);
    printWeight();
}


void PotentialParameters::getParameters(vector<double>& weight,vector<weightType>& weightStatus) const
{
    for(int i=0;i<_paraSizeRow;++i)
    {
        for(int j=0;j<_paraSizeCol;++j)
        {
            weight.push_back(_paras.at(i).at(j));
            weightStatus.push_back(_parasStatus.at(i).at(j));
        }
    }
}

void PotentialParameters::PotentialParameters::setParameters(const vector<double>& weight,const vector<weightType>& weightStatus,int& startIndex)
{
    for(int i=0;i<_paraSizeRow;i++)
    {
        for(int j=0;j<_paraSizeCol;j++)
        {
            _paras.at(i).at(j) = weight.at(startIndex + i*_paraSizeCol+j);
            _parasStatus.at(i).at(j) = weightStatus.at(startIndex + i*_paraSizeCol+j);
        }
    }

    startIndex += _paraSizeRow * _paraSizeCol;
}

int PotentialParameters::getSize() const
{
    return _paraSizeRow * _paraSizeCol;
}

/////////*************************** GraphParameter ************************************///////////////////////


GraphParameters::GraphParameters()
{

}

bool GraphParameters::operator==(const GraphParameters& T)
{
    if(!(_layerNodesWeights == T._layerNodesWeights)) return false;
    if(!(_upLayerNodesWeights == T._upLayerNodesWeights)) return false;
    if(!(_superLayerNodesWeights == T._superLayerNodesWeights)) return false;

    if(!(_layerLinksWeights == T._layerLinksWeights)) return false;
    if(!(_upLayerLinksWeights == T._upLayerLinksWeights)) return false;
    if(!(_superLayerLinksWeights == T._superLayerLinksWeights)) return false;

    if(!(_upLayer_LayerLinksWeights == T._upLayer_LayerLinksWeights)) return false;
    if(!(_superLayer_LayerLinksWeights == T._superLayer_LayerLinksWeights)) return false;
    if(!(_superLayer_upLayerLinksWeights == T._superLayer_upLayerLinksWeights)) return false;

    if(!(_constraintWeights == T._constraintWeights)) return false;
    return true;
}

void GraphParameters::setDefaultParameters()
{
    int numClass;
    if (myDataset._operation == MyDataset::TREESEGEMNTATION)
        numClass = myDataset._parsColorTable.nCategory();
    else if (myDataset._operation == MyDataset::TREESPECIESCLASSIFICATION)
        numClass = myDataset._speciesColorTable.nCategory();
    /*unary potential weights */
    if(myDataset._layerUnary != nonactive)
        _layerNodesWeights.setParameter(1,numClass,1.0,myDataset._layerUnary);
    if(myDataset._uplayerUnary != nonactive)
        _upLayerNodesWeights.setParameter(1,numClass,1.0,myDataset._uplayerUnary);
    if(myDataset._superlayerUnary != nonactive)
        _superLayerNodesWeights.setParameter(1,numClass,1.0,myDataset._superlayerUnary);
    if(myDataset._constraintlayer != nonactive)
        _constraintWeights.setParameter(1,1,0.0,myDataset._constraintlayer);

    /*set default link cost*/
    if(myDataset._layerPairwise != nonactive)
        _layerLinksWeights.setParameter(1,numClass,0.1,myDataset._layerPairwise);
    if(myDataset._uplayerPairwise != nonactive)
        _upLayerLinksWeights.setParameter(1,numClass,0.001,myDataset._uplayerPairwise);
    if(myDataset._superlayerPairwise != nonactive)
        _superLayerLinksWeights.setParameter(1,numClass,0.001,myDataset._superlayerPairwise);

    /*set default link cost*/
    if(myDataset._up_layer_Pairwise != nonactive)
        _upLayer_LayerLinksWeights.setParameter(1,numClass,0.0,myDataset._up_layer_Pairwise);
    if(myDataset._super_layer_Pairwise != nonactive)
        _superLayer_LayerLinksWeights.setParameter(1,numClass,0.0,myDataset._super_layer_Pairwise);
    if(myDataset._super_up_layerPairwise != nonactive)
        _superLayer_upLayerLinksWeights.setParameter(1,numClass,0.0,myDataset._super_up_layerPairwise);
}


void GraphParameters::getParameters(vector<double>& weight,vector<weightType>& weightStatus) const
{
    weight.clear();
    weightStatus.clear();
    _layerNodesWeights.getParameters(weight,weightStatus);        // class specific weights
    _upLayerNodesWeights.getParameters(weight,weightStatus);      // class specific weights
    _superLayerNodesWeights.getParameters(weight,weightStatus);   // class specific weights

    _layerLinksWeights.getParameters(weight,weightStatus);
    _upLayerLinksWeights.getParameters(weight,weightStatus);
    _superLayerLinksWeights.getParameters(weight,weightStatus);

    _upLayer_LayerLinksWeights.getParameters(weight,weightStatus);
    _superLayer_LayerLinksWeights.getParameters(weight,weightStatus);
    _superLayer_upLayerLinksWeights.getParameters(weight,weightStatus);

    _constraintWeights.getParameters(weight,weightStatus);
}

void GraphParameters::setParameters(const vector<double>& weight, const vector<weightType>& weightStatus)
{
    int startIndex = 0;
    _layerNodesWeights.setParameters(weight,weightStatus,startIndex);        // class specific weights
    _upLayerNodesWeights.setParameters(weight,weightStatus,startIndex);      // class specific weights
    _superLayerNodesWeights.setParameters(weight,weightStatus,startIndex);   // class specific weights

    _layerLinksWeights.setParameters(weight,weightStatus,startIndex);
    _upLayerLinksWeights.setParameters(weight,weightStatus,startIndex);
    _superLayerLinksWeights.setParameters(weight,weightStatus,startIndex);

    _upLayer_LayerLinksWeights.setParameters(weight,weightStatus,startIndex);
    _superLayer_LayerLinksWeights.setParameters(weight,weightStatus,startIndex);
    _superLayer_upLayerLinksWeights.setParameters(weight,weightStatus,startIndex);

    _constraintWeights.setParameters(weight,weightStatus,startIndex);
}

void GraphParameters::print()
{
    if(_layerNodesWeights.getSize()>0) _layerNodesWeights.print("layer node weights");
    if(_upLayerNodesWeights.getSize()>0) _upLayerNodesWeights.print("uplayer node weights");
    if(_superLayerNodesWeights.getSize()>0) _superLayerNodesWeights.print("superlayer node weights");

    if(_constraintWeights.getSize()>0) _constraintWeights.print("constraint layer node weights");
    if(_layerLinksWeights.getSize()>0) _layerLinksWeights.print("layer link weights");
    if(_upLayerLinksWeights.getSize()>0) _upLayerLinksWeights.print("uplayer link weights");
    if(_superLayerLinksWeights.getSize()>0) _superLayerLinksWeights.print("superlayer link weights");

    if(_upLayer_LayerLinksWeights.getSize()>0) _upLayer_LayerLinksWeights.print("upLayer_Layer link weights");
    if(_superLayer_LayerLinksWeights.getSize()>0) _superLayer_LayerLinksWeights.print("superLayer_Layer link weights");
    if(_superLayer_upLayerLinksWeights.getSize()>0) _superLayer_upLayerLinksWeights.print("superLayer_upLayer link weights");
}

void GraphParameters::writeToFile(ofstream& out)
{
    if(_layerNodesWeights.getSize()>0) _layerNodesWeights.writeWeight(out,"layer node weights");
    if(_upLayerNodesWeights.getSize()>0) _upLayerNodesWeights.writeWeight(out,"uplayer node weights");
    if(_superLayerNodesWeights.getSize()>0) _superLayerNodesWeights.writeWeight(out,"superlayer node weights");
    if(_constraintWeights.getSize()>0) _constraintWeights.writeWeight(out,"constraint layer node weights");

    if(_layerLinksWeights.getSize()>0) _layerLinksWeights.writeWeight(out,"layer link weights");
    if(_upLayerLinksWeights.getSize()>0) _upLayerLinksWeights.writeWeight(out,"uplayer link weights");
    if(_superLayerLinksWeights.getSize()>0) _superLayerLinksWeights.writeWeight(out,"superlayer link weights");

    if(_upLayer_LayerLinksWeights.getSize()>0) _upLayer_LayerLinksWeights.writeWeight(out,"upLayer_Layer link weights");
    if(_superLayer_LayerLinksWeights.getSize()>0) _superLayer_LayerLinksWeights.writeWeight(out,"superLayer_Layer link weights");
    if(_superLayer_upLayerLinksWeights.getSize()>0) _superLayer_upLayerLinksWeights.writeWeight(out,"superLayer_upLayer link weights");
}

void GraphParameters::loadFromFile(ifstream& in)
{
    char temp[1024*10];
    while(!in.eof())
    {
        in.getline(temp,1024*10);
        string dataLine(temp);
        if(dataLine == "& layer node weights")
        {
            _layerNodesWeights.loadWeight(in);
        }
        else if(dataLine == "& uplayer node weights")
        {
            _upLayerNodesWeights.loadWeight(in);
        }
        else if(dataLine == "& superlayer node weights")
        {
            _superLayerNodesWeights.loadWeight(in);
        }
        else if(dataLine == "& constraint layer node weights")
        {
            _constraintWeights.loadWeight(in);
        }
        else if(dataLine == "& layer link weights")
        {
            _layerLinksWeights.loadWeight(in);
        }
        else if(dataLine == "& uplayer link weights")
        {
            _upLayerLinksWeights.loadWeight(in);
        }
        else if(dataLine == "& superlayer link weights")
        {
            _superLayerLinksWeights.loadWeight(in);
        }
        else if(dataLine == "& upLayer_Layer link weights")
        {
            _upLayer_LayerLinksWeights.loadWeight(in);
        }
        else if(dataLine == "& superLayer_Layer link weights")
        {
            _superLayer_LayerLinksWeights.loadWeight(in);
        }
        else if(dataLine == "& superLayer_upLayer link weights")
        {
            _superLayer_upLayerLinksWeights.loadWeight(in);
        }
    }
}

//////////////*********************** GraphModel **********************************************//////////////////

GraphModel::GraphModel()
    : _width(0),
      _height(0),
      _layerSize(0),
      _upLayerSize(0),
      _superLayerSize(0)
{
}

GraphModel::GraphModel(const int nClass)
    :
      _width(0),
      _height(0),
      _layerSize(0),
      _upLayerSize(0),
      _superLayerSize(0)
{
    s_numClass = nClass;
}

GraphModel::~GraphModel()
{
    // TODO Auto-generated destructor stub
    /*layer*/
    release();
}

void GraphModel::setupGraph(const BasicImage& image)
{
    _inputImage._filenameImage      = image._filenameImage;             // image file name
    _inputImage._filenameMask  = image._filenameMask;         // annotation file name
    _inputImage._filenameSuperPixel = image._filenameSuperPixel;
}


/*setup graph from image*/
void GraphModel::setupGraph(const PixelImage* image,cv::Mat* layerProb,cv::Mat* layerProbIndex)
{
    _height = image->_image.rows;
    _width  = image->_image.cols;

    _inputImage._filenameImage      = image->_filenameImage;             // image file name
    _inputImage._filenameMask  = image->_filenameMask;         // annotation file name
    _inputImage._filenameSuperPixel = image->_filenameSuperPixel;

    /*To reduce memory cost, the graph structure will be constructed when inference performed*/
    setupLayer(image,layerProb,layerProbIndex);

    int numLayerNodes = 0;
    int numUpLayerNodes = 0;
    int numSuperLayerNodes = 0;

    /*re-assign index for nodes*/
    for(unsigned int i=0;i<_layer.size();i++)
    {
        if(_layer.at(i)->_isActive == true)
            numLayerNodes++;
    }

    for(unsigned int i=0;i<_upLayer.size();i++)
    {
        if(_upLayer.at(i)->_isActive == true)
            numUpLayerNodes++;
    }

    for(unsigned int i=0;i<_superLayer.size();i++)
    {
        if(_superLayer.at(i)->_isActive == true)
            numSuperLayerNodes++;
    }

    cout << "layer size " << _layer.size() << endl;
    cout<<endl<<"graph size - ";
    cout<<"layer: "<<numLayerNodes<<" / ";
    cout<<"upLayer: "<<numUpLayerNodes<<" / ";
    cout<<"superLayer: "<<numSuperLayerNodes<<" ";
    cout<<endl;
}


/* three types of models are supported: pixel + superpixel + segment */
void GraphModel::setupGraph(const SuperPixelImage* image,
                            cv::Mat* layerProb,cv::Mat* layerProbIndex,
                            cv::Mat* upLayerProb,cv::Mat* upLayerProbIndex,
                            cv::Mat* superLayerProb,cv::Mat* superLayerProbIndex)
{
    _height = image->_image.rows;
    _width  = image->_image.cols;

    if(s_withLayerUnary == true || s_withLayerPairwise == true)
        setupLayer(image,layerProb,layerProbIndex);
    if(s_withUpLayerUnary == true || s_withUpLayerPairwise == true)
        setupUpLayer(image,upLayerProb,upLayerProbIndex);

    /* cluster */
    if(s_withSuperLayerUnary == true || s_withSuperLayerPairwise == true)
    {
        setupSuperLayer(image,superLayerProb,superLayerProbIndex);
    }

    int numLayerNodes = 0;
    int numUpLayerNodes = 0;
    int numSuperLayerNodes = 0;

    /*re-assign index for nodes*/
    for(unsigned int i=0;i<_layer.size();i++)
    {
        if(_layer.at(i)->_isActive == true)
            numLayerNodes++;
    }

    for(unsigned int i=0;i<_upLayer.size();i++)
    {
        if(_upLayer.at(i)->_isActive == true)
            numUpLayerNodes++;
    }

    for(unsigned int i=0;i<_superLayer.size();i++)
    {
        if(_superLayer.at(i)->_isActive == true)
            numSuperLayerNodes++;
    }

    cout<<endl<<"graph size - ";
    cout<<"Layer: "<<numLayerNodes<<" / ";
    cout<<"upLayer: "<<numUpLayerNodes<<" / ";
    cout<<"superLayer: "<<numSuperLayerNodes<<" ";
    cout<<endl;
}


void GraphModel::setupLayer(const PixelImage* image,cv::Mat* layerProb,cv::Mat* layerProbIndex)
{
    _height = image->_image.rows;
    _width  = image->_image.cols;
    int numClass = myDataset._parsColorTable.nCategory();
    if (myDataset._operation == MyDataset::TREESPECIESCLASSIFICATION)
        numClass = myDataset._speciesColorTable.nCategory();

    for(unsigned int i=0;i<_height;i++)
    {
        for(int j=0;j<_width;j++)
        {
            GraphNode* node = new GraphNode();
            node->_index = i*_width + j;
            node->_x = j;
            node->_y = i;

            /*exclude unlabled region*/
            if(!image->_parsLabelMat.empty())
            {
                node->_trueLabel = image->_parsLabelMat.at<int>(i,j);
                if(image->_parsLabelMat.at<int>(i,j) < 0)
                {
                    node->_withLabel = false;
                    if(s_withUnLabelRegion == false)
                        node->_isActive = false;
                }
                else node->_withLabel = true;
            }

            _layer.push_back(node);
            for(int k=0;k<numClass;k++)
            {
                _layer.at(node->_index)->_featVector.push_back(0);
            }
        }
    }

    if(layerProb != NULL && layerProbIndex != NULL)
    {
        for(unsigned int i=0;i<layerProbIndex->rows;i++)
        {
            int x = layerProbIndex->at<float>(i,0);
            int y = layerProbIndex->at<float>(i,1);
            int idx = y*_width + x;

            for(int j=0;j<layerProb->cols;j++)
            {
                _layer.at(idx)->_featVector.at(j) = 1 - layerProb->at<float>(i,j); // predicted confidence for each pixels
                //Layer.at(idx)->featVector.at(j) = -log(min(float(0.999),max(layerProb->at<float>(i,j),float(0.001))));
            }
        }
    }

    if(s_withLayerPairwise == true)
    {
        /*set adjacent list of pixel node in the graph*/
        for(unsigned int i=0;i<_layer.size();i++)
        {
            int x = _layer.at(i)->_x;
            int y = _layer.at(i)->_y;

            if(x+1<_width)  _layer.at(i)->insertLayerlinks(_layer.at(y*_width + x+1));
            if(y+1<_height) _layer.at(i)->insertLayerlinks(_layer.at((y+1)*_width + x));
            if(x-1>=0)     _layer.at(i)->insertLayerlinks(_layer.at(y*_width + x-1));
            if(y-1>=0)     _layer.at(i)->insertLayerlinks(_layer.at((y-1)*_width + x));
        }

        float average_color_differnece = 16;
        /*color different between neighbors*/
        for(unsigned int i=0;i<_layer.size();i++)
        {
            int x = _layer.at(i)->_x;
            int y = _layer.at(i)->_y;
            //CvScalar s = cvGet2D(image->ptImage,y,x);
            cv::Vec3b s = image->_image.at<cv::Vec3b>(y,x);

            for(int j=0;j<_layer.at(i)->_layerlinks.size();j++)
            {
                int adjx = _layer.at(i)->_layerlinks.at(j)._linkedNode->_x;
                int adjy = _layer.at(i)->_layerlinks.at(j)._linkedNode->_y;
                //CvScalar adjs = cvGet2D(image->ptImage,adjy,adjx);
                cv::Vec3b adjs = image->_image.at<cv::Vec3b>(adjy, adjx);

                double colorDifference = 0;
                for (int k = 0; k < image->_image.channels(); ++k)
                {
                    if (colorDifference < abs(s[k] - adjs[k]))
                        colorDifference = abs(s[k] - adjs[k]);
                }
//				for(int k=0;k<image->ptImage->nChannels;k++)
//				{
//					if(colorDifference < abs(s.val[k]-adjs.val[k]))
//					{
//						colorDifference = abs(s.val[k]-adjs.val[k]);
//					}
//				}
                colorDifference /= average_color_differnece;
                _layer.at(i)->_layerlinks.at(j)._w = 1/(colorDifference+1.0);
            }
        }
    }
}


void GraphModel::setupLayer(const SuperPixelImage* image,cv::Mat* layerProb,cv::Mat* layerProbIndex)
{
    setupLayer((PixelImage*)(image),layerProb,layerProbIndex);
}

void GraphModel::setupUpLayer(const SuperPixelImage* image,
                              cv::Mat* upLayerProb,cv::Mat* upLayerProbIndex)
{
    int numClass = myDataset._parsColorTable.nCategory();
    if (myDataset._operation == MyDataset::TREESPECIESCLASSIFICATION)
        numClass = myDataset._speciesColorTable.nCategory();

    /*set superpixel node in the graph*/
    for(unsigned int i=0;i<image->_superPixels.size();i++)
    {
        GraphNode* node = new GraphNode();
        node->_index = _layer.size()+i;

        /*exclude unlabled region*/
        node->_trueLabel = image->_superPixels.at(i)._label;
        if(node->_trueLabel <= 0)
        {
            node->_withLabel = false;
            if(s_withUnLabelRegion == false) node->_isActive = false;
        }
        else node->_withLabel = true;

        _upLayer.push_back(node);
        for(int k=0;k<numClass;k++)
        {
            _upLayer.at(i)->_featVector.push_back(0);
        }
    }

    if(upLayerProb != NULL && upLayerProbIndex != NULL)
    {
        for(unsigned int i=0;i<upLayerProb->rows;i++)
        {
            int idx = upLayerProbIndex->at<int>(i,0) - 1;
            for(int j=0;j<numClass;j++)
            {
                _upLayer.at(idx)->_featVector.at(j) = 1 - upLayerProb->at<float>(i,j);
                //upLayer.at(i)->featVector.at(j) = -log(min(float(0.999),max(upLayerProb->at<float>(i,j),float(0.001))));
            }
        }
    }

    /*set superpixel node in the graph*/
    for(unsigned int i=0;i<image->_superPixels.size();i++)
    {
        for(int j=0;j<image->_superPixels.at(i)._points.size();j++)
        {
            int x = image->_superPixels.at(i)._points.at(j).x;
            int y = image->_superPixels.at(i)._points.at(j).y;

            if(s_withUpLayer_LayerPairwise == true)
                _upLayer.at(i)->insertLayerlinks(_layer.at(y*_width+x),1.0);
        }
        _upLayer.at(i)->_x = image->_superPixels.at(i)._center.x;
        _upLayer.at(i)->_y = image->_superPixels.at(i)._center.y;
    }

    if(s_withUpLayerPairwise == true)
    {
        /*set adjacent list of superpixel node in the graph*/
        for(unsigned int i=0;i<image->_superPixels.size();i++)
        {
            for(int j=0;j<image->_superPixels.at(i)._adjSpIds.size();j++)
            {
                int adjIndex = image->_superPixels.at(i)._adjSpIds.at(j)-1;
                double w     = image->_superPixels.at(i)._linkWeight.at(j);
                _upLayer.at(i)->insertUpLayerlinks(_upLayer.at(adjIndex),w);
            }
        }

//		float average_color_differnece = 16;
//		/*color different between neighbors*/
//		for(unsigned int i=0;i<upLayer.size();i++)
//		{
//			CvScalar averageColor;
//			for(int j=0;j<upLayer.at(i)->layerlinks.size();j++)
//			{
//				int x = upLayer.at(i)->layerlinks.at(j).linkedNode->x;
//				int y = upLayer.at(i)->layerlinks.at(j).linkedNode->y;
//				CvScalar s = cvGet2D(image->ptImage,y,x);
//
//				for(int k=0;k<image->ptImage->nChannels;k++)
//				{
//					averageColor.val[k] += s.val[k];
//				}
//			}
//
//			for(int j=0;j<image->ptImage->nChannels;j++)
//			{
//				averageColor.val[j] /= upLayer.at(i)->layerlinks.size();
//			}
//
//			for(int j=0;j<upLayer.at(i)->upLayerlinks.size();j++)
//			{
//				CvScalar adjAverageColor;
//				for(int k=0;k<upLayer.at(i)->upLayerlinks.at(j).linkedNode->layerlinks.size();k++)
//				{
//					int x = upLayer.at(i)->upLayerlinks.at(j).linkedNode->layerlinks.at(k).linkedNode->x;
//					int y = upLayer.at(i)->upLayerlinks.at(j).linkedNode->layerlinks.at(k).linkedNode->y;
//					CvScalar s = cvGet2D(image->ptImage,y,x);
//
//					for(int n=0;n<image->ptImage->nChannels;n++)
//					{
//						adjAverageColor.val[n] += s.val[n];
//					}
//				}
//
//				for(int k=0;k<image->ptImage->nChannels;k++)
//				{
//					adjAverageColor.val[k] /= upLayer.at(i)->upLayerlinks.at(j).linkedNode->layerlinks.size();
//				}
//
//				double colorDifference = 0;
//				for(int k=0;k<image->ptImage->nChannels;k++)
//				{
//					if(colorDifference < abs(averageColor.val[k]-adjAverageColor.val[k]))
//					{
//						colorDifference = abs(averageColor.val[k]-adjAverageColor.val[k]);
//					}
//				}
//				colorDifference /= average_color_differnece;
//				upLayer.at(i)->upLayerlinks.at(j).w = 1/(colorDifference+1.0);
//			}
//		}
    }
}

void GraphModel::setupSuperLayer(const SuperPixelImage* image,
                                 cv::Mat* superLayerProb,cv::Mat* superLayerProbIndex)
{

}



/* Through introduce some extra nodes,
 * we can insert some prior constraint into the graph model,
 * currently, the considered constraints include categories pairwise prior and
 * categories relative position prior,for example, the sky region should always
 * appears above all other categories, and the building should never appears under
 * the grass. To introduce this pairwise categories constraints, we use a N * N
 * constraint table to renote these constraints, and each element in the table will
 * be represented by a new discrete label in the labeling optimization process, if the
 * categories combination should be appears, then the penalty for the corresponding
 * label will be very large.*/
/* The strength of constraint will be decided by the number of introduced constrait
 * nodes and links. In theory, if there exists a constraint node for each node pair, then,
 * the constraint will be never voliated
 * */
void GraphModel::setupConstraintLayer()
{
      int linkCount = 0;

      assert(s_constraintLinkStep > 0 &&
             s_constraintLinkStep < _width &&
             s_constraintLinkStep < _height);

      int step = s_constraintLinkStep; /*should > 1*/
      if(s_withLayerUnary == true && _layer.size()>0)
      {
          for(unsigned int i=0;i<_layer.size();i++)
          {
              _layer.at(i)->_constraintLayerlinks.clear();
              int x = _layer.at(i)->_x;
              int y = _layer.at(i)->_y;

              //if((x+step/2)%step == 0 && (y+step/2)%step == 0)
              {
                  if(y>=step)
                  {
                      int linkIndex = (y - step)*_width + x;
                      _layer.at(i)->insertConstraintLayerlinks(_layer.at(linkIndex),1.0);
                      linkCount++;
                  }

                  if(x>=step)
                  {
                      int linkIndex = y*_width + x - step;
                      _layer.at(i)->insertConstraintLayerlinks(_layer.at(linkIndex),1.0);
                      linkCount++;
                  }

                  if(y+step<_height)
                  {
                      int linkIndex = (y + step)*_width + x;
                      _layer.at(i)->insertConstraintLayerlinks(_layer.at(linkIndex),1.0);
                      linkCount++;
                  }

                  if(x+step<_width)
                  {
                      int linkIndex = y*_width + x + step;
                      _layer.at(i)->insertConstraintLayerlinks(_layer.at(linkIndex),1.0);
                      linkCount++;
                  }
              }
          }
      }

      cout<<"number of constraint links: "<<linkCount<<endl;
}


void GraphModel::assignUnaryCost(MRF::CostVal* dCost,
                                 int numLabels,
                                 vector<int>& activeNodeIndexs,
                                 vector<GraphNode*>& inputLayer,
                                 GraphParameters::NodeType nodeType,
                                 int *initialLabeling)
{
    PotentialParameters* w = NULL;

    if(nodeType == GraphParameters::TypeLayerNode)           w = &s_weights._layerNodesWeights;
    else if(nodeType == GraphParameters::TypeUpLayerNode)    w = &s_weights._upLayerNodesWeights;
    else if(nodeType == GraphParameters::TypeSuperLayerNode) w = &s_weights._superLayerNodesWeights;

    /*set layer unary*/
    for(unsigned int i=0;i<activeNodeIndexs.size();i++)
    {
        int idx = activeNodeIndexs.at(i);
        inputLayer.at(idx)->_activeIndex = i;
        for(int j=0;j<s_numClass;j++)
        {
//    		cout<<inputLayer.at(idx)->featVector.size()<<endl;
//    		cout<<w->paras.size()<<endl;
//    		cout<<w->paras.at(0).size()<<endl;
            double cost = inputLayer.at(idx)->_featVector.at(j) * w->_paras.at(0).at(j);
            dCost[i*numLabels+j] = cost;
        }

        for(int j = s_numClass;j<numLabels;j++)
            dCost[i*numLabels+j] = LargePenalty;

        if(inputLayer.at(idx)->_trueLabel > 0 && initialLabeling != NULL)
        {
            //initialLabeling[i] = inputLayer.at(idx)->_trueLabel-1;
            initialLabeling[i] = inputLayer.at(idx)->_trueLabel;
        }
    }
}

void GraphModel::assignPairwiseCost(MRF* mrf,
                                    std::vector<int>& activeNodeIndexs,
                                    std::vector<GraphNode*>& inputLayer,
                                    GraphParameters::NodeType nodeType)
{
    /*set layer pairwise*/
    MRF::CostVal link_weight = 1;
    for(unsigned int i=0;i<activeNodeIndexs.size();i++)
    {
        int idx = activeNodeIndexs.at(i);
        assert(i == inputLayer.at(idx)->_activeIndex);

        vector< GraphNodeLink >* links = NULL;
        if(nodeType == GraphParameters::TypeLayerNode) links = &inputLayer.at(idx)->_layerlinks;
        else if(nodeType == GraphParameters::TypeUpLayerNode) links = &inputLayer.at(idx)->_upLayerlinks;
        else if(nodeType == GraphParameters::TypeSuperLayerNode) links = &inputLayer.at(idx)->_superLayerlinks;

        for(int j=0;j<links->size();j++)
        {
            int adjIdx  = links->at(j)._linkedNode->_activeIndex;
            link_weight = links->at(j)._w;

            if(inputLayer.at(idx)->_activeIndex > adjIdx && adjIdx >= 0)
            {
                if(link_weight>0)
                {
                    mrf->setNeighbors(inputLayer.at(idx)->_activeIndex,adjIdx,link_weight);
                    //cout << "set neighbor " << inputLayer.at(idx)->_activeIndex << ", " << adjIdx <<endl;
                }
            }
        }

        if(s_withConstraintLayer == true)
        {
            /*constraint link*/
            for(int j=0;j<inputLayer.at(idx)->_constraintLayerlinks.size();j++)
            {
                int adjIdx  = inputLayer.at(idx)->_constraintLayerlinks.at(j)._linkedNode->_activeIndex;
                link_weight = inputLayer.at(idx)->_constraintLayerlinks.at(j)._w;

                if(inputLayer.at(idx)->_activeIndex > adjIdx && adjIdx >= 0)
                {
                    if(link_weight>0) mrf->setNeighbors(inputLayer.at(idx)->_activeIndex,adjIdx,link_weight);
                }
            }
        }
    }
}

void GraphModel::assignAcrossLayerPairwiseCost(MRF* mrf,
                                               vector<int>& activeNodeIndexs,
                                               vector<GraphNode*>& inputLayer,
                                               GraphParameters::NodeType linkType)
{
    /*set layer pairwise*/
    MRF::CostVal link_weight = 1;
    for(unsigned int i=0;i<activeNodeIndexs.size();i++)
    {
        int idx = activeNodeIndexs.at(i);
        assert(i == inputLayer.at(idx)->_activeIndex);

        vector< GraphNodeLink >* links = NULL;
        if(linkType == GraphParameters::TypeLayerNode) links = &inputLayer.at(idx)->_layerlinks;
        else if(linkType == GraphParameters::TypeUpLayerNode) links = &inputLayer.at(idx)->_upLayerlinks;
        else if(linkType == GraphParameters::TypeSuperLayerNode) links = &inputLayer.at(idx)->_superLayerlinks;

        for(int j=0;j<links->size();j++)
        {
            int adjIdx  = links->at(j)._linkedNode->_activeIndex;
            link_weight = links->at(j)._w;

            if(inputLayer.at(idx)->_activeIndex > adjIdx && adjIdx >= 0)
            {
                if(link_weight>0) mrf->setNeighbors(inputLayer.at(idx)->_activeIndex,adjIdx,link_weight);
            }
        }
    }
}


void GraphModel::mapInference(bool withInitializaiton)
{
    int numLabels = s_numClass;

    if(s_withConstraintLayer == true) setupConstraintLayer();

    /*store the index of actived nodes*/
    vector<int> activeLayerNodes;
    vector<int> activeUpLayerNodes;
    vector<int> activeSuperLayerNodes;
    vector<int> activeConstraintLayerNodes;

    /*re-assign index for nodes*/
    for(unsigned int i=0;i<_layer.size();i++)
    {
        if(_layer.at(i)->_isActive == true)
            activeLayerNodes.push_back(i);
    }

    for(unsigned int i=0;i<_upLayer.size();i++)
    {
        if(_upLayer.at(i)->_isActive == true)
            activeUpLayerNodes.push_back(i);
    }

    for(unsigned int i=0;i<_superLayer.size();i++)
    {
        if(_superLayer.at(i)->_isActive == true)
            activeSuperLayerNodes.push_back(i);
    }

    int numLayerNodes = activeLayerNodes.size();
    int numUpLayerNodes = activeUpLayerNodes.size();
    int numSuperLayerNodes = activeSuperLayerNodes.size();
    int numConstraintLayerNodes = activeConstraintLayerNodes.size();

    _layerSize      = numLayerNodes;
    _upLayerSize    = numLayerNodes + numUpLayerNodes;
    _superLayerSize = numLayerNodes + numUpLayerNodes + numSuperLayerNodes;

    int numNodes = numLayerNodes +
                   numUpLayerNodes +
                   numSuperLayerNodes +
                   numConstraintLayerNodes;

    MRF::CostVal* dCost = new MRF::CostVal[numLabels*numNodes];

    /*setting the initial label for optimization*/
    int *initialLabeling = NULL;
    if(withInitializaiton == true)
    {
        initialLabeling = new int[numNodes];
        for(unsigned int i=0;i<numNodes;i++)
            initialLabeling[i] = 0;
    }

    /*set layer unary*/
    assignUnaryCost(dCost,numLabels,
                    activeLayerNodes,
                    _layer,
                    GraphParameters::TypeLayerNode,
                    initialLabeling);

    /*set uplayer unary*/
    assignUnaryCost(dCost,
                    numLabels,
                    activeUpLayerNodes,
                    _upLayer,
                    GraphParameters::TypeUpLayerNode,
                    initialLabeling);

    /*set superlayer unary*/
    assignUnaryCost(dCost,
                    numLabels,
                    activeSuperLayerNodes,
                    _superLayer,
                    GraphParameters::TypeSuperLayerNode,
                    initialLabeling);

    DataCost *data = new DataCost(dCost);
    SmoothnessCost *smooth = new SmoothnessCost(GraphModel::SmoothTermCost,this);
    EnergyFunction *energy = new EnergyFunction(data,smooth);
    MRF* mrf = new Expansion(numNodes,numLabels,energy);

    /*set layer pairwise*/
    assignPairwiseCost(mrf,activeLayerNodes,_layer,GraphParameters::TypeLayerNode);

    /*set uplayer pairwise*/
    assignPairwiseCost(mrf,activeUpLayerNodes,_upLayer,GraphParameters::TypeUpLayerNode);

    /*set superlayer pairwise*/
    assignPairwiseCost(mrf,activeSuperLayerNodes,_superLayer,GraphParameters::TypeSuperLayerNode);

    /*set layer-uplayer pairwise : for Robust Pn Model*/
    assignAcrossLayerPairwiseCost(mrf,activeUpLayerNodes,_upLayer,GraphParameters::TypeLayerNode);

    /*set uplayer-superlayer pairwise*/
    assignAcrossLayerPairwiseCost(mrf,activeSuperLayerNodes,_superLayer,GraphParameters::TypeUpLayerNode);

    /*set layer-superlayer pairwise*/
    assignAcrossLayerPairwiseCost(mrf,activeSuperLayerNodes,_superLayer,GraphParameters::TypeLayerNode);

    float t;
    mrf->initialize();
    if(withInitializaiton == true)
    {
        mrf->setInitialization(initialLabeling);
        delete[] initialLabeling;
    }

    MRF::EnergyVal E_smooth = mrf->smoothnessEnergy();
    MRF::EnergyVal E_data   = mrf->dataEnergy();
    cout<<"Energy = "<<E_smooth+E_data<<" (Smoothness,Data) "<<"("<<E_smooth<<","<<E_data<<")"<<endl;
    mrf->clearAnswer();
    mrf->optimize(50,t);
    E_smooth = mrf->smoothnessEnergy();
    E_data   = mrf->dataEnergy();
    cout<<"Energy = "<<E_smooth+E_data<<" (Smoothness,Data) "<<"("<<E_smooth<<","<<E_data<<")"<<endl;

    for(unsigned int i=0;i<activeLayerNodes.size();i++)
    {
        int idx = activeLayerNodes.at(i);
        //_layer.at(idx)->_predictLabel = mrf->getLabel(i) + 1;
        _layer.at(idx)->_predictLabel = mrf->getLabel(i);
        assert(_layer.at(idx)->_predictLabel < s_numClass);
    }

    for(unsigned int i=0;i<activeUpLayerNodes.size();i++)
    {
        int idx = activeUpLayerNodes.at(i);
       // _upLayer.at(idx)->_predictLabel = mrf->getLabel(i + _layerSize) + 1;
        _upLayer.at(idx)->_predictLabel = mrf->getLabel(i + _layerSize);

    }

    for(unsigned int i=0;i<activeSuperLayerNodes.size();i++)
    {
        int idx = activeSuperLayerNodes.at(i);
//        _superLayer.at(idx)->_predictLabel = mrf->getLabel(i + _upLayerSize) + 1;
        _superLayer.at(idx)->_predictLabel = mrf->getLabel(i + _upLayerSize);

    }

    delete[] dCost;
    dCost = NULL;
    delete data;
    data = NULL;
    delete smooth;
    smooth = NULL;
    delete energy;
    energy = NULL;
    delete mrf;
    mrf = NULL;
}

void GraphModel::SetupConstraintTable(cv::Mat& priorConstraintTable)
{
    int numClass = myDataset._parsColorTable.nCategory();
    assert(priorConstraintTable.cols == numClass &&
           priorConstraintTable.rows == numClass);

    if(s_constraintTable!=NULL) s_constraintTable->release();
    s_constraintTable = new cv::Mat(priorConstraintTable.rows,
                              priorConstraintTable.cols,
                              CV_32SC1,cv::Scalar(0));

    priorConstraintTable.copyTo(*s_constraintTable);

    int availabelCount = 0;
    for(int i=0;i<s_constraintTable->rows;i++)
    {
        for(int j=0;j<s_constraintTable->cols;j++)
        {
            if(s_constraintTable->at<int>(i,j)>0 ||
               s_constraintTable->at<int>(j,i)>0)
            {
                availabelCount++;
            }
        }
    }

    cout<<"available constraint label: "<<availabelCount<<"/"<<numClass*numClass<<endl;
}

void GraphModel::SetConfiguration(GraphParameters* loadedParameters)
{
    int numClass = myDataset._parsColorTable.nCategory();

    if(GraphModel::s_initialized == false)
    {
        s_withLayerUnary = false;
        s_withUpLayerUnary = false;
        s_withSuperLayerUnary = false;

        s_withLayerPairwise = false;
        s_withUpLayerPairwise = false;
        s_withSuperLayerPairwise = false;

        s_withUpLayer_LayerPairwise = false;
        s_withSuperLayer_upLayerPairwise = false;
        s_withSuperLayer_LayerPairwise = false;

        s_withConstraintLayer   = false;
        GraphModel::s_withUnLabelRegion = myDataset._withUnLabelRegion;
        GraphModel::s_numClass = numClass;

        if(myDataset._layerUnary != nonactive) s_withLayerUnary = true;
        if(myDataset._uplayerUnary != nonactive) s_withUpLayerUnary = true;
        if(myDataset._superlayerUnary != nonactive) s_withSuperLayerUnary = true;

        if(myDataset._layerPairwise != nonactive) s_withLayerPairwise = true;
        if(myDataset._uplayerPairwise != nonactive) s_withUpLayerPairwise = true;
        if(myDataset._superlayerPairwise != nonactive) s_withSuperLayerPairwise = true;

        if(myDataset._layerPairwise != nonactive) s_withLayerPairwise = true;
        if(myDataset._layerPairwise != nonactive) s_withLayerPairwise = true;
        if(myDataset._layerPairwise != nonactive) s_withLayerPairwise = true;

        if(myDataset._constraintlayer != nonactive) s_withConstraintLayer = true;

        if(myDataset._up_layer_Pairwise != nonactive) s_withUpLayer_LayerPairwise = true;
        if(myDataset._super_layer_Pairwise != nonactive) s_withSuperLayer_LayerPairwise = true;
        if(myDataset._super_up_layerPairwise != nonactive) s_withSuperLayer_upLayerPairwise = true;

        s_constraintLinkStep = 8;
        if(loadedParameters != NULL) s_weights = (*loadedParameters);
        else s_weights.setDefaultParameters();

        GraphModel::s_initialized = true;
    }
}


/* linear parameters for pairwise function: w * f */
/* Two way: 1 keep the pairwise function metric
              w[i]+w[j] for any label combination (i,j), i != j*/
/*          2 the pairwise function is not metric
 *            w[i][j] for one label combination (i,j),i != j*/
/* How to model the interaction between parameters and feature is an open problem now*/

/* note that : node_i < node_j */
MRF::CostVal GraphModel::SmoothTermCost(int node_i,int node_j,int label_i,int label_j,void* extraData)
{
    MRF::CostVal cost = 0;
    /*exchange node_i and node_j*/
    if(node_i > node_j)
    {
        int temp = node_i;
        node_i = node_j;
        node_j = temp;

        temp = label_i;
        label_i = label_j;
        label_j = temp;
    }

    if(label_i != label_j)
    {
        GraphModel* inputGraph = static_cast<GraphModel*>(extraData);
        /*the step for constraint link must make sure that step * width > maxImageWidth*/
        /*so that we can identify different types of links easily*/
        /*layer pairwise*/
        if(node_i < inputGraph->_layerSize && node_j < inputGraph->_layerSize)
        {
            if(GraphModel::s_withConstraintLayer == true)
            {
                if(abs(inputGraph->_layer.at(node_i)->_x - inputGraph->_layer.at(node_j)->_x) == 1 ||
                   abs(inputGraph->_layer.at(node_i)->_y - inputGraph->_layer.at(node_j)->_y) == 1)
                {
                    if(GraphModel::s_weights._layerLinksWeights._paraSizeRow == 1)
                    /*p(i,j) = w[i]+w[j]*/
                        cost = GraphModel::s_weights._layerLinksWeights._paras.at(0).at(label_j)+
                               GraphModel::s_weights._layerLinksWeights._paras.at(0).at(label_i);
                    else
                        cost = GraphModel::s_weights._layerLinksWeights._paras.at(label_i).at(label_j)+
                               GraphModel::s_weights._layerLinksWeights._paras.at(label_j).at(label_i);
                }
                else
                {
                    if(GraphModel::s_weights._constraintWeights._paraSizeRow == 1)
                    {
                        cost = (GraphModel::s_weights._constraintWeights._paras.at(0).at(0)+
                                GraphModel::s_weights._constraintWeights._paras.at(0).at(0))*
                               (1 - GraphModel::s_constraintTable->at<int>(label_i,label_j));
                    }
                    else
                    {
                        cost = (GraphModel::s_weights._constraintWeights._paras.at(0).at(label_j)+
                                GraphModel::s_weights._constraintWeights._paras.at(0).at(label_i))*
                               (1 - GraphModel::s_constraintTable->at<int>(label_i,label_j));
                    }
                }
            }
            else
            {
                if(GraphModel::s_weights._layerLinksWeights._paraSizeRow == 1)
                /*p(i,j) = w[i]+w[j]*/
                    cost = GraphModel::s_weights._layerLinksWeights._paras.at(0).at(label_j)+
                           GraphModel::s_weights._layerLinksWeights._paras.at(0).at(label_i);
                else
                    cost = GraphModel::s_weights._layerLinksWeights._paras.at(label_i).at(label_j)+
                           GraphModel::s_weights._layerLinksWeights._paras.at(label_j).at(label_i);
            }
        }/*uplayer pairwise*/
        else if(node_i >= inputGraph->_layerSize && node_i < inputGraph->_upLayerSize &&
                node_j >= inputGraph->_layerSize && node_j < inputGraph->_upLayerSize)
        {
            if(GraphModel::s_weights._upLayerLinksWeights._paraSizeRow == 1)
                cost = GraphModel::s_weights._upLayerLinksWeights._paras.at(0).at(label_j)+
                       GraphModel::s_weights._upLayerLinksWeights._paras.at(0).at(label_i);
            else
                cost = GraphModel::s_weights._upLayerLinksWeights._paras.at(label_i).at(label_j)+
                       GraphModel::s_weights._upLayerLinksWeights._paras.at(label_j).at(label_i);

        }/*superlayer pairwise*/
        else if(node_i >= inputGraph->_upLayerSize && node_j >= inputGraph->_upLayerSize)
        {
            if(GraphModel::s_weights._superLayerLinksWeights._paraSizeRow == 1)
                cost = GraphModel::s_weights._superLayerLinksWeights._paras.at(0).at(label_j)+
                       GraphModel::s_weights._superLayerLinksWeights._paras.at(0).at(label_i);
            else
                cost = GraphModel::s_weights._superLayerLinksWeights._paras.at(label_i).at(label_j)+
                       GraphModel::s_weights._superLayerLinksWeights._paras.at(label_j).at(label_i);
        }/*uplayer-layer pairwise*/
        else if(node_j >= inputGraph->_layerSize && node_j < inputGraph->_upLayerSize &&
                node_i <  inputGraph->_layerSize)
        {
            if(GraphModel::s_weights._upLayer_LayerLinksWeights._paraSizeRow == 1)
                cost = GraphModel::s_weights._upLayer_LayerLinksWeights._paras.at(0).at(label_j);
            else
                cost = GraphModel::s_weights._upLayer_LayerLinksWeights._paras.at(label_j).at(label_i);
        }/*superlayer-layer pairwise*/
        else if(node_j >= inputGraph->_upLayerSize && node_i < inputGraph->_layerSize)
        {
            if(GraphModel::s_weights._superLayer_LayerLinksWeights._paraSizeRow == 1)
                cost = GraphModel::s_weights._superLayer_LayerLinksWeights._paras.at(0).at(label_j);
            else
                cost = GraphModel::s_weights._superLayer_LayerLinksWeights._paras.at(label_j).at(label_i);
        }/*superlayer-uplayer pairwise*/
        else if(node_j >= inputGraph->_upLayerSize &&
                node_i >= inputGraph->_layerSize && node_i < inputGraph->_upLayerSize)
        {
            if(GraphModel::s_weights._superLayer_upLayerLinksWeights._paraSizeRow == 1)
                cost = GraphModel::s_weights._superLayer_upLayerLinksWeights._paras.at(0).at(label_j);
            else
                cost = GraphModel::s_weights._superLayer_upLayerLinksWeights._paras.at(label_j).at(label_i);
        }
    }
    return cost;
}

void GraphModel::release()
{
    for(unsigned int i=0;i<_layer.size();i++)
    {
        if(_layer.at(i)!=NULL) delete _layer.at(i);
    }

    for(unsigned int i=0;i<_upLayer.size();i++)
    {
        if(_upLayer.at(i)!=NULL) delete _upLayer.at(i);
    }

    for(unsigned int i=0;i<_superLayer.size();i++)
    {
        if(_superLayer.at(i)!=NULL) delete _superLayer.at(i);
    }

    _layer.clear();
    _upLayer.clear();
    _superLayer.clear();
}

