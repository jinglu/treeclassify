#include "BarkEstimator.h"
#include "FeatureExtractor.h"
#include "GloabalVal.h"
#include <Common/FileAPI.h>
#include <Image/Image.h>
#include "EnergyComputor.h"


BarkEstimator::BarkEstimator()
{
}

string BarkEstimator::getSVMName() const
{
    string ret = myDataset._classifierDir + "bark" + MyDataset::getFeatName(myDataset._barkFeatParameters) + "_SVM.yml";
    return ret;
}

void BarkEstimator::extractSamples(const std::vector<FeatureParameter>& featParas,
                                   const std::vector<BasicImage>& barkImages,
                                   const std::vector<BasicImage>& nonBarkImages,
                                   cv::Mat& trainData,
                                   cv::Mat& labelMat)
{
    FeatureExtractor extractor;
    vector<FeatVector> pos_features;
    vector<FeatVector> neg_features;

    vector<FeatVector> sel_pos_features;
    vector<FeatVector> sel_neg_features;

    for (size_t i = 0; i < barkImages.size(); ++i)
    {
        PixelImage barkimg(barkImages[i]);
        cout <<"bark: " << i+1 << "/" << barkImages.size() << " " << barkimg._filenameImage << endl;
        // adjust the mask if the pixel in bark image is black
        for (int y = 0; y < barkimg._image.rows; ++y)
            for (int x = 0; x < barkimg._image.cols; ++x)
            {
                if (barkimg._image.at<cv::Vec3b>(y,x) == cv::Vec3b(0,0,0))
                {
                    barkimg._activeMask.at<uchar>(y,x) = 0;
                }
            }

        vector<FeatVector> features;
        extractor.extractPixelFeature(featParas, barkimg, features);

        for (size_t j = 0; j < features.size(); ++j)
            if (features[j].getDim() > 0)
                pos_features.push_back(features[j]);
    }


    for (size_t i = 0; i < nonBarkImages.size(); ++i)
    {
        PixelImage img(nonBarkImages[i]);
        vector<FeatVector> features;
        extractor.extractPixelFeature(featParas, img, features);
        for (int y = 0, idx = 0; y < img._image.rows; ++y)
            for (int x = 0; x < img._image.cols; ++x, ++idx)
            {
                if (img._activeMask.at<uchar>(y,x) != 128 && features[idx].getDim() > 0)
                    neg_features.push_back(features[idx]);
            }
    }

    //  random sample the samples
    int num_pos = std::min(myDataset._numMaxTrainSamples / 2, int(pos_features.size()));
    int num_neg = std::min(myDataset._numMaxTrainSamples / 2, int(neg_features.size()));

    extractor.randomSampleFeatures(pos_features, num_pos, sel_pos_features);
    extractor.randomSampleFeatures(neg_features, num_neg, sel_neg_features);

    int num_sample = sel_pos_features.size() + sel_neg_features.size();
    int feat_dim = sel_pos_features[0].getDim();

    trainData = cv::Mat(num_sample, feat_dim, CV_32FC1);
    labelMat = cv::Mat(0, 1, CV_32FC1);

    for (size_t i = 0; i < sel_pos_features.size(); ++i)
    {
        for (int j = 0; j < feat_dim; ++j)
            trainData.at<float>(i, j) = sel_pos_features[i].getDataAt(j);
    }

    for (size_t i = 0; i < sel_neg_features.size(); ++i)
    {
        int idx = i + sel_pos_features.size();
        for (int j = 0; j < feat_dim; ++j)
            trainData.at<float>(idx, j) = sel_neg_features[i].getDataAt(j);
    }

    cv::Mat pos_label = cv::Mat::ones(sel_pos_features.size(), 1, CV_32FC1);
    cv::Mat neg_label = cv::Mat::ones(sel_neg_features.size(), 1, CV_32FC1) * (-1.0);

    labelMat.push_back(pos_label);
    labelMat.push_back(neg_label);

    cout << "positive sample " << pos_label.rows << ", negtive sample " << neg_label.rows << endl;


}

void BarkEstimator::pcaAnalize(const cv::Mat &trainData, const cv::Mat &labelMat)
{
    // PCA
    int num_sample = trainData.rows;
    int num_component = 2;
    cv::PCA pca(trainData, cv::Mat(), CV_PCA_DATA_AS_ROW, num_component);
    cv::Mat project_data(num_sample, num_component, CV_32F);
    pca.project(trainData, project_data);

    float left = 1e6, right = -1e6, top = 1e6, bottom = -1e6;
    for (int i = 0; i < num_sample; ++i)
    {
        left = min(left, project_data.at<float>(i,0));
        right = max(right, project_data.at<float>(i,0));

        top = min(top, project_data.at<float>(i,1));
        bottom = max(bottom, project_data.at<float>(i,1));
    }
    cout << "[" << left << "," << right<<"] * [" << top << ","<<bottom<<"]" << endl;
    int dst_width = 1024;
    int dst_height = float(dst_width) / (right-left) * (bottom-top);
    float scale_w = float(dst_width) / (right - left);
    float scale_h = float(dst_height) / (bottom - top);
    float dst_left = left * scale_w;
    float dst_top = top * scale_h;

    int offset = 200;
    dst_left -= offset/2;
    dst_top -= offset/2;
    wjl::Image showimage(dst_width+offset, dst_height+offset, wjl::Color::white());
    for (int i = 0; i < num_sample; ++i)
    {
        int x = project_data.at<float>(i,0) * scale_w - dst_left;
        int y = project_data.at<float>(i,1) * scale_h - dst_top;
        int label = labelMat.at<float>(i,0);
        if (label > 0)
            showimage.draw_circle(x,y,1,wjl::Color::red(), CV_FILLED);
        else
            showimage.draw_circle(x,y,1,wjl::Color::green(), CV_FILLED);

    }
    string pcaname = myDataset._resultDir + "bark_pca.png";
    showimage.write(pcaname);
}

void BarkEstimator::train(const vector<BasicImage> &barkImages,
                          const vector<BasicImage> &nonBarkImages)
{
    cv::Mat train_data, label_mat;
    extractSamples(myDataset._barkFeatParameters, barkImages, nonBarkImages, train_data, label_mat);
    pcaAnalize(train_data, label_mat);
    string svmname = getSVMName();
    trainSVM(svmname, train_data, label_mat);
}

void BarkEstimator::trainSVM(const string svmName,
                             const cv::Mat& trainData,
                             const cv::Mat& labelMat)
{
    cout << "Train SVM.." << endl;
    CvSVM classifier;
    classifier.train(trainData, labelMat);
    cout << "end train, save svm to " << svmName << endl;

    classifier.save(svmName.c_str());
}

//void BarkEstimator::loadSVM(const string svmName, CvSVM &classifier)
//{
//    classifier.clear();
//    classifier.load(svmName);
//}

void BarkEstimator::test(const vector<BasicImage> &testImages)
{
    //CvSVM classifer;

    string svmname = getSVMName();

    _classifer.load(svmname.c_str());

    for (size_t i = 0; i < testImages.size(); ++i)
    {
        PixelImage pimg(testImages[i]);
        pimg.initLabelMap();
        cout << "test: " << i+1<<"/"<<testImages.size() << " " << pimg._filenameImage << endl;
        cv::Mat conf_mat;
        evaluateOneImage(myDataset._barkFeatParameters, pimg, conf_mat);
    }
}

void BarkEstimator::evaluateOneImage(const vector<FeatureParameter> &featParas,
                                     const PixelImage &pimg,
                                     cv::Mat& confMat)
{
    vector<FeatVector> features;
    FeatureExtractor extractor;

    extractor.extractPixelFeatureScreened(featParas, pimg, features);

    int num_sample = features.size();
    int feat_dim = features[0].getDim();

    cv::Mat samples(num_sample, feat_dim, CV_32F);

//    for (size_t i = 0; i < features.size(); ++i)
//    {
//        cv::Mat sample(1,feat_dim, CV_32F);
//        for (int j = 0; j < feat_dim; ++j)
//            sample.at<float>(0,j) = features[i].getDataAt(j);
//        float confidence = _classifer.predict(sample);
//        //cout << confidence << endl;
//    }
    for (int i = 0; i < num_sample; ++i)
        for (int j = 0; j < feat_dim; ++j)
        {
            samples.at<float>(i,j) = features[i].getDataAt(j);
        }
    cv::Mat results;
    _classifer.predict(samples, results);

    confMat = cv::Mat::zeros(pimg._image.rows, pimg._image.cols, CV_32F);
    cv::Mat confMap = cv::Mat::ones(pimg._image.rows, pimg._image.cols, CV_8UC1) * 128;

    for (int i = 0; i < num_sample; ++i)
    {
        float conf = results.at<float>(i, 0);
        int x = features[i]._x;
        int y = features[i]._y;

        confMat.at<float>(y,x) = conf;
        //cout << conf << endl;
        if (conf >= 1.0)
            confMap.at<uchar>(y,x) = 255;
        else if (conf <= -1.0)
            confMap.at<uchar>(y,x) = 0;
        else
            confMap.at<uchar>(y,x) = 128;
    }
    string mapname = myDataset._resultDir + "branch/"+FileAPI::extractFileName(pimg._filenameImage, false) + "_brconf.png";
    cv::imwrite(mapname, confMap);
    cout << "save branch confidence to " << mapname << endl;
}


/////////////////////////////////////////////////////////////
void BarkEstimator::getCandidateWindow(const PixelImage& pimg, const cv::Mat& branchProbMat, const int winsize)
{
    assert(!branchProbMat.empty());

    float thresh = 0.08 * float(winsize * winsize);

    int height = pimg._image.rows;
    int width = pimg._image.cols;
    int step = winsize / 2;
    int starty = step;
    int startx = step;

    vector<cv::Point> candidates;
    cout << "h " << branchProbMat.rows << ", w " << branchProbMat.cols << endl;
    for (int y = starty; y < height-winsize; y += step)
        for (int x = startx; x < width-winsize; x += step)
        {
            //cout << y << ", " << x << endl;
            cv::Rect ror(x, y, winsize, winsize);
            cv::Mat roi = branchProbMat(ror);

            float sum = cv::sum(roi)[0];
            //cout << "sum " << sum << endl;
            if (sum > thresh)
            {
                candidates.push_back(cv::Point(x+step,y+step));
            }
        }

    // filter the neibors
    vector<cv::Point> filter_candidates;

    for (size_t i = 0; i < candidates.size(); ++i)
    {
        cv::Point& pi = candidates[i];
        if (filter_candidates.empty())
            filter_candidates.push_back(pi);
        else
        {
            for (size_t j = 0; j < filter_candidates.size(); ++j)
            {
                cv::Point& pj = filter_candidates[j];
                int difx = abs(pi.x - pj.x);
                int dify = abs(pi.y - pj.y);
                if (difx <= 1 || dify <= 1)
                    continue;
                filter_candidates.push_back(pi);
            }
        }

    }


    //candidates.clear();
    //candidates = filter_candidates;

    cv::Mat brimg(height, width, CV_8UC3);
    cv::Vec3b brcolor = myDataset._parsColorTable.name2color("branch");

    for (int y = 0; y < height; ++y)
        for (int x = 0; x < width; ++x)
        {
            for (int k = 0; k < 3; ++k)
                brimg.at<cv::Vec3b>(y,x)[k] = int(float(brcolor[k]) * branchProbMat.at<float>(y,x));
        }

    for (size_t i = 0; i < candidates.size(); ++i)
    {
        cv::Rect rect(candidates[i].x-step, candidates[i].y-step, winsize, winsize);
        cv::rectangle(brimg, rect, cv::Scalar(0,255,0), 1);
    }

    string name = myDataset._resultDir + "branch/" + FileAPI::extractFileName(pimg._filenameImage,false) + "_br.png";


    int search_h = (height - winsize) / 2;
    int search_w = (width - winsize) / 2;

    EnergyComputor energyComp;
    float homothresh = 0.001;
    float contrastmargin = 3;

    float min_energy = 0;
    cv::Rect optimal_rect;

    for (size_t i = 0; i < candidates.size(); ++i)
    {
        int cx = candidates[i].x;
        int cy = candidates[i].y;
        int num_ignore_h = 0;
        for (int sh = step; sh < search_h; sh += step)
        {
            int num_ignore_w = 0;

            for (int sw = step; sw < search_w; sw += step)
            {
                int sx = cx - sw / 2;
                int sy = cy - sh / 2;
                int ex = cx + sw / 2;
                int ey = cy + sh / 2;
                if (sx < 0 || sy < 0 || ex >= width || ey >= height)
                    continue;
                cv::Rect srect(sx, sy, sw, sh);
                wjl::Rectangle myrect(srect);
                cout << "candi " << i << "/" << candidates.size()<< endl;
                myrect.print();
                float energy1 = energyComp.computeEnergy1(branchProbMat, myrect, homothresh, contrastmargin);

                if (min_energy > energy1)
                {
                    min_energy = energy1;
                    optimal_rect = srect;
                    num_ignore_w = 0;
                }
                else
                {
                    num_ignore_w++;
                }
                cout << "num ignore w " << num_ignore_w << endl;
                if (num_ignore_w > width / 4)
                {
                    num_ignore_h++;
                    break;
                }
            }

        }
    }


    cv::rectangle(brimg, optimal_rect, cv::Scalar(255,0,0), 1);
    cv::imwrite(name, brimg);
}
