#include "DataSample.h"

#include <Common/StatisticAPI.h>
#include <iostream>

using namespace std;

EntropyType prob2entropy(const EntropyType prob)
{
    if (prob < 1e-8)
        return 0.0;
    return (-prob * log2(prob));
}

int DataSampleSet::s_sampleDataLength;


/////////////////********************* DataSample **************************/////////////////
DataSample::DataSample()
    : _sampleIdx(-1),
      _ptFeat(NULL)
{

}


DataSample::DataSample(const int sampleIdx)
    : _sampleIdx(sampleIdx),
      _ptFeat(NULL)
{

}


DataSample::~DataSample()
{
    if (_isReleaseFeat)
    {
        if (_ptFeat != NULL)
            delete _ptFeat;
        _ptFeat = NULL;
    }
}

int DataSample::getLabel() const
{
    return _ptFeat->_label;
}

int DataSample::getFeatDim() const
{
    return _ptFeat->getDim();
}

DataType DataSample::getDataAt(const int idx) const
{
    return _ptFeat->getDataAt(idx);
}

int DataSample::getSampleIdx() const
{
    return _sampleIdx;
}

void DataSample::setSampleIdx(const int idx)
{
    _sampleIdx = idx;
}

void DataSample::setFeatPt(FeatVector *featPt)
{
    _ptFeat = featPt;
}

////////////////////**************** DataSampleSet ***************************/////////////////
DataSampleSet::DataSampleSet()
    : _labelMapping(NULL),
      _categoryDists(NULL)
{

}

DataSampleSet::DataSampleSet(const int sampleDataLength, const int datasetType, const vector<int> &labelSet)
    : _sampleDataLength(sampleDataLength),
      _datasetType(datasetType),
      _labelSet(labelSet),
      _labelMapping(NULL),
      _categoryDists(NULL)
{
    setLabelMapping();
}

DataSampleSet::DataSampleSet(const DataSampleSet *inputDataset)
    : _sampleDataLength(inputDataset->_sampleDataLength),
      _datasetType(inputDataset->_datasetType),
      _labelSet(inputDataset->_labelSet),
      _labelMapping(NULL),
      _categoryDists(NULL)
{
    setLabelMapping();
}

void DataSampleSet::setLabelMapping()
{
    // intial mapping
    if (_labelMapping != NULL)
    {
        delete _labelMapping;
    }
    _labelMapping = new map<int,int>();
    for (size_t i = 0; i < _labelSet.size(); ++i)
    {
        pair<int,int> p(_labelSet.at(i), i);
        _labelMapping->insert(p);
    }

}

void DataSampleSet::releaseSamples()
{
    for (size_t i = 0; i < _ptSamples.size(); ++i)
        if (_ptSamples[i] != NULL)
            delete _ptSamples[i];
}

DataSampleSet::~DataSampleSet()
{

    if(_labelMapping != NULL)  delete _labelMapping;
    if(_categoryDists != NULL) delete[] _categoryDists;
}

int DataSampleSet::getNumSamples() const
{
    return _ptSamples.size();
}

int DataSampleSet::getNumLabels() const
{
    return _labelSet.size();
}

int DataSampleSet::getLabelAt(const int idx) const
{
    return _labelSet.at(idx);
}

int DataSampleSet::getLabelIdx(const int label)
{
    if (_labelMapping == NULL)
        setLabelMapping();
    map<int,int>::iterator it = _labelMapping->find(label);
    return it->second;
}

int DataSampleSet::getDatasetType() const
{
    return _datasetType;
}

DataSample* DataSampleSet::getSamplePtAt(const int idx) const
{
    return _ptSamples.at(idx);
}

void DataSampleSet::insertSample(DataSample *sample)
{
    sample->setSampleIdx(_ptSamples.size());
    _ptSamples.push_back(sample);
}

void DataSampleSet::searchActiveAttributes(std::vector<int> &activeAttributesIdx)
{
    //int featDim = this->_sampleDataLength;
    int featDim = this->getSampleDataLength();
    int numSamples = this->getNumSamples();

    vector<DataType> vmins(featDim, 1e8);
    vector<DataType> vmaxs(featDim, -1e8);

    for (int i = 0; i < numSamples; ++i)
    {
        for (int j = 0; j < featDim; ++j)
        {
            DataType data = _ptSamples[i]->getDataAt(j);
            vmaxs[i] = std::max(vmaxs[i], data);
            vmins[i] = std::min(vmins[i], data);
        }
    }

    _featValueRanges.clear();
    activeAttributesIdx.clear();

    for (int j = 0; j < featDim; ++j)
    {
        if (vmaxs[j] - vmins[j] > 1e-6)
            activeAttributesIdx.push_back(j);
        pair<DataType, DataType> attributRange = std::make_pair<DataType, DataType>(vmins[j], vmaxs[j]);
        _featValueRanges.push_back(attributRange);
    }
    cout<<"Number of active attributes: "<<activeAttributesIdx.size()<<"/"<<featDim<<endl;
}

void DataSampleSet::buildAttributesRange(const vector<int> &attributeIdxs, vector<pair<DataType,DataType> > &featValueRanges) const
{
    featValueRanges.clear();

    for (size_t j = 0; j < attributeIdxs.size(); ++j)
    {
        pair<DataType, DataType> atrributeRange(1e8, -1e8);
        featValueRanges.push_back(atrributeRange);
        int attIdx = attributeIdxs[j];

        for (size_t i = 0; i < _ptSamples.size(); ++i)
        {
            DataType dataI =  _ptSamples[i]->getDataAt(attIdx);
            featValueRanges[j].first = std::min(featValueRanges[j].first,dataI);
            featValueRanges[j].second = std::max(featValueRanges[j].second, dataI);
        }
    }
}

void DataSampleSet::randomSample(const float sampleRatio, DataSampleSet *&ptSelectSet) const
{
    if (ptSelectSet != NULL)
        delete ptSelectSet;

    ptSelectSet = new DataSampleSet(DataSampleSet::s_sampleDataLength, getDatasetType(), _labelSet);

    vector<int> selectIdxs;
    int numSamples = _ptSamples.size();
    StatisticAPI::generateRandomSequence(0,
                                         numSamples-1,
                                         (int)(numSamples*sampleRatio),
                                         selectIdxs);

    for (int i = 0; i < selectIdxs.size(); ++i)
    {
        ptSelectSet->_ptSamples.push_back(this->_ptSamples.at(selectIdxs.at(i)));
    }
}

void DataSampleSet::estimatePurity(const bool update)
{
    if (_categoryDists == NULL || update)
    {
        int numLabels = _labelSet.size();

        if (_categoryDists == NULL)
            _categoryDists = new EntropyType[numLabels];

        for (int i = 0; i < numLabels; ++i)
            _categoryDists[i] = 0;

        for (int i = 0; i < _ptSamples.size(); ++i)
        {
            int label = this->getSamplePtAt(i)->getLabel();
            _categoryDists[this->getLabelIdx(label)]++;
        }
        int totalNumSamples = _ptSamples.size();
        cout<<endl<<"Estimate Purity:  sample size        : "<<_ptSamples.size()<<endl;
        cout<<"sample distribution: ";
        for (int i = 0; i < numLabels; ++i)
        {
            _categoryDists[i] /= (float)totalNumSamples;
            cout<<_categoryDists[i]<<" ";
        }
        cout<<endl;
    }
}

EntropyType DataSampleSet::estimateEntropy(const bool updateDistribution)
{
    estimatePurity(updateDistribution);

    EntropyType entropy = 0;
    for (int i = 0; i < getNumLabels(); ++i)
    {
        entropy +=  prob2entropy(_categoryDists[i]);
    }
    return entropy;
}

int DataSampleSet::getSampleDataLength()
{
    return s_sampleDataLength;
}

void DataSampleSet::setSampleDataLength(const int length)
{
    s_sampleDataLength = length;
}
