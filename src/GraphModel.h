#ifndef GRAPHMODEL_H
#define GRAPHMODEL_H

#include "GraphNode.h"
#include "SuperPixelImage.h"
#include "MyDataset.h"

#include <fstream>
#include "MRF/GCoptimization.h"

class PotentialParameters
{
public:
    PotentialParameters();
    PotentialParameters(const int setParaSizeRow,
                        const int setParaSizeCol,
                        const double intialValue,
                        const weightType status);

    bool operator==(const PotentialParameters& T);


    void setParameter(const int setParaSizeRow,
                      const int setParaSizeCol,
                      const double intialValue,
                      const weightType status);
    void setStatus(const weightType status);

    void writeWeight(std::ofstream& out, const std::string potentialType);
    void loadWeight(std::ifstream& in);

    void printWeight();
    void printStatus(const std::string potentialType);
    void print(const std::string potentialType);

    void getParameters(std::vector<double>& weight,std::vector<weightType>& weightStatus) const;
    void setParameters(const std::vector<double>& weight, const std::vector<weightType>& weightStatus, int& startIndex);

    int getSize() const;

public:
    int _paraSizeRow;
    int _paraSizeCol;
    std::vector< std::vector<double> > _paras;
    std::vector< std::vector<weightType> > _parasStatus;
};

////////////////////////////////////////////////////////////

class GraphParameters
{
public:
    enum NodeType{TypeLayerNode, TypeUpLayerNode, TypeSuperLayerNode};
    GraphParameters();

    bool operator==(const GraphParameters& T);

    void setDefaultParameters();
    void getParameters(std::vector<double>& weight, std::vector<weightType>& weightStatus) const;
    void setParameters(const std::vector<double>& weight,const std::vector<weightType>& weightStatus);

    void print();

    void writeToFile(std::ofstream& out);
    void loadFromFile(std::ifstream& in);

public:
    /*unary potential weights*/
    PotentialParameters _layerNodesWeights;
    PotentialParameters _upLayerNodesWeights;
    PotentialParameters _superLayerNodesWeights;

    /*pairwise potential weights*/
    PotentialParameters _layerLinksWeights;
    PotentialParameters _upLayerLinksWeights;
    PotentialParameters _superLayerLinksWeights;

    /*across layer pairwise potential weights*/
    PotentialParameters _upLayer_LayerLinksWeights;
    PotentialParameters _superLayer_LayerLinksWeights;
    PotentialParameters _superLayer_upLayerLinksWeights;

    PotentialParameters _constraintWeights;
};

///////////////////////////////////////////////////////////

class GraphModel
{
public:

    GraphModel();
    explicit GraphModel(const int nClass);

    virtual ~GraphModel();

    /* Set up
     */
    void setupGraph(const BasicImage& image);

    void setupGraph(const PixelImage* image,cv::Mat* layerProb,cv::Mat* layerProbIndex);

    void setupGraph(const SuperPixelImage* image,
                    cv::Mat* layerProb,cv::Mat* layerProbIndex,
                    cv::Mat* upLayerProb,cv::Mat* upLayerProbIndex,
                    cv::Mat* superLayerProb,cv::Mat* superLayerProbIndex);

    void setupLayer(const PixelImage* image,cv::Mat* layerProb,cv::Mat* layerProbIndex);

    void setupLayer(const SuperPixelImage* image,cv::Mat* layerProb,cv::Mat* layerProbIndex);

    void setupUpLayer(const SuperPixelImage* image,
                      cv::Mat* upLayerProb,cv::Mat* upLayerProbIndex);

    void setupSuperLayer(const SuperPixelImage* image,
                         cv::Mat* superLayerProb,cv::Mat* superLayerProbIndex);

    void setupConstraintLayer();

    /* static function
     */

    static void AssignWeightVector(GraphParameters& setWeight);

    static void SetupConstraintTable(cv::Mat& priorConstraintTable);

    /* MRF inference
     */
    void viewGraphLabel();

    /*map inference with graph cut*/
    void mapInference(bool withInitializaiton = false);
    /*map inference with loopy belief propagation*/
    void mapInferenceBP(bool withInitializaiton = false);
    void writeWeights();
    void loadWeights();

    void assignUnaryCost(MRF::CostVal* dCost,
                         int numLabels,
                         std::vector<int>& activeNodeIndexs,
                         std::vector<GraphNode*>& inputLayer,
                          GraphParameters::NodeType nodeType,
                         int *initialLabeling = NULL);

    void assignPairwiseCost(MRF* mrf,
                            std::vector<int>& activeNodeIndexs,
                            std::vector<GraphNode*>& inputLayer,
                            GraphParameters::NodeType nodeType);

    void assignAcrossLayerPairwiseCost(MRF* mrf,
                                       std::vector<int>& activeNodeIndexs,
                                       std::vector<GraphNode*>& inputLayer,
                                       GraphParameters::NodeType linkType);

    void release();


    /*must be called before use graph model*/
    static void SetConfiguration(GraphParameters* loadedParameters);

public:

    /*layer of different layer*/
    std::vector<GraphNode*> _layer;
    std::vector<GraphNode*> _upLayer;
    std::vector<GraphNode*> _superLayer;

    //string imageFileName;
    BasicImage _inputImage;

    int _width;
    int _height;

    /* static variables
     */
    static bool s_withConstraintLayer;

    /*link configuration*/
    static bool s_withLayerUnary;
    static bool s_withUpLayerUnary;
    static bool s_withSuperLayerUnary;

    static bool s_withLayerPairwise;
    static bool s_withUpLayerPairwise;
    static bool s_withSuperLayerPairwise;

    static bool s_withUpLayer_LayerPairwise;
    static bool s_withSuperLayer_upLayerPairwise;
    static bool s_withSuperLayer_LayerPairwise;

    static bool s_initialized;

    static int s_constraintLinkStep;
    static GraphParameters s_weights;
    static cv::Mat*   s_constraintTable;    // store valid constraint combination
    static int s_numClass;
    static MRF::CostVal SmoothTermCost(int node_i,int node_j,int label_i,int label_j,void* extraData);

    static bool s_withUnLabelRegion;        // include unlabeled region or not
                                          // for training, if it is true, then
                                          // exclude unlabeled region
                                          // for testing, always be true


    /* sizes
     */
    int _layerSize;
    int _upLayerSize;
    int _superLayerSize;
  };

#endif // GRAPHMODEL_H
