#ifndef SUPERPIXELIMAGE_H
#define SUPERPIXELIMAGE_H

#include "PixelImage.h"

class SuperPixel
{
public:
    SuperPixel();
    SuperPixel(const int id);
    void setCenter();
    void setBoundBox();

public:
    int _id;
    int _label;
    std::vector<cv::Point> _points;
    std::vector<int> _adjSpIds;
    cv::Point _center;
    cv::Rect _boundbox;
    FeatVector _feat;

    std::vector<float> _linkWeight;

};

class SuperPixelImage : public PixelImage
{
public:
    enum SPType {SuperSLIC, SuperKMean};
    SuperPixelImage();
    SuperPixelImage(const BasicImage& bimg, const SPType sptype = SuperSLIC);
    SuperPixelImage(const PixelImage& pimg, const SPType sptype = SuperSLIC);

    virtual ~SuperPixelImage(){}

    void constructSP(const SPType sptype);

    bool constructSLICSP();

    void viewSuperPixel(bool isDrawBoundary = false);

    void segmentationPostProcessing();

    void segResultP2SP();

public:
    std::vector<SuperPixel> _superPixels;
    cv::Mat _superPixelMap;
};

#endif // SUPERPIXELIMAGE_H
