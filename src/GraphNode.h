#ifndef GRAPHNODE_H
#define GRAPHNODE_H

#include <vector>

class GraphNode;

class GraphNodeLink
{
public:
    GraphNodeLink();
    typedef float WType;

public:
    WType _w;   // link weight
    GraphNode* _linkedNode;  /*linked node*/
};

class GraphNode
{
public:
    GraphNode();

    void insertLayerlinks(GraphNode* T, const GraphNodeLink::WType setw = 0);
    void insertUpLayerlinks(GraphNode* T, const GraphNodeLink::WType setw = 0);
    void insertSuperLayerlinks(GraphNode* T, const GraphNodeLink::WType setw = 0);
    void insertConstraintLayerlinks(GraphNode* T, const GraphNodeLink::WType setw = 0);

public:
    /* link & link-wieght*/
    std::vector< GraphNodeLink >   _layerlinks;         // the link in the same layers
    std::vector< GraphNodeLink >   _upLayerlinks;         // the link across different layers
    std::vector< GraphNodeLink >   _superLayerlinks;      // the link across different layers
    std::vector< GraphNodeLink >   _constraintLayerlinks; // the link to constraint nodes

    std::vector<float> _featVector;           // the feature of the node

    int _index;                          // the index of nodes
    int _activeIndex;                    // the index of nodes active
    int _trueLabel;                      // ground truth label
    int _predictLabel;                   // predicted label

    int _x;                              // position
    int _y;
    bool _isActive;                      // active flag
    bool _withLabel;                     // is the node with ground truth label
};


#endif // GRAPHNODE_H
