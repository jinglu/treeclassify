#include "Feature.h"
#include <assert.h>

//std::string FeatureParameter::getStrName(const FeatureParameter::FeatureName name)
//{
//    std::string ret;
//    switch (name)
//    {
//    case FeatureParameter::SHAPEMASK:
//        ret = "SHAPEMASK";
//        break;
//    case FeatureParameter::SHAPEMOMENT:
//        ret = "SHAPEMOMENT";
//        break;
//    case FeatureParameter::TEXTON:
//        ret = "TEXTON";
//        break;
//    case FeatureParameter::HOG:
//        ret = "HOG";
//        break;
//    default:
//        ret = "NONE";
//    }
//    return ret;
//}

std::string FeatureParameter::strName() const
{
    std::string ret;
    switch (_featName)
    {
    case FeatureParameter::SHAPEMASK:
        ret = "SHAPEMASK";
        break;
    case FeatureParameter::SHAPEMOMENT:
        ret = "SHAPEMOMENT";
        break;
    case FeatureParameter::EDGEHIST:
        ret = "EDGEHIST";
        break;
    case FeatureParameter::TEXTON:
        ret = "TEXTON";
        break;
    case FeatureParameter::HOG:
        ret = "HOG";
        break;
    case FeatureParameter::SIFT:
        ret = "SIFT";
        break;
    case FeatureParameter::COLOR:
        ret = "COLOR";
        break;
    default:
        ret = "NONE";
    }
    return ret;
}

std::string FeatureParameter::strLevel() const
{
    std::string ret;
    switch (_featLevel)
    {
    case FeatureParameter::PIXELLEVLE:
        ret = "PIXEL";
        break;
    case FeatureParameter::SUPERPIXELLEVEL:
        ret = "SUPER";
        break;
    case FeatureParameter::OBJECTLEVEL:
        ret = "OBJECT";
        break;
    }
    return ret;
}
/////////////////////////////////////////////////////////////////



FeatVector::FeatVector()
    : _label(-1)
{

}

int FeatVector::getLabel() const
{
    return _label;
}
void FeatVector::setLabel(const int label)
{
    _label = label;
}

void FeatVector::setPos(const int x, const int y)
{
    _x = x;
    _y = y;
}

size_t FeatVector::getDim() const
{
    return _data.size();
}

DataType FeatVector::getDataAt(const int idx) const
{
    assert(idx < _data.size());
    assert(idx >= 0);

    return _data.at(idx);
}

std::vector<DataType>* FeatVector::getDataPtr()
{
    return &(_data);
}

void FeatVector::clear()
{
    _data.clear();
    _label = -1;
}

void FeatVector::clearData()
{
    _data.clear();
}

void FeatVector::copyData2(std::vector<DataType> &dstData)
{
    dstData.clear();
    dstData = this->_data;
}

void FeatVector::normalize()
{
    DataType sum = DataType(0);

    for (size_t i = 0; i < _data.size(); ++i)
        sum += _data[i];
    if (sum > 0)
    {
        for (size_t i = 0; i < _data.size(); ++i)
            _data[i] /= sum;
    }
}
