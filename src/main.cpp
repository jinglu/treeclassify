#include "GloabalVal.h"
#include "Classifier.h"
#include "RandomClassifer.h"
#include "Clustering.h"
#include "Bow.h"
#include "BarkEstimator.h"
#include "SuperPixelImage.h"
#include <boost/program_options.hpp>

using namespace std;
namespace po = boost::program_options;
int main(int argc, char *argv[])
{
    myDataset.configure();
    cv::Vec3b branchColor = myDataset._parsColorTable.name2color("branch");
    cv::Vec3b crownColor = myDataset._parsColorTable.name2color("crown");
    myDataset._operation = MyDataset::TREESEGEMNTATION;

    RandomClassifer rClassifer;


    po::options_description cmd_desc("Options");
    cmd_desc.add_options()
            ("help,h", "Display this information")
            ("train,L", "Train")
            ("test,T", "Test")
            ("bowbuild", "Bag of words, build vocabulary")
            ("bowtrain", "Bag of words, train classifiers")
            ("bowtest", "Bag of words, test classifiers")
            ("cluster,C", "Kmeans cluster")
            ("barktrain", "train bark svm classifier")
            ("barktest", "estimate bark confidence")
            ("parstrain", "tree segmentation train")
            ("parstest", "tree segmentation test")
            ("debug", "for debug");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, cmd_desc), vm);
    po::notify(vm);

    if (vm.count("help"))
    {
        cout << cmd_desc << endl;
        return 1;
    }

    if (vm.count("train"))
    {
        Classifier treeTypeClassifer;
        cout<<"2D feature classifier learning... number of images " << myDataset._trainImages.size() <<endl;
        treeTypeClassifer.speciesTrain(myDataset._trainImages, crownColor);
        //rClassifer.train(myDataset._trainImages, crownColor);
    }

    if (vm.count("test"))
    {
        Classifier treeTypeClassifer;

        cout<<"testing classifiers for segmentaion" << endl;
        if(myDataset._testImages.size()>0)
            treeTypeClassifer.speciesTest(myDataset._testImages, crownColor);
            //rClassifer.test(myDataset._testImages);
//		if(myDataset.validateImages.size()>0)
//			treeClassifer.Test(myDataset.validateImages);
    }

    if (vm.count("bowbuild"))
    {
        Bow bow;

        //treeClassifer.trainBow(myDataset._trainImages);

        bow.buildvoc(myDataset._trainImages,
                     myDataset._bowname,
                     myDataset._vocname,
                     crownColor);
    }

    if (vm.count("bowtrain"))
    {
         Bow bow;
         //treeClassifer.trainBow(myDataset._trainImages);

        bow.trainbow(myDataset._trainImages,
                     myDataset._vocname,
                     myDataset._bowsample,
                     crownColor);
    }

    if (vm.count("bowtest"))
    {
         Bow bow;
         //treeClassifer.testBow(myDataset._testImages);
        bow.test_classifiers(myDataset._testImages, crownColor);
    }

    if (vm.count("cluster"))
    {
        cout << "Cluster targets..." << endl;
        Clustering clustering;
        clustering.objLevelCluster(myDataset._testImages, 5, crownColor);
        //clustering.objLevelClusterPCA2d(myDataset._testImages);
    }

    if (vm.count("barktrain"))
    {
        BarkEstimator bark_estimator;
        bark_estimator.train(myDataset._barkImages, myDataset._trainImages);
    }

    if (vm.count("barktest"))
    {
        BarkEstimator bark_estimator;
        bark_estimator.test(myDataset._testImages);
    }

    if (vm.count("parstrain"))
    {
        myDataset._operation = MyDataset::TREESEGEMNTATION;
        Classifier treeSegClassifer;
        treeSegClassifer.parsingTrain(myDataset._trainImages);
    }

    if (vm.count("parstest"))
    {
        myDataset._operation = MyDataset::TREESEGEMNTATION;
        Classifier treeSegClassifer;
        treeSegClassifer.parsingTest(myDataset._testImages);
    }
    if (vm.count("debug"))
    {
        for (size_t i = 0; i < myDataset._testImages.size(); ++i)
        {
            SuperPixelImage spimg(myDataset._testImages[i]);
            spimg.viewSuperPixel(true);
        }
    }
    return 0;
}
