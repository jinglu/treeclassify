#ifndef ENERGYCOMPUTOR_H
#define ENERGYCOMPUTOR_H

#include "SuperPixelImage.h"
#include <Primitives/Primitives.h>

class EnergyComputor
{
public:
    EnergyComputor();


    float computeHomogeneity(const cv::Mat& probMat,
                             const wjl::Rectangle& rect,
                             const float threshold) const;

    float computeConstrast_term1(const cv::Mat& probMat,
                           const wjl::Rectangle& rect,
                           const int margin);

    float computeConstrast(const cv::Mat& probMat,
                           const wjl::Rectangle& rect,
                           const int margin,
                           const float d);

    float computeEnergy1(const cv::Mat& probMat,
                         const wjl::Rectangle& rect,
                         const float homoThresh,
                         const int contrastMargin);
};

#endif // ENERGYCOMPUTOR_H
