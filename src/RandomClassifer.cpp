
#include "RandomClassifer.h"
//#include "RandomForest.h"
#include "GloabalVal.h"
#include <Common/FileAPI.h>
#include <iostream>

using namespace std;

RandomClassifer::RandomClassifer()
    : _objectRf(NULL)
{
}


void RandomClassifer::train(const std::vector<BasicImage>& trainImages, const cv::Vec3b& activeColor)
{
    _rfFileName = myDataset._objModelName;
    //_rfFileName = myDataset._modelName;
    cout << "Classifier Name: " << _rfFileName << endl;

    vector<FeatVector> sampleFeatureSet;
    _extractor.extractImageLevelTrainFeature(myDataset._objFeatParameters, activeColor, myDataset._trainImages, sampleFeatureSet);
    trainClassifier(_rfFileName, sampleFeatureSet);
}

// convert sample feature set --> DataSample style ,
// then used for RandomForest class

void RandomClassifer::trainClassifier(const string rfname, vector<FeatVector> &sampleFeatureSet)
{
    // initial the train set
    DataSampleSet trainSet;
    int numClass = myDataset._speciesColorTable.nCategory();
    vector<string> treenames = myDataset._speciesColorTable.getSortedClassNames();

    //myDataset._treeCategory.getLabels(trainSet._labelSet); // copy the labe set
    for (int i = 0; i < numClass; ++i)
        trainSet._labelSet.push_back(i);

    int numSamples = sampleFeatureSet.size();
    assert(numSamples > 0);
    int featDim = sampleFeatureSet.at(numSamples - 1).getDim();
    trainSet.setSampleDataLength(featDim);

    for (int i = 0; i < numSamples; ++i)
    {
        //int sampleLabel = sampleFeatureSet.at(i).getLabel();
        DataSample* ptSample = new DataSample(i);
        ptSample->setFeatPt(&(sampleFeatureSet[i]));
        trainSet._ptSamples.push_back(ptSample);
    }

    vector<int> trainSampleStat(numClass, 0);

    for (int i = 0; i < numSamples; ++i)
    {
        int label = sampleFeatureSet.at(i).getLabel(); // label == labelIdx
        //int labelIdx = myDataset
        assert(label >= 0 && label < numClass);
        assert(sampleFeatureSet.at(i).getDim() == featDim);
        trainSampleStat[label]++;
    }

    for (int i = 0; i < numClass; ++i)
    {
        cout << treenames[i] << ": " << trainSampleStat[i] << " " << float(trainSampleStat[i]) / float(numSamples) * 100 << "%" << endl;
    }

    // random forest train
    RandomForest rtrees(myDataset._numThreads);
    rtrees.constructForest(&trainSet);
    rtrees.saveForest(rfname);
    rtrees.classifySamples(&trainSet);
    trainSet.releaseSamples();

    // release memory of features
    sampleFeatureSet.clear();
}

/////////////////////////////////////////////////////////////////////////////////////

void RandomClassifer::test(vector<BasicImage> &testImages)
{
    testInitialize();

    for (size_t i = 0; i < testImages.size(); ++i)
    {
        cout << "\n("<<(i+1)<< "/"<<testImages.size()<< ") " << testImages.at(i)._filenameImage
             << "\n" << testImages.at(i)._filenameMask << endl;

        PixelImage testImage(testImages[i]);
        testBasicClassifer(testImage);
    }
}

bool RandomClassifer::loadRandomForest(const string rfname, RandomForest *&rtrees)
{
    if (rtrees != NULL)
        delete rtrees;
    rtrees = NULL;

    ifstream input;
    if (FileAPI::fileExistCheck(rfname.c_str(), input))
    {
        input.close();
        rtrees = new RandomForest();
        rtrees->loadForest(rfname);
    }
    input.close();

    if (rtrees == NULL)
    {
        cout << "Load random forest failed!" << endl;
        return false;
    }
    cout << "load random forest " << rfname << endl;
    return true;
}

void RandomClassifer::testInitialize()
{
    _rfFileName = myDataset._objModelName;
    loadRandomForest(_rfFileName, _objectRf);
}

void RandomClassifer::testBasicClassifer(PixelImage &pimg)
{
    FeatVector feat;
    _extractor.extractImageLevelFeature(myDataset._objFeatParameters, pimg, feat);

    DataSample testSample;
    testSample.setFeatPt(&feat);
    vector<float> predictedProbs;

    _objectRf->predictClassProbs(&testSample, predictedProbs);
    pimg.setTreeTypeProbs(predictedProbs);
    pimg.saveTreeTypeProbFile();
}
