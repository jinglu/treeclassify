#ifndef RANDOMCLASSIFER_H
#define RANDOMCLASSIFER_H

#include "PixelImage.h"
#include "FeatureExtractor.h"
#include "RandomForest.h"

class RandomClassifer
{
public:
    RandomClassifer();

    /* Train */
    void train(const std::vector<BasicImage>& trainImages, const cv::Vec3b& activeColor);

    void trainClassifier(const std::string rfname, std::vector<FeatVector>& sampleFeatureSet);

    /* Test */
    void test(std::vector<BasicImage>& testImages);

    void testInitialize();
    bool loadRandomForest(const std::string rfname, RandomForest*& rtrees);

    void testBasicClassifer(PixelImage& pimg);

public:
    std::string _rfFileName;

    RandomForest* _objectRf;

    FeatureExtractor _extractor;
};

#endif // RANDOMCLASSIFER_H
