#ifndef FEATURE_H
#define FEATURE_H
#include <vector>
#include <string>

class FeatureParameter
{
public:
    enum FeatureLevel{PIXELLEVLE, SUPERPIXELLEVEL, OBJECTLEVEL};
    enum FeatureName {SHAPEMASK,SHAPEMOMENT, EDGEHIST,
                      TEXTON, HOG, SIFT, COLOR};
    FeatureParameter(){};

    //static std::string getStrName(const FeatureParameter::FeatureName name);
    std::string strName() const;
    std::string strLevel() const;

public:
    FeatureName _featName;
    FeatureLevel _featLevel; //pixle, superpixel, image level
    int _numScales;
    mutable int _sampleStep;
    int _patchSize;
};

typedef float DataType;
class Feature
{
public:
    Feature();
};

class FeatVector
{
public:
    FeatVector();
    virtual ~FeatVector(){}

    int getLabel() const;
    void setLabel(const int label);
    void setPos(const int x, const int y);

    size_t getDim() const;
    std::vector<DataType>* getDataPtr();
    DataType getDataAt(const int idx) const;
    void copyData2(std::vector<DataType>& dstData);

    void clear();
    void clearData();
    void normalize();

public:
    int _label;
    std::vector<DataType> _data;
    int _x, _y;
};


#endif // FEATURE_H
