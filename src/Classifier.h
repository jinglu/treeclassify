#ifndef CLASSIFIER_H
#define CLASSIFIER_H
#include <ALGLIB/dataanalysis.h>
#include "GloabalVal.h"
#include "FeatureExtractor.h"
#include "SuperPixelImage.h"



typedef float DataType;
class Classifier
{
public:
    Classifier();
    ~Classifier();

    // for tree category classify
    /* Train
     */
    void speciesTrain(const std::vector<BasicImage>& trainImages, const cv::Vec3b& activeColor);

    void analizeTrainSampleStas(std::vector<FeatVector>& sampleFeatureSet,
                                const vector<string>& sortedClassNames);
    void extractTrainSamples(std::vector<FeatVector>& sampleFeatureSet,
                             const int numClass,
                             alglib::real_2d_array*& trainData);

    void trainClassifierRF(const std::string modelName, const int numClass, std::vector<FeatVector>& sampleFeatureSet);

    /* Test
     */
    void loadRandomForest(const std::string modelName, alglib::decisionforest*& forest);
    void speciesTest(const std::vector<BasicImage>& testImages, const cv::Vec3b& activeColor);

    void testInitialize();
    void extractTestSample(const FeatVector& feat, alglib::real_1d_array &sample);

    void estimateSampleProb(alglib::decisionforest* rTrees,
                            alglib::real_1d_array& sample,
                            alglib::real_1d_array& prob);
    // pixel level
    //void testBasicClassifers(PixelImage& testImage, std::vector<float>& treeTypeProbs, cv::Mat& confusionMatrix);
    void testPixelBasicClassifers(SuperPixelImage& testImage,
                                  const vector<FeatureParameter>& featParas,
                                  const ColorCategoryTable& colorCategory,
                                  vector<float>* treeTypeProbs,
                                  cv::Mat *confusionMatrix);
    // image level
    void testBasicClassifers2prob(PixelImage& testImage, std::vector<float>& treeTypeProbs, cv::Mat& confusionMatrix);

    //bow test
    void trainBow(std::vector<BasicImage>& trainImages);
    void testBow(std::vector<BasicImage>& testImages);

    /* parsing train & test (for segmentation)
     * activeMask if full
     */
    void parsingTrain(const std::vector<BasicImage>& trainImages);
    void parsingTest(const std::vector<BasicImage>& testImages);

public:
//    std::string _rTFileName;
//    std::string _rTObjFileName;
    alglib::decisionforest* _rTrees;
    alglib::decisionforest* _rTreesObj;
    FeatureExtractor _extractor;
};

#endif // CLASSIFIER_H
