#include "GraphNode.h"
#include <cstddef>

GraphNode::GraphNode()
    : _index(-1),
      _trueLabel(-1),
      _predictLabel(-1),
      _x(-1),
      _y(-1),
      _isActive(true),
      _withLabel(false),
      _activeIndex(-1)
{
}


void GraphNode::insertLayerlinks(GraphNode *T, const GraphNodeLink::WType setw)
{
    GraphNodeLink e;
    _layerlinks.push_back(e);
    int idx = _layerlinks.size()-1;
    _layerlinks.at(idx)._linkedNode  = T;
    _layerlinks.at(idx)._w = setw;
}

void GraphNode::insertUpLayerlinks(GraphNode *T, const GraphNodeLink::WType setw)
{
    GraphNodeLink e;
    _upLayerlinks.push_back(e);
    int idx = _upLayerlinks.size()-1;
    _upLayerlinks.at(idx)._linkedNode  = T;
    _upLayerlinks.at(idx)._w = setw;
}

void GraphNode::insertSuperLayerlinks(GraphNode *T, const GraphNodeLink::WType setw)
{
    GraphNodeLink e;
    _superLayerlinks.push_back(e);
    int idx = _superLayerlinks.size()-1;
    _superLayerlinks.at(idx)._linkedNode  = T;
    _superLayerlinks.at(idx)._w = setw;
}

void GraphNode::insertConstraintLayerlinks(GraphNode *T, const GraphNodeLink::WType setw)
{
    GraphNodeLink e;
    _constraintLayerlinks.push_back(e);
    int idx = _constraintLayerlinks.size()-1;
    _constraintLayerlinks.at(idx)._linkedNode  = T;
    _constraintLayerlinks.at(idx)._w = setw;
}

/////////////// GraphNodeLink ///////////////////
GraphNodeLink::GraphNodeLink()
    : _w(0.0),
      _linkedNode(NULL)
{
}
