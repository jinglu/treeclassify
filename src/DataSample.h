#ifndef DATASAMPLE_H
#define DATASAMPLE_H

#include <map>
#include "Feature.h"
typedef double EntropyType;

EntropyType prob2entropy(const EntropyType prob);

class DataSample {
public:
    DataSample();
    DataSample(const int sampleIdx);

    virtual ~DataSample();

    int getLabel() const;
    int getFeatDim() const;
    DataType getDataAt(const int idx) const;

    int getSampleIdx() const;
    void setSampleIdx(const int idx);

    void setFeatPt(FeatVector* featPt);

private:
    FeatVector *_ptFeat;
    int _sampleIdx;

    bool _isReleaseFeat; //whether the feat should be released in the destructor

};
///////////////////////////////////////////////
class DataSampleSet {
public:
    DataSampleSet();
    DataSampleSet(const int sampleDataLength, const int datasetType, const std::vector<int>& labelSet);
    DataSampleSet(const DataSampleSet* inputDataset); //copy

    virtual ~DataSampleSet();

    void setLabelMapping();

    void releaseSamples();

    int getNumSamples() const;
    int getNumLabels() const;

    int getLabelAt(const int idx) const;
    int getLabelIdx(const int label);

    int getDatasetType() const;

    DataSample* getSamplePtAt(const int idx) const;

    void insertSample(DataSample* sample);


    void searchActiveAttributes(std::vector<int>& activeAttributesIdx);
    void buildAttributesRange(const std::vector<int>& attributeIdxs,
                              std::vector<std::pair<DataType, DataType> >& featValueRanges) const;

    void randomSample( const float sampleRatio, DataSampleSet*& ptSelectSet) const;

    void estimatePurity(const bool update = false);

    EntropyType estimateEntropy(const bool updateDistribution);

    static int getSampleDataLength();
    static void setSampleDataLength(const int length);
public:
    std::vector<int> _labelSet;
    int _datasetType;

    std::vector<DataSample *> _ptSamples;

    EntropyType* _categoryDists;

private:
    int _sampleDataLength;
    std::vector<std::pair<DataType, DataType> > _featValueRanges;
    std::map<int, int>* _labelMapping;

    static int s_sampleDataLength;
};

#endif // DATASAMPLE_H
