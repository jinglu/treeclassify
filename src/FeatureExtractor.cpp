#include "FeatureExtractor.h"
#include <Common/StatisticAPI.h>
#include <Common/FileAPI.h>

#include "assert.h"
#include <iostream>
#include "GloabalVal.h"

extern "C"{
#include <vl/dsift.h>
#include <vl/sift.h>
}

#include <flann/flann.h>
#include <flann/algorithms/KMeansTree.h>
#include <flann/util/ResultSet.h>

using namespace std;
FeatureExtractor::FeatureExtractor()
{
}

void FeatureExtractor::extractImageLevelTrainFeature(const vector<FeatureParameter>& featParas,
                                                     const cv::Vec3b& activeColor,
                                                     const vector<BasicImage> &trainImages,
                                                     vector<FeatVector> &sampleFeatureSet)
{
    assert(trainImages.size() > 0);
    sampleFeatureSet.clear();
    sampleFeatureSet.reserve(trainImages.size());

    for (size_t i = 0; i < trainImages.size(); ++i)
    {
        FeatVector featVector;
        PixelImage trainImg(trainImages.at(i));
        trainImg.setAtivePart(activeColor);

        extractImageLevelFeature(featParas, trainImg, featVector);
        cout << (i+1)<<"/"<<trainImages.size()<<" image " << trainImages[i]._filenameImage << "\nShape feature " << featVector._data.size() << endl;
        sampleFeatureSet.push_back(featVector);
    }
}

void FeatureExtractor::adjustSampleStep(const vector<FeatureParameter> &featParas)
{
    for (size_t i = 0; i < featParas.size(); ++i)
    {
        if (featParas[i]._sampleStep < 4)
        {
            cout << featParas[i].strName() << " sample step for classifier training adjust from "
                 << featParas[i]._sampleStep << " to " << featParas[i]._patchSize / 2 << endl;
            featParas[i]._sampleStep = featParas[i]._patchSize / 2;
        }
    }
}

// extract pixel-level all training samples from all images
void FeatureExtractor::extractPixelLevelTrainFeature(const vector<FeatureParameter> &featParas,

                                                    const vector<BasicImage> &trainImages,
                                                    vector<FeatVector> &selectSampleFeatureSet,
                                                     const cv::Vec3b& activeColor)
{
    // when training, adjust the sample step
    adjustSampleStep(featParas);

    vector<FeatVector> sampleFeatureSet;
    for (size_t i = 0; i < trainImages.size(); ++i)
    {
        cout <<i+1<<"/"<<trainImages.size()<<" image " << trainImages[i]._filenameImage << endl;

        vector<FeatVector> features;
        PixelImage trainImg(trainImages.at(i));
        if (activeColor != cv::Vec3b(0,0,0))
            trainImg.setAtivePart(activeColor);

        extractPixelFeature(featParas, trainImg, features);

        for (size_t j = 0; j < features.size(); ++j)
        {
            if (features[j]._data.size() > 0)
            {
                assert(features[j]._label >= 0);
                sampleFeatureSet.push_back(features[j]);
                //cout << "texton data " << features[j]._data[1] << endl;
            }
        }

        cout << "Pixel all feature set size " << sampleFeatureSet.size() << ", dim " << sampleFeatureSet.at(sampleFeatureSet.size()-1)._data.size() << endl;
    }
    cout << "all sample num " << sampleFeatureSet.size() << ", dim " << sampleFeatureSet.at(sampleFeatureSet.size()-1)._data.size() << endl;
    randomSampleFeatures(sampleFeatureSet, myDataset._numMaxTrainSamples, selectSampleFeatureSet);
    sampleFeatureSet.clear();
    cout << "after sample, all sample num "<< selectSampleFeatureSet.size() << ", dim " << selectSampleFeatureSet.at(selectSampleFeatureSet.size()-1).getDim() << endl;
}

// randomly sample features
void FeatureExtractor::randomSampleFeatures(const vector<FeatVector> &featureSet, const int numSelectFeatures, vector<FeatVector> &selectFeatureSet)
{
    // random select features
    selectFeatureSet.clear();
    if (featureSet.size() > numSelectFeatures)
    {
        int numFeatures = featureSet.size();
        vector<int> randomSeq;
        StatisticAPI::generateRandomSequence(0, numFeatures-1, numSelectFeatures, randomSeq);
        //cout << "rand seq size " << randomSeq.size() << endl;
        for (size_t i = 0; i < randomSeq.size(); ++i)
        {
            int idx = randomSeq[i];
            //cout << " num " << numFeatures << ", select " << idx << endl;
            selectFeatureSet.push_back(featureSet[idx]);
        }
    }
    else
        selectFeatureSet = featureSet;
}

/////************************ Image level feature ******************************///////////

void FeatureExtractor::extractShapeMaskFeature(const PixelImage &pimg, FeatVector &feat)
{
    feat.clear();
    pimg.constructShapeMaskFeature(feat);
}

void FeatureExtractor::extractShapeMomentFeature(const PixelImage &pimg, FeatVector &feat)
{
    feat.clear();
    pimg.constructContourMoment(feat);
}

void FeatureExtractor::extractEdgeHistogram(const PixelImage &pimg, FeatVector &feat)
{
    feat.clear();
    pimg.constructEdgeHist(feat);
}

void FeatureExtractor::extractImageLevelFeature(const std::vector<FeatureParameter> &featParas, const PixelImage &pimg, FeatVector &feat)
{
    feat.setLabel(pimg._treeId);

    for (size_t i = 0; i < featParas.size(); ++i)
    {
        FeatVector featVec1;
        if (featParas[i]._featName == FeatureParameter::SHAPEMASK)
            extractShapeMaskFeature(pimg, featVec1);
        else if (featParas[i]._featName == FeatureParameter::SHAPEMOMENT)
            extractShapeMomentFeature(pimg, featVec1);
        else if (featParas[i]._featName == FeatureParameter::EDGEHIST)
            extractEdgeHistogram(pimg, featVec1);

        feat._data.insert(feat._data.end(), featVec1._data.begin(), featVec1._data.end());
        cout << "extract feature " <<featParas[i].strName()<<" , dim = "<< feat._data.size() << ", label " << feat._label << endl;
    }

}

///////// *************** pixel level *******************//////////////
void FeatureExtractor::initializePixelFeature(const PixelImage &pimg, vector<FeatVector> &features)
{
    features.clear();
    features = vector<FeatVector>(pimg._image.rows * pimg._image.cols, FeatVector());
    for (int y = 0; y < pimg._image.rows; ++y)
        for (int x = 0; x < pimg._image.cols; ++x)
        {
            int idx = y * pimg._image.cols + x;
            features[idx].setPos(x, y);

        }
    ColorCategoryTable* colorTable;
    if (myDataset._operation == MyDataset::TREESEGEMNTATION)
        colorTable = &(myDataset._parsColorTable);
    else if (myDataset._operation == MyDataset::TREESPECIESCLASSIFICATION)
        colorTable = &(myDataset._speciesColorTable);
//    bool is_parsing = false;
//    if (colorTabel._categoryType == ColorCategoryTable::SEGMENTATION)
//        is_parsing = true;
    assignPixelFeatureLabel(pimg,
                            *colorTable,
                            features);
}

void FeatureExtractor::assignPixelFeatureLabel(const PixelImage &pimg,
                                               const ColorCategoryTable& colorTable,
                                               vector<FeatVector> &features)
{
    assert(features.size() == pimg._image.cols * pimg._image.rows);
    bool isParsing = false;
    if (colorTable._categoryType == ColorCategoryTable::SEGMENTATION)
        isParsing = true;
    //string name = myDataset._rootDir+"/test/"+FileAPI::extractFileName(pimg._filenameMask, true);
    //cout << "assign img " << pimg._filenameImage << endl;
    //cout << "label or " << pimg._filenameMask << endl;
    //cout << "assign label " << name << endl;
    //cv::imwrite(name, pimg._labelMap);

    if (isParsing)
    {
        for (int y = 0, idx = 0; y < pimg._image.rows; ++y)
            for (int x = 0; x < pimg._image.cols; ++x, ++idx)
            {
                cv::Vec3b color = pimg._labelMap.at<cv::Vec3b>(y,x);
                int label = colorTable.color2label(color);
                //cout << "assgin " << x << ", " << y << ": rgb("<<color[2]<<","<<color[1]<<","<<color[0]<<")  " << label << endl;
                features[idx].setLabel(label);
            }
    }
    else
    {
        for (size_t i = 0; i < features.size(); ++i)
            features[i].setLabel(pimg._treeId);
    }
}

// extract pixel features in one image, according to feature parameter (main extract function)
// each pixel has a featVector (but may be empty one)
// so have (m * n) featVectors
void FeatureExtractor::extractPixelFeature(const vector<FeatureParameter> &featParas,
                                           const PixelImage &pimg,
                                           vector<FeatVector> &features)
{

    initializePixelFeature(pimg, features);

    int sumFeatDim = 0;
    for(size_t i = 0; i < featParas.size(); ++i)
    {
        vector<FeatVector> tempFeatures;
        int tempDim;



        //initializePixelFeature(pimg._image, tempFeatures);
        if (featParas[i]._featName == FeatureParameter::TEXTON)
        {
            extractTexton(featParas[i], pimg, tempFeatures, tempDim);
        }
        else if (featParas[i]._featName == FeatureParameter::HOG)
        {
            extractHOG(featParas[i], pimg, tempFeatures, tempDim);
        }
        else if (featParas[i]._featName == FeatureParameter::SIFT)
        {
            extractSIFT(featParas[i], pimg, tempFeatures, tempDim);
        }
        else if (featParas[i]._featName == FeatureParameter::COLOR)
            extractCOLOR(featParas[i], pimg, tempFeatures, tempDim);

        sumFeatDim += tempDim;
        cout << "extract pixel featType " << featParas[i].strName() << ", num " << tempFeatures.size() << ", dim " << tempDim << endl;
        // add each type feature to the features

        for (int y = 0; y < pimg._image.rows; ++y)
            for (int x = 0; x < pimg._image.cols; ++x)
            {
                int idx = y * pimg._image.cols + x;

                // some feature type doesn't cover all pixels
                if (pimg._activeMask.at<uchar>(y,x) > 0 && sumFeatDim == features[idx].getDim() + tempFeatures[idx].getDim())
                {
                    //collect features

                    features[idx]._data.insert(features[idx]._data.end(),
                                               tempFeatures[idx]._data.begin(),
                                               tempFeatures[idx]._data.end());

                }
                else
                    features[idx].clear();
            }
    }

//    for (size_t i = 0; i < features.size(); ++i)
//    {
//        features[i].setLabel(pimg._treeId);
//    }
}


// in one image
void FeatureExtractor::extractPixelFeatureScreened(const vector<FeatureParameter> &featParas, const PixelImage &pimg, vector<FeatVector> &features)
{
    // when training, adjust the sample step
    if (pimg._isTrain)
    {
    for (size_t i = 0; i < featParas.size(); ++i)
    {
        if (featParas[i]._sampleStep < 4)
        {
            cout << featParas[i].strName() << " sample step for classifier training adjust from "
                 << featParas[i]._sampleStep << " to " << featParas[i]._patchSize / 2 << endl;
            featParas[i]._sampleStep = featParas[i]._patchSize / 2;
        }
    }
    }

    features.clear();
    vector<FeatVector> cand_features;
    this->extractPixelFeature(featParas, pimg, cand_features);

    for (size_t i = 0; i < cand_features.size(); ++i)
    {
        if (cand_features[i].getDim() > 0)
        {
            features.push_back(cand_features[i]);
        }
    }
}

void FeatureExtractor::extractTextonSample(const int sampleStep, const PixelImage &pimg, vector<FeatVector> &features)
{
    initializePixelFeature(pimg, features);

    int texton_dim = 17;

    vector<cv::Mat> responses;

    for (int i = 0; i < texton_dim; ++i)
    {
        cv::Mat responseImg(pimg._image.rows, pimg._image.cols, CV_32F);
        responses.push_back(responseImg);
    }

    TextonFeature::preprocessImage(pimg._image, 1.0, responses);
    //cout << "response size " << responses.size() << endl;
    //cout << "sample extraction features size " << features.size() << endl;
    int height = pimg._image.rows;
    int width = pimg._image.cols;
    for (int i = 0; i < height; i += sampleStep)
        for (int j = 0; j < width; j += sampleStep)
        {
            if (pimg._activeMask.at<uchar>(i,j) > 0)
            {
                int idx = i * width + j;
                //cout << "extract " << i << ", " << j << " texton " << endl;
                FeatVector& textonFeature = features.at(idx);
                for (int k = 0; k < texton_dim; ++k)
                    textonFeature._data.push_back(responses[k].at<float>(i,j));
            }
        }
}

void FeatureExtractor::extractTexton(const FeatureParameter &featPara, const PixelImage &pimg, vector<FeatVector>& features, int &dim)
{
    if (featPara._numScales == 1)
        extractTextonSample(featPara._sampleStep, pimg, features);
    else if (featPara._numScales > 1)
    {
        extractTextonSample(featPara._sampleStep, pimg, features);

        for (int k = 2, scaleFactor = 2; k <= featPara._numScales; ++k, scaleFactor *= 2)
        {
            vector<FeatVector> scaledFeatures;
            int swidth = pimg._image.cols / scaleFactor;
            int sheight = pimg._image.rows / scaleFactor;

            PixelImage scaledImage(pimg);
            scaledImage._image = cv::Mat(sheight, swidth, CV_8UC3);
            scaledImage._activeMask = cv::Mat(sheight, swidth, CV_8UC1);
            //cv::resize(pimg._image, scaledImage._image, cv::Size(swidth,sheight));
            //cv::resize(pimg._ativeMask, scaledImage._ativeMask, cv::Size(swidth,sheight));
            pimg.resizeTo(swidth, sheight, scaledImage);

            extractTextonSample(1, scaledImage, scaledFeatures);

            for (int i = 0; i < pimg._image.rows; ++i)
                for (int j = 0; j < pimg._image.cols; ++j)
                {
                    int y = std::min(i / scaleFactor, sheight - 1);
                    int x = std::min(j / scaleFactor, swidth - 1);

                    int idx1 = i * pimg._image.cols + j;
                    int idx2 = y * swidth + x;
                    if (features[idx1]._data.size() == (k-1) * scaledFeatures[idx2]._data.size())
                    {
                        features[idx1]._data.insert(features[idx1]._data.end(), scaledFeatures[idx2]._data.begin(), scaledFeatures[idx2]._data.end());
                    }
                    else
                        features[idx1].clear();
                }
        }
    }

    //get dim
    for (size_t i = 0; i < features.size(); ++i)
    {
        if (features[i].getDim() > 0)
        {
            dim = features[i].getDim();
            break;
        }
    }
}

void FeatureExtractor::extractHOG(const FeatureParameter &featPara, const PixelImage &pimg, vector<FeatVector> &features, int &dim)
{
    if (featPara._numScales == 1)
        extractHOGSample(featPara._sampleStep, featPara._patchSize, pimg, features);
    else if (featPara._numScales > 1)
    {
        extractHOGSample(featPara._sampleStep, featPara._patchSize, pimg, features);

        for (int k = 2, scaleFactor = 2; k <= featPara._numScales; ++k, scaleFactor *= 2)
        {
            vector<FeatVector> scaledFeatures;
            int swidth = pimg._image.cols / scaleFactor;
            int sheight = pimg._image.rows / scaleFactor;

            PixelImage scaledImage;
            //scaledImage._image = cv::Mat(sheight, swidth, CV_8UC3);
            //scaledImage._activeMask = cv::Mat(sheight, swidth, CV_8UC1);
            //cv::resize(pimg._image, scaledImage._image, cv::Size(swidth,sheight));
            //cv::resize(pimg._ativeMask, scaledImage._ativeMask, cv::Size(swidth,sheight));
            pimg.resizeTo(swidth, sheight, scaledImage);

            extractHOGSample(1, featPara._patchSize, scaledImage, scaledFeatures);

            for (int i = 0; i < pimg._image.rows; ++i)
                for (int j = 0; j < pimg._image.cols; ++j)
                {
                    int y = std::min(i / scaleFactor, sheight - 1);
                    int x = std::min(j / scaleFactor, swidth - 1);

                    int idx1 = i * pimg._image.cols + j;
                    int idx2 = y * swidth + x;
                    if (features[idx1]._data.size() == (k-1) * scaledFeatures[idx2]._data.size())
                    {
                        features[idx1]._data.insert(features[idx1]._data.end(), scaledFeatures[idx2]._data.begin(), scaledFeatures[idx2]._data.end());
                    }
                    else
                        features[idx1].clear();
                }
        }
    }

    //get dim
    for (size_t i = 0; i < features.size(); ++i)
    {
        if (features[i].getDim() > 0)
        {
            dim = features[i].getDim();
            break;
        }
    }
}

void FeatureExtractor::extractHOGSample(const int smapleStep, const int patchSize, const PixelImage &pimg, vector<FeatVector> &features)
{
    initializePixelFeature(pimg, features);

    const double eps = 0.0001;
    // unit vectors used to compute gradient orientation
    double uu[9] = {1.0000,0.9397,0.7660,
                    0.500,0.1736,-0.1736,
                   -0.5000,-0.7660,-0.9397};
    double vv[9] = {0.0000,0.3420,0.6428,
                    0.8660,0.9848,0.9848,
                    0.8660,0.6428,0.3420};

    int dims[2];
    dims[0] = pimg._image.rows;
    dims[1] = pimg._image.cols;

    int sbin = floor(patchSize/2);
    double *kernel =  new double[sbin*sbin];

    // bilinear interpolation kernel (ideally, it should be gaussian kernel)
    for (int x=0;x<sbin;x++)
    {
        for (int y=0;y<sbin;y++)
        {
          //kernel[x][y]
          *(kernel + x*sbin + y) = double((sbin-x)*(sbin-y))/(sbin*sbin);
        }
    }

    // memory for caching orientation histograms & their norms
    int blocks[2];
    blocks[0]    = dims[0]-2*sbin;
    blocks[1]    = dims[1]-2*sbin;
    double *hist = new double[blocks[0]*blocks[1]*18];

    double *norm = new double[blocks[0]*blocks[1]];
    for(unsigned int i=0;i<blocks[0]*blocks[1]*18;i++)
    {
        hist[i] = 0;
    }
    for(unsigned int i=0;i<blocks[0]*blocks[1];i++)
    {
        norm[i] = 0;
    }

    // memory for HOG features
    int out[3];
    out[0] = blocks[0]-2*sbin;
    out[1] = blocks[1]-2*sbin;
    out[2] = 27+4;

    double* im = new double[dims[0]*dims[1]*3];
    for(int x = 0;x<dims[1];x++)
    {
        for(int y=0;y<dims[0];y++)
        {
            for (int k = 0; k < 3; ++k)
            {
                im[x*dims[0]+y + k * dims[0]*dims[1]] = pimg._image.at<cv::Vec3b>(y,x)[k];
            }

        }
    }

    for (int x = 1; x < dims[1]-1; x++)
    {
        for (int y = 1; y < dims[0]-1; y++)
        {
            // first color channel
            double *s = im + x*dims[0] + y;
            double dy = *(s+1) - *(s-1);
            double dx = *(s+dims[0]) - *(s-dims[0]);
            double v  = dx*dx + dy*dy;

            // second color channel
            s += dims[0]*dims[1];
            double dy2 = *(s+1) - *(s-1);
            double dx2 = *(s+dims[0]) - *(s-dims[0]);
            double v2 = dx2*dx2 + dy2*dy2;

            // third color channel
            s += dims[0]*dims[1];
            double dy3 = *(s+1) - *(s-1);
            double dx3 = *(s+dims[0]) - *(s-dims[0]);
            double v3 = dx3*dx3 + dy3*dy3;

            // pick channel with strongest gradient
            if (v2 > v)
            {
                v = v2;
                dx = dx2;
                dy = dy2;
            }
            if (v3 > v)
            {
                v = v3;
                dx = dx3;
                dy = dy3;
            }

            // snap to one of 18 orientations
            double best_dot = 0;
            int best_o = 17;
            for (int o = 0; o < 9; o++)
            {
                double dot = uu[o]*dx + vv[o]*dy;
                if (dot > best_dot)
                {
                    best_dot = dot;
                    best_o = o;
                }
                else if (-dot > best_dot)
                {
                    best_dot = -dot;
                    best_o = o+9;
                }
            }

            // add to surrounding bins using weights from the precomputed kernels
            v = sqrt(v);
            for (int dx = -(sbin-1); dx <= (sbin-1); dx ++)
            {
              int xdx = x + dx;
              if (sbin <= xdx && xdx < dims[1]-sbin )
              {
                int adx = abs(dx);
                xdx = xdx - sbin;
                for (int dy = -(sbin-1); dy <= (sbin-1); dy ++)
                {
                  int ydy = y + dy;
                  if (sbin <= ydy && ydy < dims[0]-sbin )
                  {
                    int ady = abs(dy);
                    ydy = ydy - sbin;
                    *(hist + xdx*blocks[0] + ydy + best_o*blocks[0]*blocks[1]) += v * (*(kernel + adx*sbin + ady));
                  }
                }
              }
            }
        }
    }

    // compute energy in each block by summing over orientations
    for (int o = 0; o < 9; o++)
    {
        double *src1 = hist + o*blocks[0]*blocks[1];
        double *src2 = hist + (o+9)*blocks[0]*blocks[1];
        double *dst  = norm;
        double *end  = norm + blocks[1]*blocks[0];

        while (dst < end)
        {
          *(dst++) += (*src1 + *src2) * (*src1 + *src2);
          src1++;
          src2++;
        }
    }

    int sbin_blocks0 = sbin*blocks[0];
    int sbin_blocks0_sbin = sbin*blocks[0]+sbin;

    vector< vector< vector<float> > > HOGFeat;
    for (int x = 0; x < out[1]; x++)
    {
        vector< vector<float> > rowFeat;
        HOGFeat.push_back(rowFeat);
        for (int y = 0; y < out[0]; y++)
        {
            vector<float> feat;
            HOGFeat.at(x).push_back(feat);
        }
    }

    int bw = floor(sbin * 2.5);
    for (int x = 0; x < out[1]; x++)
    {
        for (int y = 0; y < out[0]; y++)
        {
            double *src, *p, n1, n2, n3, n4;
            if(y < out[0] - sbin && x < out[1] - sbin)
            {
                bool insertFeature = true;
                //if(LabelImage != NULL)
                {
                    //if(features.at((y+bw)*pimg._image.cols+x+bw)._label < 0)
                    if (pimg._activeMask.at<uchar>(y+bw, x+bw) <= 0)
                    {
                        insertFeature = false;
                    }
                }

                if(insertFeature == true)
                {
                    //vector<float>* ptFeat = features.at((y+bw)*Image->width+x+bw).GetDataPtr();
                    //features.at((y+bw)*Image->width+x+bw).SetExtraData(x + bw,y + bw);
                    vector<float>* ptFeat = features.at((y+bw)*pimg._image.cols+x+bw).getDataPtr();
                    features.at((y+bw)*pimg._image.cols+x+bw).setPos(x + bw, y + bw);

                    p = norm + x*blocks[0] + y;
                    n4 = 1.0 / sqrt(*p + *(p+sbin) + *(p+sbin_blocks0) + *(p+sbin_blocks0_sbin) + eps);

                    p = p + sbin;
                    n3 = 1.0 / sqrt(*p + *(p+sbin) + *(p+sbin_blocks0) + *(p+sbin_blocks0_sbin) + eps);

                    p = p + sbin*blocks[0];
                    n1 = 1.0 / sqrt(*p + *(p+sbin) + *(p+sbin_blocks0) + *(p+sbin_blocks0_sbin) + eps);

                    p = p - sbin;
                    n2 = 1.0 / sqrt(*p + *(p+sbin) + *(p+sbin_blocks0) + *(p+sbin_blocks0_sbin) + eps);

                    double t1 = 0;
                    double t2 = 0;
                    double t3 = 0;
                    double t4 = 0;

                    // contrast-sensitive features
                    src = hist + (x+sbin)*blocks[0] + (y+sbin);
                    for (int o = 0; o < 18; o++)
                    {
                        double h1 = min(*src * n1, 0.2);
                        double h2 = min(*src * n2, 0.2);
                        double h3 = min(*src * n3, 0.2);
                        double h4 = min(*src * n4, 0.2);
                        double v = 0.5 * (h1 + h2 + h3 + h4);

                        ptFeat->push_back(v);

                        t1 += h1;
                        t2 += h2;
                        t3 += h3;
                        t4 += h4;
                        src += blocks[0]*blocks[1];
                    }

                    // contrast-insensitive features
                    src = hist + (x+sbin)*blocks[0] + (y+sbin);
                    for (int o = 0; o < 9; o++)
                    {
                      double sum = *src + *(src + 9*blocks[0]*blocks[1]);
                      double h1 = min(sum * n1, 0.2);
                      double h2 = min(sum * n2, 0.2);
                      double h3 = min(sum * n3, 0.2);
                      double h4 = min(sum * n4, 0.2);
                      double v = 0.5 * (h1 + h2 + h3 + h4);

                      ptFeat->push_back(v);

                      src += blocks[0]*blocks[1];
                    }

                    // texture features
                    ptFeat->push_back(0.2357 * t1);
                    ptFeat->push_back(0.2357 * t2);
                    ptFeat->push_back(0.2357 * t3);
                    ptFeat->push_back(0.2357 * t4);
                }
            }
        }
    }

    delete[] im;
    delete[] hist;
    delete[] norm;
    delete[] kernel;
}

void FeatureExtractor::extractSIFT(const FeatureParameter &featPara, const PixelImage &pimg, vector<FeatVector> &features, int &dim)
{
    if (featPara._numScales == 1)
        extractSIFTSample(featPara._sampleStep, pimg, features);
    else if (featPara._numScales > 1)
    {
        extractSIFTSample(featPara._sampleStep, pimg, features);

        for (int k = 2, scaleFactor = 2; k <= featPara._numScales; ++k, scaleFactor *= 2)
        {
            vector<FeatVector> scaledFeatures;
            int swidth = pimg._image.cols / scaleFactor;
            int sheight = pimg._image.rows / scaleFactor;

            PixelImage scaledImage;
            //scaledImage._image = cv::Mat(sheight, swidth, CV_8UC3);
            //scaledImage._activeMask = cv::Mat(sheight, swidth, CV_8UC1);
            //cv::resize(pimg._image, scaledImage._image, cv::Size(swidth,sheight));
            //cv::resize(pimg._ativeMask, scaledImage._ativeMask, cv::Size(swidth,sheight));
            pimg.resizeTo(swidth, sheight, scaledImage);

            extractSIFTSample(1, scaledImage, scaledFeatures);

            for (int i = 0; i < pimg._image.rows; ++i)
                for (int j = 0; j < pimg._image.cols; ++j)
                {
                    int y = std::min(i / scaleFactor, sheight - 1);
                    int x = std::min(j / scaleFactor, swidth - 1);

                    int idx1 = i * pimg._image.cols + j;
                    int idx2 = y * swidth + x;
                    if (features[idx1]._data.size() == (k-1) * scaledFeatures[idx2]._data.size())
                    {
                        features[idx1]._data.insert(features[idx1]._data.end(), scaledFeatures[idx2]._data.begin(), scaledFeatures[idx2]._data.end());
                    }
                    else
                        features[idx1].clear();
                }
        }
    }

    //get dim
    for (size_t i = 0; i < features.size(); ++i)
    {
        if (features[i].getDim() > 0)
        {
            dim = features[i].getDim();
            break;
        }
    }

}

void FeatureExtractor::extractSIFTSample(const int sampleStep, const PixelImage &pimg, vector<FeatVector> &features)
{
    initializePixelFeature(pimg, features);

    int width = pimg._image.cols;
    int height = pimg._image.rows;

    cv::Mat smoothImage(height, width, pimg._image.type());
    cv::GaussianBlur(pimg._image, smoothImage, cv::Size(5,5), 0);

    cv::Mat grayImage(height, width, CV_8UC1);
    cv::cvtColor(smoothImage, grayImage, CV_BGR2GRAY);

    float *imagedata = new float[width * height];

    for (int i = 0; i < height; ++i)
        for (int j = 0; j < width; ++j)
            imagedata[i * width + j] = float(grayImage.at<uchar>(i,j));

    int patchSize = 8;
    VlDsiftFilter* siftFilter = vl_dsift_new_basic(width,height,sampleStep,patchSize);
    vl_dsift_process(siftFilter,imagedata);
    VlDsiftKeypoint* ptkeypoint = (VlDsiftKeypoint*)vl_dsift_get_keypoints(siftFilter);

    float* descriptor = (float*) vl_dsift_get_descriptors(siftFilter);
    int numpoints = vl_dsift_get_keypoint_num(siftFilter);
    int dim = vl_dsift_get_descriptor_size(siftFilter);

    for (int i = 0; i < numpoints; ++i)
    {
        FeatVector &siftFeat = features[ptkeypoint->y * width + ptkeypoint->x];

        assert(siftFeat._x == ptkeypoint->x);
        assert(siftFeat._y == ptkeypoint->y);

        for (int j = 0; j < dim; ++j)
        {
            float v = descriptor[i * dim + j];
            siftFeat._data.push_back(v);
        }
        ptkeypoint++;
    }

    delete [] imagedata;
    vl_dsift_delete(siftFilter);
}

void FeatureExtractor::extractSparseSIFT(const FeatureParameter &featPara, const PixelImage &pimg, vector<FeatVector> &features, int &dim)
{
    initializePixelFeature(pimg, features);
    int width = pimg.getWidth();
    int height = pimg.getHeight();

    cv::Mat smoothImage(height, width, pimg._image.type());
    cv::GaussianBlur(pimg._image, smoothImage, cv::Size(5,5), 0);

    cv::Mat grayImage(height, width, CV_8UC1);
    cv::cvtColor(smoothImage, grayImage, CV_BGR2GRAY);

    int noctaves = log2(min(pimg.getWidth(), pimg.getHeight()));
    int nlevels = 3;
    int o_min = -1;


    VlSiftFilt* sift = vl_sift_new(width, height, noctaves, nlevels, o_min);

    vector<float> data(height * width);

    for (int i = 0, idx = 0; i < height; ++i)
        for (int j = 0; j < width; ++j)
            data[idx++] = float(grayImage.at<uchar>(i, j));

    vl_sift_process_first_octave(sift, &(data[0]));

    for (int i = o_min; i < o_min + noctaves; ++i)
    {
        vl_sift_process_next_octave(sift);
        vl_sift_delete(sift);

        const VlSiftKeypoint* keypoint = vl_sift_get_keypoints(sift);
        int numpts = vl_sift_get_nkeypoints(sift);

        double angles[4];

        for (int k = 0; i < numpts; ++k)
        {
            int nangles = vl_sift_calc_keypoint_orientations(sift, angles, keypoint);
            for (int j = 0; j < j < nangles; ++j)
            {
                float desc[128];

                vl_sift_calc_keypoint_descriptor(sift, desc, keypoint, angles[j]);

                FeatVector& sift_feat = features.at(keypoint->y * width + keypoint->x);

                if (pimg._activeMask.at<uchar>(keypoint->y, keypoint->x) > 0)
                {
                    for (int d = 0; d < 128; ++d)
                        sift_feat._data.push_back(desc[d]);
                }
            }
            keypoint++;
        }
    }

    vl_sift_delete(sift);
}

void FeatureExtractor::extractCOLOR(const FeatureParameter &featPara,
                                    const PixelImage &pimg,
                                    vector<FeatVector> &features,
                                    int &dim)
{
    initializePixelFeature(pimg, features);
    int width = pimg.getWidth();
    int height = pimg.getHeight();

    cv::Mat img_lab;
    pimg._image.convertTo(img_lab, CV_32F, 1.0/255.0);
    cout << "lab " << img_lab.rows << "," << img_lab.cols << endl;
    cv::cvtColor(img_lab, img_lab, CV_BGR2Lab);

    cv::Mat img_hsv(height, width, CV_32FC3);
    pimg._image.convertTo(img_hsv, CV_32FC3, 1.0/255.0);
    cv::cvtColor(img_hsv, img_hsv, CV_BGR2HSV);

    for (int i = 0, idx = 0; i < height; i += featPara._sampleStep)
        for (int j = 0; j < width; j += featPara._sampleStep, ++idx)
        {
            //if (pimg._mask.at<uchar>(i,j) > 0)
            {
                FeatVector& colorFeat = features[idx];

//                for (int k = 0; k < 3; ++k)
//                    colorFeat._data.push_back(float(pimg._image.at<cv::Vec3b>(i,j)[k]) / 255.0);
//                for (int k = 0; k < 3; ++k)
//                    colorFeat._data.push_back(img_lab.at<cv::Vec3f>(i,j)[k]);
                for (int k = 0; k < 3; ++k)
                    colorFeat._data.push_back(img_hsv.at<cv::Vec3f>(i,j)[k]);
            }
        }

    //get dim
    for (size_t i = 0; i < features.size(); ++i)
    {
        if (features[i].getDim() > 0)
        {
            dim = features[i].getDim();
            break;
        }
    }
}

void FeatureExtractor::analysisPCA(const vector<FeatVector> &features, const PixelImage& pimg)
{
    int featDim = 0;
    int featNum = 0;

    vector<int> featIdxSet;

    for (size_t i = 0; i <features.size(); ++i)
    {
        const FeatVector* ptFeat = &features[i];
        if (ptFeat->getDim() > 0)
        {
            featDim = ptFeat->getDim();
            featIdxSet.push_back(i);
        }
    }

    featNum = featIdxSet.size();
    if (featNum == 0)
    {
        cout << "No input feature " << endl;
        return;
    }

    cv::Mat data(featNum, featDim, CV_32FC1);
    cv::Mat mean;

    for (size_t i = 0; i < featIdxSet.size(); ++i)
    {
        const FeatVector* ptFeat = &features[featIdxSet[i]];

        if (ptFeat->getDim() != featDim)
            cout << ptFeat->getDim() << ", " << featDim << endl;
        assert(ptFeat->getDim() == featDim);

        for (int k = 0; k < featDim; ++k)
            data.at<float>(i, k) = ptFeat->_data[k];
    }

    int subSpaceDim = 3;
    cv::PCA dataPCA(data, mean, CV_PCA_DATA_AS_ROW, subSpaceDim);

    cv::Mat projectedData(featNum, subSpaceDim, CV_32FC1);
    dataPCA.project(data, projectedData);

    vector<float> maxs(subSpaceDim, -1e6);
    vector<float> mins(subSpaceDim, 1e6);

    cv::Mat colorProjectedData(featNum, subSpaceDim, CV_32FC1);

    for(int i=0;i<featNum;i++)
    {
        float v0 = projectedData.at<float>(i,0);
        float v1 = projectedData.at<float>(i,1);
        float v2 = projectedData.at<float>(i,2);
        // R G B
        colorProjectedData.at<float>(i,0) = (2*v0+3*v1+2*v2)/6.0;
        colorProjectedData.at<float>(i,1) = (2*v0-3*v1+2*v2)/6.0;
        colorProjectedData.at<float>(i,2) = (v0-2*v2)/6.0;

        for(int j=0;j<subSpaceDim;j++)
        {
            if(maxs[j]<colorProjectedData.at<float>(i,j))
                maxs[j] = colorProjectedData.at<float>(i,j);
            if(mins[j]>colorProjectedData.at<float>(i,j))
                mins[j] = colorProjectedData.at<float>(i,j);
        }
    }

    for(int i=0;i<featNum;i++)
    {
        for(int j=0;j<subSpaceDim;j++)
        {
            colorProjectedData.at<float>(i,j) = (colorProjectedData.at<float>(i,j)-mins[j])/(maxs[j]-mins[j])*254;
        }
    }

    // visualize PCA
    cv::Mat featurePCAMap = cv::Mat::zeros(pimg._image.rows, pimg._image.cols, CV_8UC3);

    for (int i = 0; i < featNum; ++i)
    {
        const FeatVector* ptFeat = &features[featIdxSet[i]];

        int r = colorProjectedData.at<float>(i,0);
        int g = colorProjectedData.at<float>(i,1);
        int b = colorProjectedData.at<float>(i,2);
        int x = ptFeat->_x;
        int y = ptFeat->_y;
        featurePCAMap.at<cv::Vec3b>(y,x)[0] = b;
        featurePCAMap.at<cv::Vec3b>(y,x)[1] = g;
        featurePCAMap.at<cv::Vec3b>(y,x)[2] = r;
    }

    string pcaName = myDataset._featurePCADir + FileAPI::extractFileName(pimg._filenameImage, false) + "_PCA.png";
    cv::imwrite(pcaName, featurePCAMap);
    cout << "save feature PCA map " << pcaName << endl;
}

////////////////// ********* Texture featue **********************////////////////

int TextonFeature::kernelSize(const float sigma)
{
    return (2*(int)(sigma * 3) + 1);
}



void TextonFeature::textonfilter(const cv::Mat& img, vector<cv::Mat>& responses)
{
    const int NUM_FILTERS = 17;
    float kappa = 1.0;

//    if (responses.empty())
//        responses.resize(NUM_FILTERS);
//    for (int i = 0; i < NUM_FILTERS; ++i)
//    {
//        responses[i] = cv::Mat(img.rows, img.cols, CV_32FC1);
//    }


    cv::Mat imgCIELab(img.rows, img.cols, CV_32FC3);
    //cv::convertScaleAbs(img, imgCIELab, 1.0 / 255.0, 0.0);
    //cvConvertScale(&imgCIELab, &imgCIELab, 1.0/255.0);
    //cv::cvtColor(imgCIELab, imgCIELab, CV_BGR2Lab);
    img.convertTo(imgCIELab, CV_32FC3, 1.0/255.0);

    //cv::Mat grayImg(img.size, CV_32FC1);

    vector<cv::Mat> channels(3);
    cv::split(img, channels);

    cv::Mat& grayImg = channels[0];
    //cv::Mat& imgCIELab_C0 = channels[0];

    int k = 0;
    // gaussian filter on all color channels
    cv::Mat gImg32f(img.rows, img.cols, CV_32FC3);
    for (float sigma = 1.0; sigma <= 4.0; sigma *= 2.0)
    {
        int aperturewidth = 2 * (int)(kappa * sigma) + 1;
        cv::GaussianBlur(imgCIELab, gImg32f, cv::Size(aperturewidth,aperturewidth), 0);
        //cv::imwrite("test.png", gImg32f);
        vector<cv::Mat> gchannels(3);
        cv::split(gImg32f, gchannels);
        for (int c = 0; c < 3; ++c)
        {
            responses[k] = gchannels[c];

//            cout << "k = " << k << ", response " << responses[k].cols << ", " <<responses[k].rows << endl;
//            cout << "c = " << c << ", channel " << gchannels[c].cols << ", " << gchannels[c].rows << endl;
            k++;
        }
    }
    // derivatives of gaussians on just greyscale image
    for (float sigma = 2.0; sigma <= 4.0; sigma *= 2.0)
    {
        // x-direction
        cv::Sobel(grayImg, responses[k], CV_32F, 1, 0);
        int aperturewidth = 2 * (int)(kappa * sigma) + 1;
        int aperturewidth1 = 2 * (int)(3.0 * kappa * sigma) + 1;
        cv::GaussianBlur(responses[k], responses[k], cv::Size(aperturewidth, aperturewidth1), 0) ;
        k++;

        // y-direction
        cv::Sobel(grayImg, responses[k], CV_32F, 0, 1);

        cv::GaussianBlur(responses[k], responses[k], cv::Size(aperturewidth1, aperturewidth), 0);
        k++;
    }
    // laplacian of gaussian on just greyscale image
    cv::Mat tmpImg(grayImg.rows, grayImg.cols, CV_32FC1);
    for (float sigma = 1.0; sigma <= 8.0; sigma *= 2.0)
    {
        int aperturewidth = 2 * (int)(kappa * sigma) + 1;
        cv::GaussianBlur(grayImg, tmpImg, cv::Size(aperturewidth, aperturewidth), 0);
        cv::Laplacian(tmpImg, responses[k++], CV_32F);
    }
//    for (size_t i = 0; i < responses.size(); ++i)
//        cout << "fileter response " << i << ", size " << responses[i].cols << ", " << responses[i].rows << endl;
}

void TextonFeature::preprocessImage(const cv::Mat& image, const float scale, vector<cv::Mat> &responses)
{
    int n = TextonFeature::kernelSize(scale);
    cv::Mat image1 = image.clone();
    cv::GaussianBlur(image1, image1, cv::Size(n, n), scale);
    cv::resize(image1, image1, cv::Size(image1.cols / scale, image1.rows / scale));
    TextonFeature::textonfilter(image1, responses);

//    for (size_t i = 0; i < responses.size(); ++i)
//        cout << "response " << i << ", size " << responses[i].cols << ", " << responses[i].rows << endl;
}

////////////////************************* BOW ********************************////////////////////////

void FeatureExtractor::buildBowIndex(vector<DataType>& data,
                                     const int num_features,
                                     const int feat_dim,
                                     const string treename)
{
    ofstream out_tree(treename.c_str());

    FLANNParameters flannpara;
    flannpara.log_level = LOG_INFO;
    flannpara.log_destination = NULL;
    flannpara.algorithm = KMEANS;
    flannpara.checks = 32;
    flannpara.branching = 8;
    flannpara.iterations = 10;  //  max iterantions
    flannpara.target_precision = -1;
    flannpara.centers_init = CENTERS_KMEANSPP;
    int cluster_num = 100;  //  objective cluster num

    Params kmean_para;

    parametersToParams(flannpara, kmean_para);

    Dataset<DataType> kmean_data(num_features, feat_dim, &(data[0]));

    KMeansTree kmean_tree(kmean_data, kmean_para);
    kmean_tree.buildIndex();

    int actual_cluster_num = kmean_tree.WriteOnlyClusterToFile(out_tree,cluster_num );
    out_tree.close();
    cout << "end write " << treename
         << "\n actual cluster num " << actual_cluster_num
         << "\n object cluster num " << cluster_num
         << "\n tree node num " << kmean_tree.GetNodeNum()<< endl;


}

void FeatureExtractor::calculateBowWeight(const string treename)
{
//    ifstream in_tree(treename.c_str());
//    KMeansTree kmeans_tree(in_tree);
//    cout << "Load kmeans tree from " << treename << endl;
//    cout << "Node num " << kmeans_tree.GetNodeNum() << endl;
//    cout << "Depth " << kmeans_tree.GetDepth() << endl;
//    cout << "Leave cluster num " << kmeans_tree.GetLeavesClusterNum() << endl;
//    in_tree.close();


//    vector<float> doc_freq(kmeans_tree.GetNodeNum(), 0.0);

//    // count visit
//    string line;
//    Vector<DATADIM, float> feature;

//    int imagen = 0;
//    while(true) {
//      getline(input_stream, line);
//      if (!input_stream.good()) break;
//      ++imagen;
//      string name;
//      int n;
//      istringstream iss(line);
//      iss >> name >> n;
//      map<int, int> visited;
//      for (int i = 0; i < n; ++i) {
//        getline(input_stream, line);
//        istringstream iss2(line);
//        iss2 >> feature;
//        vector<int> visited_node;
//        int cluster = tree.FindCluster(feature.Data(), DATADIM, 32, &visited_node);
//        if (cluster >= 0) {
//          for (zuint i = 0; i < visited_node.size(); ++i) {
//            if (visited_node[i] >= 0) {
//              map<int, int>::iterator mi = visited.find(visited_node[i]);
//              if (mi == visited.end()) visited[visited_node[i]] = 1;
//              else ++mi->second;
//            }
//          }
//        }
//      }
//      score_stream << name << ' ' << visited.size() << ' ';
//      for (map<int, int>::iterator si = visited.begin(); si != visited.end(); ++si) {
//        ++doc_freq[si->first];
//        score_stream << si->first << ' ' << si->second << ' ';
//      }
//      score_stream << endl;
//    }
//    score_stream.close();
//    Array1f w(tree.GetNodeNum());
//    for (zuint i = 0; i < w.size(); ++i) {
//      w[i] = log(float(imagen) / doc_freq[i]);
//    }
//    output_stream << w << endl;
//    output_stream.close();
}


void FeatureExtractor::buildBowTree(const vector<FeatureParameter> &featParas,
                                vector<BasicImage> &trainImages,
                                const string treename,
                                    const string scorename,
                                    const string weightname)
{
    //vector<FeatVector> rawFeatures;
    //extractPixelLevelTrainFeature(featParas, trainImages, rawFeatures);
    ofstream out_score(scorename.c_str());
    ofstream out_weight(weightname.c_str());

    vector<vector<FeatVector> > train_feature_sets(trainImages.size());
    vector<vector<DataType> > train_data_all_img(trainImages.size()); // each image has a data vector

    int num_features = 0;
    for (size_t i = 0; i < trainImages.size(); ++i)
    {
        PixelImage pimg(trainImages.at(i));
        vector<FeatVector> features;
        extractPixelFeatureScreened(featParas, pimg, features);

        train_feature_sets[i] = features;

        num_features += features.size();

        for (size_t j = 0; j < features.size(); ++j)
            train_data_all_img[i].insert(train_data_all_img[i].end(),
                                         features[j]._data.begin(), features[j]._data.end());

        cout <<i+1<<"/"<<trainImages.size()<<" " << pimg._filenameImage << endl;
        cout <<"feature num " << features.size() <<"/"<<num_features<<", dim " << features[0].getDim() << endl;
        cout << "feat all data size " << train_data_all_img[i].size() << ", " << features.size() * features[0].getDim() << endl;
    }

    int feat_dim = train_feature_sets[0][0].getDim();

    vector<DataType> data;
//    vector<size_t> data_start_ids(num_features);

//    for (size_t i = 0, start_id = 0; i < trainImages.size(); ++i)
//    {
//        data_start_ids[i] = start_id;

//        for (size_t j = 0; j < train_feature_sets[i].size(); ++j)
//        {
//            vector<DataType> temp_data;
//            train_feature_sets[i][j].copyData2(temp_data);
//            data.insert(data.end(), temp_data.begin(), temp_data.end());

//        }
//        start_id += train_feature_sets[i].size();
//    }

    // contrast all the data as a vector
    for (size_t i = 0; i < train_data_all_img.size(); ++i)
        data.insert(data.end(), train_data_all_img[i].begin(), train_data_all_img[i].end());

    cout << "data size " << data.size() << ", featdim * featnum " << feat_dim * num_features << endl;
    this->buildBowIndex(data, num_features, feat_dim, treename);

    // read tree
    ifstream in_tree(treename.c_str());
    KMeansTree kmeans_tree(in_tree);
    cout << "Load kmeans tree from " << treename << endl;
    cout << "Node num " << kmeans_tree.GetNodeNum() << endl;
    cout << "Depth " << kmeans_tree.GetDepth() << endl;
    cout << "Leave cluster num " << kmeans_tree.GetLeavesClusterNum() << endl;
    in_tree.close();

    // calculate node weight
    vector<int> doc_freq(kmeans_tree.GetNodeNum(), 0);

    for (size_t i = 0; i < trainImages.size(); ++i)
    {

        map<int, int> visited;
        //
        for (size_t j = 0;  j < train_feature_sets[i].size(); ++j)
        {
            vector<int> visited_node;
            vector<DataType>& testdata = train_feature_sets[i][j]._data;
            int cluster = kmeans_tree.FindCluster(&(testdata[0]), feat_dim, 32, &visited_node);
            //cout << i<<", " << j << ", cluster " << cluster << endl;
            if (cluster >= 0)
            {
                for (size_t k = 0; k < visited_node.size(); ++k)
                    if (visited_node[k] >= 0)
                    {
                        map<int, int>::iterator mit = visited.find(visited_node[k]);
                        if (mit == visited.end())
                            visited[visited_node[k]] = 1;
                        else
                            ++mit->second;
                    }
            }
        }

        out_score << trainImages[i]._filenameImage <<" " << visited.size()<< " ";
        for (map<int,int>::iterator mit = visited.begin(); mit != visited.end(); ++mit)
        {
            ++doc_freq[mit->first];
            out_score << mit->first << " " << mit->second << " ";
        }
        out_score << endl;

    }

    out_score.close();

    vector<float> weight(kmeans_tree.GetNodeNum());
    for (size_t i = 0; i < weight.size(); ++i)
    {
        weight[i] = log(float(trainImages.size()) / doc_freq[i]);
        //cout << i << ", weight " << weight[i] << endl;
        out_weight << weight[i] << " ";
    }
    out_weight << endl;
    out_weight.close();
}


void FeatureExtractor::loadBow(const string filename, KMeansTree& kmeans_tree)
{
//    ifstream in(filename.c_str());
//    kmeans_tree = KMeansTree(in);

//    cout << "Load kmeans tree from " << filename << endl;
//    cout << "Node num " << kmeans_tree.GetNodeNum() << endl;
//    cout << "Depth " << kmeans_tree.GetDepth() << endl;
//    cout << "Leave cluster num " << kmeans_tree.GetLeavesClusterNum() << endl;
}

void FeatureExtractor::searchBow(vector<FeatureParameter> &featParas, PixelImage &pimg, KMeansTree& kmeans_tree)
{
    vector<FeatVector> testFeatures;
    extractPixelFeature(featParas, pimg, testFeatures);

    int tcount = 0;
    int nn = 3;

    for (size_t j = 0; j < testFeatures.size(); ++j)
    {
        if (testFeatures[j].getDim() > 0)
        {
            tcount++;
        }
    }
    int tcols = testFeatures.at(testFeatures.size() - 1).getDim();


    vector<DataType> test_data(tcount * tcols,0.0);
    DataType* test_data_pt = new DataType[tcount * tcols];

    for (int i = 0, idx = 0; i < tcount; ++i)
        for (int j = 0; j < tcols; ++j)
        {
            test_data[idx] = testFeatures[tcount]._data[j];
            test_data_pt[idx] = testFeatures[tcount]._data[j];
            idx++;
        }

    FLANNParameters flannpara;
    flannpara.log_level = LOG_INFO;
    flannpara.log_destination = NULL;
    flannpara.algorithm = KMEANS;
    flannpara.checks = 32;
    flannpara.branching = 8;
    flannpara.iterations = 10;  //  max iterantions
    flannpara.target_precision = -1;
    flannpara.centers_init = CENTERS_KMEANSPP;


    Params search_para;
    //search_para.insert(pair<string, Variant>("checks", Variant(32)));
    //search_para.insert(pair<string, Variant>("cb_index", Variant(1)));
    parametersToParams(flannpara, search_para);

    //KNNResultSet result_set(tcount * nn);
    //kmeans_tree.findNeighbors(result_set, &(test_data[0]), search_para);

    int level = kmeans_tree.GetDepth();
    vector<int> visitedNodes;
    visitedNodes.reserve(level);
    int clusterId = kmeans_tree.FindCluster(test_data_pt, test_data.size(), 32, &(visitedNodes));

    cout << "\n"<<pimg._filenameImage << endl;
    cout << "blongs to cluster " << clusterId << endl;
    cout << "the trances : ";
    for (size_t i = 0; i < visitedNodes.size(); ++i)
        cout << visitedNodes[i] << " ";

    delete [] test_data_pt;
}
