#-------------------------------------------------
#
# Project created by QtCreator 2013-04-22T22:29:36
#
#-------------------------------------------------
SHARELIBRARIES = $$(HOME)/SharedLibraries

QT       += core

QT       -= gui

TARGET = TreeClassify
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

unix {

#check the architecture
!include($$SHARELIBRARIES/ShareConfig.pri) {
    error("Cannot include ShareConfig.pri")
}

OBJECTS_DIR = OBJ$$ARCH
TARGET = $$TARGET$$ARCH$$DEBUGSUFFIX

#common lib
!include($$SHARELIBRARIES/ShareBase.pri) {
    error("Cannot include ShareBase.pri")
}

#vlfeat
!include($$SHARELIBRARIES/ShareVlfeat.pri) {
    error("Cannot include ShareVlfeat.pri")
}

#alglib
!include($$SHARELIBRARIES/ShareAlglib.pri) {
    error("Cannot include ShareAlglib.pri")
}

#flann
!include($$SHARELIBRARIES/ShareFlann.pri) {
    error("Cannot include ShareFlann.pri")
}

#boost
LIBS += -lboost_system

#omp
LIBS += -fopenmp

}


SOURCES += src/main.cpp \
    src/PixelImage.cpp \
    src/Classifier.cpp \
    src/Feature.cpp \
    src/Dataset.cpp \
    src/TreeCategory.cpp \
    src/GloabalVal.cpp \
    src/FeatureExtractor.cpp \
    src/RandomForest.cpp \
    src/DataSample.cpp \
    src/RandomClassifer.cpp \
    src/Clustering.cpp


HEADERS += \
    src/PixelImage.h \
    src/Classifier.h \
    src/Feature.h \
    src/Dataset.h \
    src/TreeCategory.h \
    src/GloabalVal.h \
    src/FeatureExtractor.h \
    src/RandomForest.h \
    src/DataSample.h \
    src/RandomClassifer.h \
    src/Clustering.h

